<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 27.05.14
 * Time: 16:13
 */

namespace app\common;

use Exception;
use yii\web\UrlManager;
use Yii;
use app\modules\seo\models\Seo;

class BaseUrlManager extends UrlManager {

    public function parseRequest($request)
    {
        $pathInfo = $request->getPathInfo();

        if($pathInfo == '')
            $pathInfo = '/';

        try {
            $seo = Yii::$app->cache->get('seo_'.$pathInfo);
            if(!$seo)
            {
                $seo = Seo::find()->where(['external_link'=>$pathInfo])->one();
                if($seo)
                    Yii::$app->cache->set('seo_'.$pathInfo,$seo,86400);
            }
        } catch (Exception $e){
            $seo = null;
        }

        if($seo!=null)
            return [$seo->internal_link, ['id'=>$seo->model_id]];

        return parent::parseRequest($request);

    }
}