<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 23.05.14
 * Time: 10:46
 */

namespace app\common;
use app\modules\params\models\Params;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use app\modules\seo\models\Seo;

class BaseController extends Controller {

    public $title, $description, $keywords, $noindex = false, $nofollow = false, $canonical, $sidebarAdvancedClass,
        $headerAdvanceClass, $galleryMenuItems = null, $showLoading = false, $advancedWrapClass;

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->params = Params::getParamsList();

        $url = '';

        if(isset($action->controller->module->request))
            $url = $action->controller->module->request->pathInfo;

        if(isset($action->controller->module->module->request->pathinfo))
            $url = $action->controller->module->module->request->pathinfo;

        if($url == '')
            $url = '/';

        $seo = Yii::$app->cache->get('seo_'.$url);
        if(!$seo)
        {
            $seo = Seo::find()->where(['external_link'=>$url])->one();
            if($seo)
                Yii::$app->cache->set('seo_'.$url,$seo,86400);
        }
        if($seo!=null)
        {
            $this->title = $seo->title;
            $this->description = $seo->description;
            $this->keywords = $seo->keywords;
        }

        return parent::beforeAction($action);
    }
}