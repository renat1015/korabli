<?php
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\helpers\FileHelper;
use yii\imagine\Image as ImageHelper;

function cmp($a, $b) {
    if ($a['ordering'] == $b['ordering']) {
        return 0;
    }
    return ($a['ordering'] < $b['ordering']) ? -1 : 1;
}

function getShortText($text,$width,$points=false)
{
    $text = strip_tags($text);
    $parts = preg_split('/([\s\n\r]+)/', $text, null, PREG_SPLIT_DELIM_CAPTURE);
    $parts_count = count($parts);
    $length = 0;
    $last_part = 0;
    for (; $last_part < $parts_count; ++$last_part) {
        $length += strlen($parts[$last_part]);
        if ($length > $width) { break; }
    }
    $result = implode(array_slice($parts, 0, $last_part));
    if($points)
        if($length > $width) $result = $result.'...';
    return $result;
}

function searchHighlighter($text, $word, $trim=false, $length=0, $points=false)
{
    if($trim)
        $fragment = getShortText($text,$length,$points);
    else
        $fragment = $text;
    $highlighted = str_ireplace($word, '<mark>' . $word . '</mark>', $fragment);
    return $highlighted;
}

function dump($data, $num = 10, $highlight = true)
{
     yii\helpers\VarDumper::dump($data, $num, $highlight);
}

/**
 * @param      $original_path
 * @param      $original_file_name
 * @param      $resize_file_path
 * @param      $width
 * @param      $height
 * @param bool $web_path
 *
 * @return bool|string
 */

function resizeImage($original_path, $original_file_name, $resize_file_path, $width, $height, $web_path = false)
{
    FileHelper::createDirectory($resize_file_path);

    $name = $width.'_'.$height.'_'.$original_file_name;

    $originalFullName = DIRECTORY_SEPARATOR . trim($original_path, '/') . DIRECTORY_SEPARATOR . $original_file_name;

    if(!file_exists($originalFullName))
        return false;

    $thumbFullName = DIRECTORY_SEPARATOR . trim($resize_file_path, '/') . DIRECTORY_SEPARATOR .$name;


    if(!file_exists($thumbFullName))
    {
        $originalImgInfo    = getimagesize($originalFullName);
        $originalWidth      = $originalImgInfo[0];
        $originalHeight     = $originalImgInfo[1];

        ImageHelper::getImagine()->open($originalFullName)->thumbnail(new Box($width+$width/2,$height+$height/2))->save($thumbFullName);

        $imgInfo = getimagesize($thumbFullName);
        $imgWidth = $imgInfo[0];
        $imgHeight = $imgInfo[1];


        if($imgWidth<$width)
        {
            $newWidth = $width+($width-$imgWidth);
            $newHeight = round($width / $originalWidth * $originalHeight);
            ImageHelper::getImagine()->open($originalFullName)->thumbnail(new Box($newWidth,$newHeight))->save($thumbFullName,['quality' => 100]);
            $imgInfo = getimagesize($thumbFullName);
            $imgWidth = $imgInfo[0];
            $imgHeight = $imgInfo[1];
        }

        if($imgHeight<$height)
        {
            $newWidth = round($height / $originalHeight * $originalWidth);
            $newHeight = $height+($height-$imgHeight);
            ImageHelper::getImagine()->open($originalFullName)->thumbnail(new Box($newWidth,$newHeight))->save($thumbFullName,['quality' => 100]);
            $imgInfo = getimagesize($thumbFullName);
            $imgWidth = $imgInfo[0];
            $imgHeight = $imgInfo[1];
        }

        if($imgWidth>$width || $imgHeight>$height)
        {
            $startX = 0;
            $startY = 0;
            if ($imgWidth > $width) {
                $startX = ceil($imgWidth - $width) / 2;
            }
            if ($imgWidth > $height) {
                $startY = ceil($imgHeight - $height) / 2;
            }
            ImageHelper::getImagine()->open($thumbFullName)->crop(new Point($startX,$startY), new Box($width,$height))->save($thumbFullName,['quality'=>100]);
        }
    }

    if($web_path)
        return DIRECTORY_SEPARATOR . trim($web_path, '/') . DIRECTORY_SEPARATOR . $name;

    return $name;

}

/**
 * @param           $str
 * @param bool|true $spec_crop
 * @param bool|true $to_lower
 *
 * @return mixed|string
 */
function translateIt($str, $spec_crop = true, $to_lower = true)
{

    $tr = array(
        "А" => "A", "Б" => "B", "В" => "V", "Г" => "G",
        "Д" => "D", "Е" => "E", "Ё" => "YO", "Ж" => "J", "З" => "Z", "И" => "I",
        "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N",
        "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T",
        "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "TS", "Ч" => "CH",
        "Ш" => "SH", "Щ" => "SCH", "Ъ" => "", "Ы" => "YI", "Ь" => "",
        "Э" => "E", "Ю" => "YU", "Я" => "YA", "а" => "a", "б" => "b",
        "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo", "ж" => "j",
        "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
        "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
        "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
        "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
        "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya", "»" => "", "«" => "", "ї" => "ji", "Ї" => "Ji",
    );

    $str = str_replace(array_keys($tr), array_values($tr), $str);

    if ($spec_crop === true) {
        $str = preg_replace('/[^а-яА-Яa-zA-ZёЁ\d\-_]+/', '-', $str);
    }

    if ($to_lower === true) {
        $str = mb_strtolower($str, 'utf-8');
    }

    return $str;
}