<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 23.05.14
 * Time: 10:36
 */

namespace app\common;
use app\modules\image\models\Image;
use ReflectionClass;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\caching\DbDependency;
use yii\db\Expression;
use yii\db\ActiveRecord;
use app\modules\seo\models\Seo;
use yii\db\Query;

class BaseModel extends ActiveRecord {

    /**
     * @return array
     */
    const STATUS_PUBLISHED = 1;
    const STATUS_NOT_PUBLISHED = 0;
    const DEFAULT_CACHE_DURATION = 86400;
    public $useCache = true;

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getImages($width=null,$height=null,$escapeMain=false)
    {
        return Image::getModelImages($this->id,$this->getModelName(),$width,$height,$escapeMain);
    }

    public static function getModelName()
    {
        $reflect = new ReflectionClass(self::className());
        return $reflect->getShortName();
    }

    public function getStatusName()
    {
        return self::getStatusList()[$this->status];
    }

    static function getStatusList()
    {
        return [
            self::STATUS_PUBLISHED => 'Опубликовано',
            self::STATUS_NOT_PUBLISHED => 'Не опубликовано'
        ];
    }

    /**
     * @return static
     */
    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['model_id' => 'id'])->where(['model_name' => self::getModelName()]);
    }

    /**
     * @return string
     */
    public function getSeoUrl()
    {
        return '/'.trim($this->seo->external_link, '/');
    }

    /**
     * @param null $table
     * @param string $field
     * @return DbDependency
     */
    static function getDbDependency($table=null,$field='updated_at')
    {
        if($table==null)
            $table = self::tableName();
        $dependency = new DbDependency();
        $dependency->sql = 'SELECT MAX('.$field.') FROM '.$table;
        return $dependency;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($this->useCache)
            Yii::$app->cache->set($this->defaultCacheId($this->id),$this,self::DEFAULT_CACHE_DURATION);
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        if($this->useCache)
            Yii::$app->cache->delete($this->defaultCacheId($this->id));
        parent::afterDelete();
    }

    public static function findByCacheId($cache_id,$model_id=null)
    {
        $model = Yii::$app->cache->get($cache_id);
        if(!$model)
        {
            $model = self::findOne($model_id);
            Yii::$app->cache->set(self::defaultCacheId($model_id),$model,self::DEFAULT_CACHE_DURATION);
        }
        return $model;
    }

    public function getCacheId()
    {
        return self::defaultCacheId($this->id);
    }

    public static function defaultCacheId($model_id)
    {
        return 'model_'.strtolower(self::getModelName()).'_id_'.$model_id;
    }

    /**
     * @param $table
     * @return bool|string
     */
    public function getMaxOrder($table=null)
    {
        if($table==null)
            $table = self::getModelName();
        $maxOrder = (new Query())
            ->select('MAX(ordering) as maxOrder')
            ->from($table)
            ->scalar();
        return $maxOrder;
    }
}