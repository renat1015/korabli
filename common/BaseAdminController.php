<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 23.05.14
 * Time: 10:46
 */

namespace app\common;
use Yii;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\params\models\Params;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class BaseAdminController extends Controller {
    public $layout = '@app/views/layouts/admin';

    public function beforeAction($action)
    {
        Yii::$app->params = Params::getParamsList();

        return parent::beforeAction($action);
    }
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles'=>['@']
//                        'roles' => [User::ROLE_ADMIN,User::ROLE_MODERATOR] // if use User module
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $table
     * @return bool|string
     */
    public function getMaxOrder($table)
    {
        $maxOrder = (new Query())
            ->select('MAX(ordering) as maxOrder')
            ->from($table)
            ->scalar();
        return $maxOrder;
    }

    public function actionUploadImperavi($module)
    {
        $pic = UploadedFile::getInstanceByName('file');
        if (
            $pic->type == 'image/png'
            || $pic->type == 'image/jpg'
            || $pic->type == 'image/gif'
            || $pic->type == 'image/jpeg'
            || $pic->type == 'image/pjpeg'
        ) {
            $name =  md5(time()).'.jpg';
            $path = '/uploads/'.strtolower($module).'/imperavi/';
            $base_path = Yii::$app->basePath.'/..'.$path;
            if(!is_dir($base_path))
                FileHelper::createDirectory($base_path, 0777);

            if($pic->saveAs($base_path.$name))
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'filelink' => $path.$name
                ];
            }
        }
        return false;
    }


    public function actionDeleteImperaviImg()
    {
        if(Yii::$app->request->isAjax)
        {
            $imgUrl = Yii::$app->request->post('imgUrl');
            if($imgUrl)
                @unlink(Yii::$app->basePath.'/..'.$imgUrl);
        }
        else
            throw new NotFoundHttpException('Сторінка не існує.');
    }
}