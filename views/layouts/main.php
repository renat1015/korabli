<?php

// определение мобильного устройства
function check_mobile_device() {
    $mobile_agent_array = array('ipad', 'iphone', 'android', 'pocket', 'palm', 'windows ce', 'windowsce', 'cellphone', 'opera mobi', 'ipod', 'small', 'sharp', 'sonyericsson', 'symbian', 'opera mini', 'nokia', 'htc_', 'samsung', 'motorola', 'smartphone', 'blackberry', 'playstation portable', 'tablet browser');
    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    // var_dump($agent);exit;
    foreach ($mobile_agent_array as $value) {
        if (strpos($agent, $value) !== false) return true;
    }
    return false;
}

function open_url($url){
    $url_c=parse_url($url);
    if (!empty($url_c['host']) and checkdnsrr($url_c['host'], 'ANY')) {
        $otvet=@get_headers($url);

        if ($otvet){
            return substr($otvet[0], 9, 3);
        }
    }
    return false;
}

// пример использования
$is_mobile_device = check_mobile_device();
$url = 'https://m.' . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'];
//
$url_arr = explode('/', $_SERVER['REQUEST_URI']);



if($is_mobile_device){
    $exist = false;

    // ссылка корректная
    if ($o=open_url($url))
    {
        if($o != 404){
            $exist = true;
        }
    }


    if($exist){
        header("Location: ". $url);
        exit();

    } else {
        header("Location: https://m." . $_SERVER['HTTP_HOST']);
        exit();

    }


}

use app\modules\htmlBlock\widgets\HtmlWidget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this View */
/* @var $content string */

//yii\bootstrap\BootstrapPluginAsset::register($this);
AppAsset::register($this);
$this->registerMetaTag([
    'name'    => 'robots',
    'content' => ($this->context->noindex ? 'NOINDEX' : 'INDEX') . ', ' . ($this->context->nofollow ? 'NOFOLLOW' : 'FOLLOW'),
]);

if (isset($this->context->description)) {
    $this->registerMetaTag([
        'name'    => 'description',
        'content' => Html::encode($this->context->description)
    ]);
}
if (isset($this->context->keywords)) {
    $this->registerMetaTag([
        'name'    => 'keywords',
        'content' => Html::encode($this->context->keywords)
    ]);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode(isset($this->context->title) ? $this->context->title : $this->title) ?></title>
    <link rel="icon" type="image/png" sizes="36x36"
          href="<?= Yii::$app->urlManager->createAbsoluteUrl('') . 'favicon.ico'; ?>">
    <link rel="shortcut icon" type="image/png" sizes="36x36"
          href="<?= Yii::$app->urlManager->createAbsoluteUrl('') . 'favicon.ico'; ?>">
    <link rel="apple-touch-icon" href="<?= Yii::$app->urlManager->createAbsoluteUrl('') . 'favicon.ico'; ?>"/>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <?php if ($this->context->canonical) { ?>
        <link rel="canonical"
              href="<?= Yii::$app->urlManager->createAbsoluteUrl('') . trim($_SERVER['REQUEST_URI'], '/') ?>"/>
    <?php } ?>
    <meta name="yandex-verification" content="8d6c7811e5f688d3" />
</head>
<body class="<?= $this->context->sidebarAdvancedClass ?>">
<?php $this->beginBody() ?>
<?= \app\modules\banner\widgets\BannerWidget::widget() ?>
<header class="<?= $this->context->headerAdvanceClass ?>">
    <div class="logo">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . '/img/w_logo.png', ['class' => 'white', 'alt' => Yii::$app->name]), \yii\helpers\Url::home()) ?>
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . '/img/b_logo.png', ['class' => 'blue', 'alt' => Yii::$app->name]), \yii\helpers\Url::home()) ?>
    </div>
    <div class="menu-block">
        <a href="#" class="btn-menu-open">
            <span></span><span></span><span></span>Открыть меню
        </a>
        <div class="kor-menu">
            <a href="#" class="btn-menu-close">
                <span class="btn-close"></span> <span>Закрыть меню</span>
            </a>
            <ul class="lincks">
                <li>
                    <?= Html::a('<span>На главную</span><span class="image">'. Html::img(Yii::$app->request->baseUrl . '/img/lincks1.png', ['alt' => Yii::$app->name]) .'</span>', \yii\helpers\Url::home()) ?>
                </li>
                <li>
<!--                    --><?//= Html::a('<span>Выбор квартир</span><span class="image">'. Html::img(Yii::$app->request->baseUrl . '/img/lincks2.png', ['alt' => Yii::$app->name]) .'</span>', ['/realty']) ?>
                    <a href="/realty/realty" ><span>Выбор квартир</span><span class="image"><?= Html::img(Yii::$app->request->baseUrl . '/img/lincks2.png', ['alt' => Yii::$app->name]); ?></span></a>
                </li>
                <li>
                    <?= Html::a('<span>Ипотека</span><span class="image">'. Html::img(Yii::$app->request->baseUrl . '/img/lincks3.png', ['alt' => Yii::$app->name]) .'</span>', ['/ipoteka']) ?>
                </li>
                <li>
                    <?= Html::a('<span>Документы</span><span class="image">'. Html::img(Yii::$app->request->baseUrl . '/img/lincks4.png', ['alt' => Yii::$app->name]) .'</span>', ['/files/default/index']) ?>
                </li>
            </ul>
            <?= \app\modules\menu\widgets\MenuWidget::widget([
                'galleryItems' => $this->context->galleryMenuItems,
            ]) ?>

<!--                <li class="active"><a href="#">О застройщике</a></li>-->
<!--                <li class="folder"><a href="#">О проекте</a>-->
<!--                    <ul>-->
<!--                        <li><a href="#">Расположение </a></li>-->
<!--                        <li><a href="#"> О ЖК КОрабли</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li><a href="#">выбор квартир</a></li>-->
<!--                <li class="folder">-->
<!--                    <a href="#">Динамика строительства</a>-->
<!--                    <ul>-->
<!--                        <li><a href="#">Фотоотчеты</a></li>-->
<!--                        <li><a href="#">Онлайн камера</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li class="folder">-->
<!--                    <a href="#">Галерея</a>-->
<!--                    <ul>-->
<!--                        <li><a href="#">3D Визуализация</a></li>-->
<!--                        <li><a href="#">3D виртуальный тур</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li><a href="#">Новости</a></li>-->
<!--                <li><a href="#">Контакты</a></li>-->
        </div>
    </div>
</header>

<div class="main">
    <?= $content ?>
</div>

<?php

$route = Yii::$app->controller->route;

$header_another = [
    'site/contact',
];

if(!in_array($route, $header_another)):
?>

<footer>
    <div class="foot-left">
        <div class="social">
            <?= HtmlWidget::widget(['position' => 'social']) ?>
        </div>
        <div class="copyright">
            Любая информация, представленная на данном сайте, носит исключительно информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями статьи 437 ГК РФ.
            <a href="<?= \yii\helpers\Url::to(['/important-information'])?>" class="important">Важная информация.</a>
            <a href="#">Политика обработки персональных данных.</a>
        </div>
        <div class="author">
            <a href="https://nnovgorod3d.ru/?utm_source=korabli&utm_medium=site&utm_campaign=korabli" target="_blank">Создание сайта   <span>NNovgorod3D</span></a>
        </div>
    </div>
    <div class="foot-right">
        <div class="phone">
            <span class="icon-phone"><i class="fa fa-phone" aria-hidden="true"></i></span>
            <a href="tel:<?= Yii::$app->params['phone'] ?>"><span><?= Yii::$app->params['phone'] ?></span> </a>
        </div>
        <?= Html::a('<span>Заказать звонок</span>', ['/contacts/default/call'], ['class' => 'open-modal', 'onclick' => "ym(45591978,'reachGoal','callback_click')"]) ?>
        <div class="address">Нижний Новгород, ул. Белозерская, д3</div>
    </div>
</footer>
<?php endif;?>


<?php $this->endBody() ?>
<?php

$this->registerJs(<<<JS
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-30277278-2', 'auto'); ga('send', 'pageview');
JS
    , $this::POS_HEAD);
?>


<script type="text/javascript" >
(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

ym(45591978, "init", {
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,
webvisor:true
});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45591978" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<!-- BEGIN K50 TRACKER CODE-->
<script>
(function(c,a,p) {
var s = document.createElement(a); s.src = p; s.type = "text/javascript"; s.async =!0; s.readyState ? s.onreadystatechange = function() { if ( s.readyState === "loaded" || s.readyState === "complete" ) { s.onreadystatechange = null; c();}} : s.onload = function () {c();}; var n = document.getElementsByTagName(a)[0]; n.parentNode.insertBefore(s,n); })(function(){
k50Tracker.init({
siteId: 82089184954186
})
},"script","https://k50-a.akamaihd.net/k50/k50tracker2.js");
</script>
<!-- END K50 TRACKER CODE-->

</body>
</html>
<?php $this->endPage() ?>
