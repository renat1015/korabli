<?php
use app\modules\htmlBlock\widgets\HtmlWidget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this View */
/* @var $content string */

//yii\bootstrap\BootstrapPluginAsset::register($this);
AppAsset::register($this);
$this->registerMetaTag([
    'name'    => 'robots',
    'content' => ($this->context->noindex ? 'NOINDEX' : 'INDEX') . ', ' . ($this->context->nofollow ? 'NOFOLLOW' : 'FOLLOW'),
]);

if (isset($this->context->description)) {
    $this->registerMetaTag([
        'name'    => 'description',
        'content' => Html::encode($this->context->description)
    ]);
}
if (isset($this->context->keywords)) {
    $this->registerMetaTag([
        'name'    => 'keywords',
        'content' => Html::encode($this->context->keywords)
    ]);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode(isset($this->context->title) ? $this->context->title : $this->title) ?></title>
    <link rel="icon" type="image/png" sizes="36x36"
          href="<?= Yii::$app->urlManager->createAbsoluteUrl('') . 'favicon.ico'; ?>">
    <link rel="shortcut icon" type="image/png" sizes="36x36"
          href="<?= Yii::$app->urlManager->createAbsoluteUrl('') . 'favicon.ico'; ?>">
    <link rel="apple-touch-icon" href="<?= Yii::$app->urlManager->createAbsoluteUrl('') . 'favicon.ico'; ?>"/>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <?php if ($this->context->canonical) { ?>
        <link rel="canonical"
              href="<?= Yii::$app->urlManager->createAbsoluteUrl('') . trim($_SERVER['REQUEST_URI'], '/') ?>"/>
    <?php } ?>
</head>
<body class="<?= $this->context->sidebarAdvancedClass ?>">
<?php $this->beginBody() ?>

<?= $content ?>


<?php $this->endBody() ?>
<?php

$this->registerJs(<<<JS
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-30277278-2', 'auto'); ga('send', 'pageview');
JS
    , $this::POS_HEAD);
?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter34112585 = new Ya.Metrika({
                    id: 34112585,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/34112585" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
<?php $this->endPage() ?>
