<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
\app\assets\BackAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode(isset($this->context->title) ? $this->context->title : $this->title) ?></title>
    <script type="text/javascript" src="/js/humane_js/humane.js"></script>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <?php
    $this->registerJs(<<<JS
$('.sidebar-menu li ul li.active').parents('li').addClass('active');
JS
    );
    ?>
</head>
<body class="skin-purple sidebar-mini fixed">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/admin" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>MG</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">ЖК Корабли </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-gear"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <ul class="menu">
                                    <li>
                                        <a target="_blank" href="/">Перейти на сайт</a>
                                    </li>
                                    <li>
                                        <?= Html::a('Выход',['/site/logout'],['data-method'=>'post']) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="header">Главное меню</li>
                <?= \app\modules\admin\models\Admin::buildMenu()?>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?= $content ?>
        </section>

    </div><!-- /.content-wrapper -->

    <footer class="main-footer">

    </footer>

</div>

<?php $this->endBody() ?>
<?php foreach(Yii::$app->session->getAllFlashes() as $key => $message): ?>
    <script type="text/javascript">
        humane.log('<?php echo Html::encode($message);?>',{timeout:2500});
    </script>
<?php endforeach; ?>
</body>
</html>
<?php $this->endPage() ?>