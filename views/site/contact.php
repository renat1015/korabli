<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\modules\htmlBlock\widgets\HtmlWidget;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

//$this->title = 'Contact';
?>
<div class="kor-choice-home map contacts">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="contact open">
        <section class="contact-block-inner">
            <h1 class="title">
                Контакты
            </h1>
            <div class="content">
                <address class="address">Нижний Новгород <br> <span class="strit">ул.Белозерская д3</span></address>
                <a class="phone" href="tel:<?= Yii::$app->params['phone'] ?>"><?= Yii::$app->params['phone'] ?></a>
                <div class="schedule">
                    <p>Пн.-Пт.: 8:00-18:00</p>
                    <p>Сб.: 9:00-16:00</p>
					<p>Вс.: Выходной</p>
                </div>
                <a href="#">Проложить маршрут</a>
                <?= Html::a('Обратная связь',['/contacts/default/letter'],['class'=>'write-letter open-modal', 'onclick' => "ym(45591978,'reachGoal','obratnaya_svyaz_click')"]) ?>
            </div>
            <div class="footer contacts">
                <div class="foot-left">
                    <div class="social">
                        <?= HtmlWidget::widget(['position' => 'social']) ?>
                    </div>
                    <div class="copyright">
                        Любая информация, представленная на данном сайте, носит исключительно информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями статьи 437 ГК РФ.
                        <a href="<?= \yii\helpers\Url::to(['/important-information'])?>" class="important">Важная информация.</a>
                        <a href="#">Политика обработки персональных данных.</a>
                    </div>
                    <div class="author">
                        <a href="https://nnovgorod3d.ru/?utm_source=korabli&utm_medium=site&utm_campaign=korabli" target="_blank">Создание сайта   <span>NNovgorod3D</span></a>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="map">
        <iframe src="https://snazzymaps.com/embed/61106" width="100%" height="100%" style="border:none;"></iframe>
    </div>
    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>

