<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

$this->context->sidebarAdvancedClass = 'error__page';

?>
<main>
    <div class="error-main">
        <div id="rotate-device">
            <div class="middle">
                <img src="/img/rotate__device.svg">
            </div>
        </div>
        <div class="error-cont">
            <img src="/img/error_404.png" alt="error-404">
            <h1 class="title">Страница не найдена</h1>
        </div>

        <?= Html::a('На ГЛАВНУЮ Страницу', \yii\helpers\Url::home(), ['class' => 'btn btn-to-main']) ?>
    </div>
</main>