<?php
use app\modules\user\models\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
$this->context->sidebarAdvancedClass = 'pre-closed';

?>
<div class="site-login container" style="padding-top: 10%">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>Для входа введите логин и пароль:</p>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-9\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-3 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="col-lg-offset-3 col-lg-3">
                <?= $form->field($model, 'rememberMe', [
                    'template' => "{input}{error}",
                ])->checkbox() ?>
            </div>

            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-11">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>

</div>
<?php

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/bootstrap.css');
