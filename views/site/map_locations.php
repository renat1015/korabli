<?php
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 02.11.15
 * Time: 11:09
 * @var $this yii\web\View
 */
?>

<!--<div id="map-block" style="width: 100%; height: 100%;"></div>-->
<!--<script type="text/javascript">-->
<!--    var map;-->
<!--    function initMap() {-->
<!--        var myLatLng = new google.maps.LatLng(56.30217234707454,43.97392487292484);-->
<!--//        var markerIcon = '/img/map_icon.png';-->
<!--        map = new google.maps.Map(document.getElementById('map-block'), {-->
<!--            center: myLatLng,-->
<!--            zoom: 16-->
<!--        });-->
<!---->
<!--        --><?php //if($points): ?>
<!--           --><?php //foreach($points as $point): ?>
<!--                var markerIcon = '--><?////= $point['map_icon'] ?><!--//',-->
<!--                    title = '--><?////= $point['title'] ?><!--//',-->
<!--                    latLng = new google.maps.LatLng('--><?////= $point['lat_lng'][0] ?><!--//','--><?////= $point['lat_lng'][1] ?><!--//');-->

<!--//                var marker = new google.maps.Marker({-->
<!--//                    position: latLng,-->
<!--//                    map: map,-->
<!--//                    title: title,-->
<!--//                    icon: markerIcon-->
<!--//                });-->
<!---->
<!--//                var objectDescription = '--><?////= str_ireplace(array("\r", "\n", '\r', '\n'), '', $point['object_desc']) ?><!--//';-->
<!---->
<!--//                openInfo(marker,objectDescription);-->
<!--//            --><?php ////endforeach; ?>
<!--        --><?php //endif; ?>

<!--//        function openInfo(marker,objectDescription){-->
<!--//-->
<!--//            var infoWindow = new google.maps.InfoWindow({-->
<!--//                content: objectDescription,-->
<!--//                maxWidth: 250-->
<!--//            });-->
<!--//-->
<!--//            google.maps.event.addListener(marker, 'click', function (event) {-->
<!--//                infoWindow.open(map, marker)-->
<!--//            });-->
<!--//-->
<!--//            google.maps.event.addListener(marker, 'dblclick', function (event) {-->
<!--//                infoWindow.close(map, marker)-->
<!--//            });-->
<!--//-->
<!--//            google.maps.event.addListener(map,'click',function(event){-->
<!--//                infoWindow.close(map, marker)-->
<!--//            });-->
<!--//        }-->
<!--//    }-->
<!--</script>-->
<!--//<script async defer src="https://maps.googleapis.com/maps/api/js?language=ru&callback=initMap"></script>-->

<div class="kor-choice-home map">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="title">
        <h3>Расположение</h3>
    </div>

    <div id="map" style="width: 100%;height: 100vh;">
<!--        <iframe src="https://snazzymaps.com/embed/37474" width="100%" height="100%" style="border:none;"></iframe>-->
    </div>

    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>

<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/gmap3.min.js', ['depends' => 'yii\web\JqueryAsset', 'position' => yii\web\View::POS_HEAD]);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyA1ywZ8DEQjcKi5EjV8A61j1iRwnIN5oVw', ['depends' => 'yii\web\JqueryAsset', 'position' => yii\web\View::POS_HEAD]);

?>



<script>
    $(document).ready(function(){
        var windows = [
            <?php if($points): ?>
                <?php foreach($points as $point): ?>
                    {
                        lat: <?= $point['lat_lng'][0] ?>,
                        lng: <?= $point['lat_lng'][1] ?>
                    },
                <?php endforeach; ?>
            <?php endif; ?>
        ];

        $('#map').gmap3({
            <?php if($points): ?>
                <?php foreach($points as $point): ?>
                    <?php if($point['main'] == 1):?>
                        center: [<?= $point['lat_lng'][0] ?>,<?= $point['lat_lng'][1] ?>],
                    <?php endif;?>
                <?php endforeach; ?>
            <?php endif; ?>
            zoom: 16,
            mapTypeId: "shadeOfGrey", // to select it directly
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, "shadeOfGrey"]
            }
        })
            .marker([
                <?php if($points): ?>
                    <?php foreach($points as $point): ?>
                        {
                            position:[<?= $point['lat_lng'][0] ?>, <?= $point['lat_lng'][1] ?>],
                            icon: "<?= $point['map_icon'] ?>",
                            info: '<?= str_ireplace(array("\r", "\n", '\r', '\n'), '', $point['object_desc']);?>'
                        },
                    <?php endforeach; ?>
                <?php endif; ?>
            ])
            .then(function (markers) {
                markers.forEach(function (marker) {
                    marker.addListener('click', function() {
                        infowindow.setContent(marker.info);
                        infowindow.open(marker.getMap(), marker);
                    });
                })
            })
            .infowindow({
                content: ''
            })
            .then(function (iw) {
                infowindow = iw;
            })

            .styledmaptype(
                "shadeOfGrey",
                [
                    {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
                    {"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},
                    {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},
                    {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
                    {"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}
                ],
                {name: "Shades of Grey"}
            )

    });
</script>
