<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\web\View;
use app\modules\pointMap\models\PointType;
use app\modules\realty\models\RealtyFlat;

$this->title = Yii::$app->name;
$this->context->advancedWrapClass = 'full-width';
$this->registerJs(<<<JS
    $('body').css('overflow','hidden');
JS
);

$this->registerJsFile("@web/js/show.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCss(<<<CSS
.tooltip_box{
	display: none;
	background-color: #fff;
	padding: 10px;
	border-radius: 10px;
	position: absolute;
	object-fit: contain;
	margin-left: 25px;
	margin-top: 25px;
	z-index: 1002;
	font-size: 12px;
}
CSS
);
?>

<div class="kor-home">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
	<div class="map-xxx" id="map_xxx" style="width:fit-content; min-width:100%; position:relative; height: 100%;">
		<img src="<?= $model->getImage() ?>" alt="img" style="min-width: 100%; width: auto; height: 100%;">
		<?php foreach($points as $point): ?>
			<?php $point_type = PointType::getTypePoint($point->point_type); ?>
			<img src="<?= $point_type->getImage() ?>" class="point-on-map" alt="img" style="left: <?= $point->point_x ?>%; top: <?= $point->point_y ?>%; position: absolute; width: 50px; height: 50px; object-fit: contain; margin-left: -25px; margin-top: -25px; z-index: 0;">
			<div class="tooltip_box" style="left: <?= $point->point_x ?>%; top: <?= $point->point_y ?>%;">
				<span><b><?= $point->title ?></b></span><br>
				<span><?= $point->description ?></span>
				<?php if($point->home_num != 0): ?>
					<?php $arr_flat = RealtyFlat::getApartmentsByHouseId($point->home_num); ?>
					<?php if($arr_flat !== FALSE): ?>
						<span>Квартир в продаже:</span><br>
						<?php foreach($arr_flat as $key=>$value): ?>
							<?php if($key == 0): ?>
								<span>Студий: </span>
							<?php elseif($key != 0): ?>
								<span><?= $key ?>-комнатных: </span>
							<?php endif; ?>
							<?= $value; ?><br>
						<?php endforeach; ?>
					<?php endif; ?>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>