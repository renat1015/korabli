<?php

namespace app\modules\crm;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\crm\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
