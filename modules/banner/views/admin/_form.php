<?php

use kartik\widgets\DatePicker;
use kartik\widgets\SwitchInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\banner\models\Banner */
/* @var $form yii\widgets\ActiveForm */
if($model->timer==null)
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

    <p class="alert alert-info" role="alert">Рекомендуемая ширина изображения 800px</p>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(\yii\imperavi\Widget::className(), [
        'id'=>'html-content',
        'options' => [
            'buttonSource'      => 'true',
            'minHeight'         => 350,
            'imageUpload' => \yii\helpers\Url::to(['/admin/default/upload-imperavi','module'=>$model->getModelName()]),
            'uploadImageFields' => [
                Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()
            ],
            'uploadFileFields' => [
                Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()
            ],
            'imageDeleteCallback' => "function(url,image){
                $.ajax({
                    url: '/admin/default/delete-imperavi-img?modeule=category',
                    type: 'post',
                    data: {imgUrl:$(image).attr('src'), _csrf: yii.getCsrfToken()}
                });
            }",
        ],
        'plugins' => [
            'fullscreen',
            'clips',
            'fontcolor',
            'fontfamily',
            'fontsize',
        ],
    ]) ?>

    <?= $form->field($model,'timer')->widget(DatePicker::className(),[
        'type' => DatePicker::TYPE_INPUT,
        'id' => 'timer-date-input',
        'options' => [
            'value'=> ($model->timer=='<span class="not-set">(не задано)</span>') ? '' : $model->timer,
            'placeholder'=>$model->getAttributeLabel('timer'),
        ],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'startDate' => date('Y-m-d')
        ]
    ]) ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),[
        'pluginOptions' =>
            [
                'size'    => 'small',
                'onText'  => 'Да',
                'offText' => 'Нет',
                'state'   => $model->isNewRecord ? true : $model->status,
            ],
    ]); ?>

    <?php
    $model->pages = \yii\helpers\Json::decode($model->pages);

    echo $form->field($model, 'pages')->checkboxList(
        [
            'site/index' => 'Главная',
            'realty' => 'Квартиры',
            'page' => 'Текстовые страницы',
            'ipoteka' => 'Ипотека',
            'files' => 'Документы',
            'about' => 'О проекте',
            'map-locations' => 'Расположение',
            'progress' => 'Динамика строительства',
            'architecture' => 'Галерея',
            'virtualTour' => '3D тур',
            'news' => 'Новости',
            'contact' => 'Контакты',
            'all' => 'На всех страницах',
        ],
        ['separator' => '<br>']
    )->label('На каких страницах выводить?')
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
