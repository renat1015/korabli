<?php

namespace app\modules\banner\controllers;

use app\common\BaseAdminController;
use app\modules\banner\models\Banner;
use app\modules\banner\models\BannerSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * AdminController implements the CRUD actions for Banner model.
 */
class AdminController extends BaseAdminController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors,[
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actionUpload()
    {
        $pic = UploadedFile::getInstanceByName('file');
        if (
            $pic->type == 'image/png'
            || $pic->type == 'image/jpg'
            || $pic->type == 'image/gif'
            || $pic->type == 'image/jpeg'
            || $pic->type == 'image/pjpeg'
        ) {
            $name =  md5(time()).'.jpg';
            if(!file_exists(Yii::$app->basePath.'/../uploads/html/'))
                FileHelper::createDirectory(Yii::$app->basePath.'/../uploads/html/', 0777);
            if(!file_exists(Yii::$app->basePath.'/../uploads/html/imperavi'))
                FileHelper::createDirectory(Yii::$app->basePath.'/../uploads/html/imperavi', 0777);

            if($pic->saveAs(Yii::$app->basePath.'/../uploads/html/imperavi/'.$name))
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'filelink' => Yii::$app->params['image_site'].'/uploads/html/imperavi/'.$name
                ];
            }
        }
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banner();
        $model->status = $model::STATUS_PUBLISHED;
        if ($model->load(Yii::$app->request->post())) {
            $model->pages = Json::encode($model->pages);
            $model->save();
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->timer = Yii::$app->formatter->asDate($model->timer,'yyyy-MM-dd');
        if ($model->load(Yii::$app->request->post())) {
            if($model->pages == ''){
                $array = [
                    'all'
                ];
                $model->pages = $array;
            }
            $model->pages = Json::encode($model->pages);

            $model->save();
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
