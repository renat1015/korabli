<?php

namespace app\modules\banner;

class BannerModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\banner\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
