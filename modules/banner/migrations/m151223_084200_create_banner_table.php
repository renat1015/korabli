<?php

use yii\db\Schema;
use yii\db\Migration;

class m151223_084200_create_banner_table extends Migration
{
    public function up()
    {
        $this->createTable('banner', [
            'id'         => $this->primaryKey(),
            'title'      => $this->string(255)->notNull() . ' COMMENT "Назвение"',
            'content'    => $this->text()->notNull(). ' COMMENT "Содержание"',
            'status'     => $this->smallInteger()->defaultValue(1) . ' COMMENT "Публиковать"',
            'timer'      => $this->timestamp(). ' COMMENT "Таймер"',
            'updated_at' => $this->timestamp()->notNull() . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата обновления"',
            'created_at' => $this->timestamp() . ' COMMENT "Дата добавления"',
        ]);
    }

    public function down()
    {
        $this->dropTable('banner');
    }
}
