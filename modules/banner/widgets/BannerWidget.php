<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 23.12.15
 * Time: 10:52
 */

namespace app\modules\banner\widgets;


use app\modules\banner\models\Banner;
use yii\base\Widget;

class BannerWidget extends Widget
{
    public function run()
    {
        $model = Banner::find()->active()->orderBy('RAND()')->limit(1)->one();

        if($model)
            return $this->render('banner_widget',['model'=>$model]);
        return false;
    }
}