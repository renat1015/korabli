<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 23.12.15
 * Time: 10:53
 * @var $model \app\modules\banner\models\Banner
 * @var $this \yii\web\View
 */
use yii\helpers\Html;

    $this->registerJsFile('/js/jquery.countdown.js',['depends'=>'yii\web\JqueryAsset']);
    $end_date = Yii::$app->formatter->asDate($model->timer,'yyyy/MM/dd');
    $this->registerJs(<<<JS
    
    function getCookie(name) {
		var matches = document.cookie.match(new RegExp(
			"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		));
		return matches ? decodeURIComponent(matches[1]) : undefined
	}

	function setCookie(name, value, props) {
		props = props || {};
		var exp = props.expires;
		if (typeof exp == "number" && exp) {
			var d = new Date();
			d.setTime(d.getTime() + exp*1000);
			exp = props.expires = d
		}
		if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

		value = encodeURIComponent(value);
		var updatedCookie = name + "=" + value;
		for(var propName in props){
			updatedCookie += "; " + propName;
			var propValue = props[propName];
			if(propValue !== true){ updatedCookie += "=" + propValue }
		}
		document.cookie = updatedCookie

	}

	function deleteCookie(name) {
		setCookie(name, null, { expires: -1 })
	}
    var show = false;

	if (getCookie('popups')) {
		var popupShowed = JSON.parse(getCookie('popups'));

		$('#visual-block').each(function() {
		    var popup = $(this);
		    
		    var BreakException = {};

            try {
                popupShowed.forEach(function(item, i, arr) {
                    if (popup.data('id') == item) {
                        show = true;
                        throw BreakException;
                    }
                });
            } catch (e) {
                if (e !== BreakException) throw e;
            }
            
            
            		    
		});
	}
            
    if(show == false){
        $('#visual-block').addClass('show');
    }
    var endDate = '$end_date';
     $('.timer-block').countdown(endDate, function(event) {
       var totalHours = event.offset.totalDays * 24 + event.offset.hours;
       $(this).html(event.strftime(totalHours + ' <span>час.</span> %M <span>мин.</span> %S <span>сек.</span>'));
    });
    $('#visual-block .content-block .close-page').click(function(e){
        e.preventDefault();
        $(this).parents('#visual-block').fadeOut('fast',function(){
            $(this).remove();
        });
        var array = [];
        var arr = getCookie('popups');
        var id = $(this).parents('#visual-block').data('id');
        
        if(arr){
            arr = JSON.parse(arr);
            array = arr.slice();
        }
        
        if($.inArray(id,array) == -1){
            array.push(id);
        }
        
        setCookie('popups', JSON.stringify(array));
        
        return false;
    });
JS
);
$model->pages = \yii\helpers\Json::decode($model->pages);
$route = Yii::$app->controller->route;
$show = false;

if(!in_array('all', $model->pages)){
    foreach ($model->pages as $page){

        if(strpos($route, $page) !== false){
            $show = true;
            break;
        }
    }
} else {
    $show = true;
}


?>

<?php if($show):?>
<div id="visual-block" data-id="<?= $model->id; ?>">
    <div class="content-block">
        <?= Html::a('<span class="close-icon">x</span>','#',['class'=>'close-page']) ?>
        <?php if($model->timer && strtotime($model->timer) > strtotime(date('Y-m-d H:i:s'))): ?>
            <div class="timer-block"></div>
        <?php endif; ?>
        <?= $model->content ?>
    </div>
</div>
<?php endif;?>