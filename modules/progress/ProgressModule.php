<?php

namespace app\modules\progress;

class ProgressModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\progress\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
