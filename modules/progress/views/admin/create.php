<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\progress\models\Progress */

$this->title = 'Добавить альбом';
$this->params['breadcrumbs'][] = ['label' => 'Ход строительства', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="progress-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
