<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\progress\models\Progress */

$this->title = 'Редактировать альбом: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ход строительства', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
?>
<div class="progress-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
