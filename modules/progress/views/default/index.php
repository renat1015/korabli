<?php

use app\modules\progress\models\Progress;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Progress */

$this->title = 'Progresses';
$this->params['breadcrumbs'][] = $this->title;
$subItems = null;
if ($models) {
    $subItems = [];
    foreach ($models as $model) {
        $images = $model->getImages(277, 165);
        $imgList = [];
        $i = 0;
        foreach ($images as $image) {
            $i += 1;
            if($i == 1) continue;
            $imgList[] = $image['fullSizeUrl'];
        }

        $item_content = $images[0]['fullSizeUrl'];

        $subItems[] = [
            'content' => $item_content,
            'title' => $model->title,
            'date' => $model->created_at,
            'small_date' => Yii::$app->formatter->asDate($model->created_at,'dd.MM'),
            'images' => $imgList
        ];
    }
}
//$this->context->galleryMenuItems = [
//    'label'   => Yii::t('menu', 'Галерея'),
//    'url'     => Url::to(['/site/gallery/construction-progress']),
//    'options' => ['class'=>'with-sub cp-gallery'],
//    'items'   => $subItems,
//];
$this->context->headerAdvanceClass = 'header-dynamics';
?>
<div class="dynamics">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="title">
        <h3>Динамика строительства</h3>
        <div class="year">
            <?php foreach ($years as $year):?>
                <a
                        href="<?= \yii\helpers\Url::to(['/site/gallery/construction-progress', 'year' => $year['m_date']])?>"
                    <?= ($main_year == $year['m_date']) ? 'class="active"' : ''; ?>
                ><?= $year['m_date'];?></a>
            <?php endforeach;?>
        </div>
    </div>
    <div class="dynamics-slider">
        <div class="dynamics-slider-inner <?= ($subItems <= 3) ? 'with-loop' : 'no-loop' ?> parent-container owl-carousel">
            <?php
            $i = 0;
            foreach ($subItems as $item): ?>
                <?php
                $i++;
                ?>
                <div class="item">
                    <a href="<?= $item['content'];?>" class="image">
                        <img src="<?= $item['content'];?>" alt="img">

                        <span class="hover"></span>
                    </a>
                    <div class="author">Размещено:
                        <span class="us-name"><?= $item['title'];?></span>
                        <span class="ch-date"><?= $item['date'];?></span>
                    </div>
                    <div class="date"><?= $item['small_date'];?></div>

                    <div class="hidden">
                        <?php foreach ($item['images'] as $image) :?>
                            <a href="<?= $image;?>">
                                <img src="<?= $image;?>" alt="img">
                            </a>
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
        <div class="btn-muve">
            <img src="/img/muve.png" alt="img">
            <span>Онлайн камера</span>
        </div>
    </div>
    <div class="online-muve">
        <div class="muve">
            <a href="#" class="btn-menu-close">
                <span class="btn-close"></span> <span>Закрыть</span>
            </a>
            <div class="lincks">
                <?php
                $i = 0;
                foreach (Yii::$app->params as $key => $param):
                    $pos = strpos($key,'camera');
                    if($pos === false) continue;
                    $i++;

                    ?>
                    <?php if($param != '#'):
                        $data = explode('|', $param);
                    ?>
                        <div class="cam <?php if($i == 1): ?>active<?php endif;?>" href="#">
                            <span><?= isset($data[1]) ? $data[1] : 'Камера'; ?></span>
                            <div id="cam1">
                                <img id="webimg_<?= $data[0];?>" src="<?= $data[0];?>">
                                <img class="hidden_image" src="<?= $data[0];?>">
                                <script>
                                    setInterval(function(){
                                        var img = document.getElementById("webimg_<?= $data[0];?>");
                                        img.src = '<?= $data[0];?>?img' +Math.random(0,100)+'';
                                        function hide (){

                                            webc.style.display = 'block'
                                        }
                                        }, 4000);
                                </script>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="image">
        <img src="/img/slide8.jpg" alt="img">
    </div>

    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>

<?php
//$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
//$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',['depends'=>'yii\web\JqueryAsset']);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/magnific-popup.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jquery.magnific-popup.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/owl.carousel.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/owl.carousel.min.js', ['depends' => 'yii\web\JqueryAsset']);


$redirect_link = Url::to(['/site/gallery']);

if(Yii::$app->request->get('camera') == 'show'){
    $this->registerJs(<<<JS
    
        $('.btn-muve').trigger('click');

//     $('.cp-gallery').on('click','> span, > ul .close-icon',function(){
//        window.location='$redirect_link';
//    });
//
//    $('.open-cp-gallery').click(function(e){
//        e.preventDefault();
//        var galleryArray = [];
//        $(this).parent().find('.img-list img').each(function(i,e){
//            galleryArray.push({href:$(e).attr('src')});
//        });
//        $.fancybox.open(galleryArray, {
//            padding : 0,
//            maxWidth: 1200,
//            tpl: {
//                wrap : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
//                image : '<img class="fancybox-image" src="{href}" alt="" />',
//                closeBtn : '<a title="Закрыть" class="close-icon" href="javascript:;"></a>',
//                next : '<a title="Предидущая" class="cp-gallery-nav next" href="javascript:;"><span></span></a>',
//                prev : '<a title="Следидущая" class="cp-gallery-nav prev" href="javascript:;"><span></span></a>'
//            }
//        });
//    });
//
//    $('.cp-gallery > ul').mCustomScrollbar();
//    $('.cp-gallery > ul').append('<span class="close-icon"></span>');
JS
    );
}


?>
