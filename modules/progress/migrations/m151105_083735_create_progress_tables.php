<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_083735_create_progress_tables extends Migration
{
    public function up()
    {
        $this->createTable('progress',[
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(). ' COMMENT "Назвение"',
            'status' => $this->smallInteger()->defaultValue(1).' COMMENT "Публиковать"',
            'updated_at' => $this->timestamp()->notNull() . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата обновления"',
            'created_at' => $this->timestamp() . ' COMMENT "Дата добавления"',
        ]);
    }

    public function down()
    {
        $this->dropTable('progress');
    }
}
