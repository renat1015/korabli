<?php

namespace app\modules\progress\controllers;

use app\common\BaseController;
use app\modules\progress\models\Progress;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * DefaultController implements the CRUD actions for Progress model.
 */
class DefaultController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Progress models.
     * @return mixed
     */
    public function actionIndex($year=null)
    {
//        $models = Progress::getDb()->cache(function(){
//            return Progress::find()->where(['status'=>Progress::STATUS_PUBLISHED])->all();
//        },Progress::DEFAULT_CACHE_DURATION,Progress::getDbDependency());

        $query = Progress::find();
        if(!Yii::$app->request->get('year'))
            $query->orderBy('created_at DESC');
        else
            $query->where("DATE_FORMAT(created_at,'%Y') = '$year'");

        $models = $query->all();

        $query='SELECT DATE_FORMAT(created_at, "%Y") as m_date FROM progress GROUP BY m_date ORDER BY m_date DESC;';
        $years = Yii::$app->db->createCommand($query)->queryAll();
        $main_year = $year;

        return $this->render('index', [
            'models' => $models,
            'years' => $years,
            'main_year' => $main_year
        ]);
    }

    /**
     * Displays a single Progress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Progress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Progress();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Progress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Progress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Progress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Progress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Progress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
