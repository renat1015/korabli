<?php

namespace app\modules\progress\models;

use app\common\BaseModel;
use app\modules\image\behaviors\ImagesBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "progress".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 */
class Progress extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'progress';
    }

    public $imgFiles;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Автор',
            'imgFiles' => 'Изображения',
            'status' => 'Публиковать',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors,[
            'imagesBehavior' => [
                'class'     => ImagesBehavior::className(),
                'model'     => strtolower(self::getModelName()),
                'attribute' => 'imgFiles',
            ],
        ]);
    }
}
