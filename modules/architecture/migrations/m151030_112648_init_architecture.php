<?php

use yii\db\Schema;
use yii\db\Migration;

class m151030_112648_init_architecture extends Migration
{
    public function up()
    {
        $this->createTable('architecture', [
            'id'         => $this->primaryKey(),
            'title'      => $this->string(255)->notNull(),
            'text'       => $this->text(),

            'image'      => $this->string(255)->notNull(),

            'ordering'   => $this->integer()->defaultValue('0'),
            'status'     => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);
    }

    public function down()
    {
        $this->dropTable('architecture');
    }
}
