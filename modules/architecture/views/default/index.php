<?php
use app\modules\architecture\models\Architecture;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 03.11.15
 * Time: 10:50
 * @var $this View
 * @var $model Architecture
 */
$this->context->sidebarAdvancedClass = 'closed';
$this->registerJs(<<<JS
// function runArchSlider()
// {
//     var autoPlay = false,
//         autoPlayDelay = 10000,
//         timer,
//         sliderItemClass = '.item',
//         sliderBlock = $('.architecture-index'),
//         innerBlock = $('.inner',sliderBlock),
//         sliderItems = $('.items',innerBlock),
//         itemsCount = $(sliderItemClass,sliderItems).size()
//         previewBlock = $('.preview-list',sliderBlock),
//         isAnimated = false;
//
//     $(innerBlock).css({height:$(window).height()});
//
//     if(autoPlay==true)
//     {
//         playSlider(autoPlayDelay);
//     }
//
//     function playSlider(delay)
//     {
//         if(autoPlay)
//         {
//             timer = window.setInterval(function(){
//                 if(!isAnimated){
//                     isAnimated = true;
//                     slide('right');
//                     isAnimated = false;
//                 }
//             },delay);
//         }
//     }
//
//     $(sliderBlock).on('click','.button',function()
//     {
//         if(!$('.item',sliderItems).is(':animated'))
//         {
//             if($(this).hasClass('right'))
//                 slide('right');
//             else
//                 slide('left');
//         }
//         return false;
//     });
//
//     function stopSlider()
//     {
//         window.clearInterval(timer);
//     }
//
//     function slide(direction)
//     {
//         if(direction=='left')
//         {
//             if(!$('.item',sliderItems).is(':animated'))
//             {
//                 stopSlider();
//                 sliderItems.find(sliderItemClass+':last').fadeIn(1000,function(){
//                     $(this).prependTo(sliderItems).next().hide();
//                 });
//                 switchButton(direction);
//                 playSlider(autoPlayDelay);
//             }
//             return false;
//         }
//         if(direction=='right')
//         {
//             if(!$('.item',sliderItems).is(':animated'))
//             {
//                 stopSlider();
//                 sliderItems.find(sliderItemClass+':first').next().fadeIn(1000,function(){
//                     $(this).prev().appendTo(sliderItems).hide();
//                 });
//                 switchButton(direction);
//                 playSlider(autoPlayDelay);
//             }
//             return false;
//         }
//     }
//
//     function switchButton(direction)
//     {
//         var activeEl = $('.active',previewBlock);
//         if(direction=='left')
//         {
//             activeEl.removeClass('active');
//             if($(activeEl).prev('.item').length==1)
//             {
//                 $(activeEl).prev().addClass('active');
//             }
//             else
//             {
//                 $('li.item',previewBlock).last().addClass('active');
//             }
//         }
//         if(direction=='right')
//         {
//             activeEl.removeClass('active');
//             if($(activeEl).next('.item').length==1)
//             {
//                 $(activeEl).next().addClass('active');
//             }
//             else
//             {
//                 $('li.item',previewBlock).first().addClass('active');
//             }
//         }
//     }
//
//     $('.preview',previewBlock).click(function(){
//         if(!$(this).hasClass('active'))
//         {
//             stopSlider();
//
//             var previewItem = $(this).parent(),
//                 slideNum = $(previewItem).index(),
//                 isLastEl = false,
//                 maxCount = itemsCount + 1,
//                 firstElNum = itemsCount - maxCount,
//                 count = slideNum;
//
//             if(!$('.item',sliderItems).is(':animated'))
//             {
//                 $('.active',previewBlock).removeClass('active');
//
//                 if(itemsCount==2)
//                 {
//                     sliderItems.find('.slide'+slideNum,sliderItems).fadeIn(1000,function(){
//                         $(this).prependTo(sliderItems).next().hide();
//                         $(previewItem).addClass('active');
//                     });
//                 }
//                 else{
//                     if(slideNum == itemsCount)
//                     {
//                         isLastEl = true;
//                         maxCount = itemsCount - 1;
//                         firstElNum = itemsCount - maxCount;
//                         count = firstElNum;
//                     }
//
//                     sliderItems.find('.slide'+slideNum,sliderItems).fadeIn(1000,function(){
//                         $('.item',sliderItems).not(this).hide();
//                         for (var i = count; i < maxCount; i++)
//                         {
//                             if(isLastEl)
//                             {
//                                 $(".slide"+slideNum,sliderItems).prependTo(sliderItems);
//                                 if(i == firstElNum)
//                                     $(".slide"+i,sliderItems).insertAfter(".slide"+slideNum,sliderItems);
//                                 else
//                                     $(".slide"+i,sliderItems).insertAfter(".slide"+(i-1),sliderItems);
//                             }
//                             else
//                             {
//                                 if(i==slideNum)
//                                 {
//                                     $(".slide"+i,sliderItems).prependTo(sliderItems);
//                                 }
//                                 else
//                                 {
//                                     var num = i-1;
//                                     $(".slide"+i,sliderItems).insertAfter(".slide"+num,sliderItems);
//                                 }
//                             }
//                         }
//                     });
//                     $(previewItem).addClass('active');
//                 }
//             }
//             playSlider(autoPlayDelay);
//         }
//     });
//
//     if(itemsCount>1)
//     {
//         autoPlay = true;
//         $(sliderBlock).prepend('<span class="left button"></span>').append('<span class="right button"></span>');
//     }
//
//     playSlider(autoPlayDelay);
//
//     $(window).resize(function(){
//         $(innerBlock).css({height:$(window).height()});
//     });
// }
//
// runArchSlider();
JS
);
?>

<div class="kor-home galery">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="title">
        <h3>Галерея</h3>
    </div>
    <div class="slick-control">
        <a href="#" class="arrow prev"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
        <a href="#" class="arrow next"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
    </div>
    <div class="slide-num">
        <div class="this-slide-num">1</div>/<div class="all-slide-length"></div>
    </div>
    <div class="slick main-owl galery">
        <?php $i=1; foreach($models as $model): ?>
            <div>
                <img src="<?= $model->getImage(); ?>" alt="img">
            </div>
        <?php $i++; endforeach; ?>
    </div>
    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>
