<?php
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 03.11.15
 * Time: 10:49
 */

namespace app\modules\architecture\controllers;


use app\common\BaseController;
use app\common\BaseModel;
use app\modules\architecture\models\Architecture;

class DefaultController extends BaseController
{
    public function actionIndex()
    {
        $models = Architecture::getDb()->cache(function(){
            return Architecture::find()->where(['status'=>Architecture::STATUS_PUBLISHED])->all();
        },BaseModel::DEFAULT_CACHE_DURATION,Architecture::getDbDependency());

        return $this->render('index',['models'=>$models]);
    }
}