<?php

namespace app\modules\architecture\models;

use app\common\BaseModel;
use app\modules\image\models\Image;
use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $ordering
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 * @property string $image
 */
class Architecture extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'architecture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['text'], 'string'],
            [['ordering', 'status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название слайда',
            'text' => 'Текст слайда',
            'ordering' => 'Сортировка',
            'status' => 'Публиковать',
            'image' => 'Изображения для слайда',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/architecture/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/architecture/' . $file_name);
            $this->image = $file_name;

            if(!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/architecture/' . $this->oldAttributes['image']);

        } else {
            if(!$insert)
                $this->image = $this->oldAttributes['image'];
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function getImage($width=null,$height=null)
    {
        if(!$this->image)
            return false;
        $model_base_path = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/architecture/';
        $base_file_path = $model_base_path . $this->image;
        if(file_exists($base_file_path))
        {
            if(!$width && !$height)
                return '/uploads/architecture/' . $this->image;
            else
            {
                return Image::getThumb($width,$height,self::getModelName(),$this->id,$this->image,[
                    'original_file_path' => '/uploads/architecture/'.$this->image,
                    'base_path' =>  '/uploads/architecture/'.$this->id.'/'
                ]);
            }
        } else
            return false;
    }

    /**
     * @return mixed
     */
    public static function getLastOrder()
    {
        $last_item = self::find()->orderBy('ordering DESC')->one();

        return $last_item->ordering + 1;
    }
}
