<?php

namespace app\modules\architecture\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ArchitectureSearch represents the model behind the search form about `app\modules\architecture\models\Architecture`.
 */
class ArchitectureSearch extends Architecture
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ordering', 'status'], 'integer'],
            [['title', 'text', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Architecture::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ordering' => $this->ordering,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
