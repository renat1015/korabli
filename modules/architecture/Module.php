<?php

namespace app\modules\architecture;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\architecture\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
