<?php

use app\modules\contacts\models\ContactsCallrSearch;
use app\modules\realty\models\RealtyFlat;
use yii\grid\GridView;

?>

<div class="col-lg-2">
    <div class="admin-default-index">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= RealtyFlat::find()->where(['sale_status' => RealtyFlat::SALE_STATUS_IN_SALE])->count() ?></h3>

                <p>В продаже</p>
            </div>
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
            <a href="/realty/flat?RealtyFlatSearch%5Bsale_status%5D=1" class="small-box-footer">
                Перейти <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>
<div class="col-lg-2">
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?= RealtyFlat::find()->where(['sale_status' => RealtyFlat::SALE_STATUS_RESERVED])->count() ?></h3>

            <p>Зарезервировано</p>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
        <a href="/realty/flat?RealtyFlatSearch%5Bsale_status%5D=3" class="small-box-footer">
            Перейти <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-2">
    <div class="small-box bg-green">
        <div class="inner">
            <h3><?= RealtyFlat::find()->where(['sale_status' => RealtyFlat::SALE_STATUS_SALE])->count() ?></h3>

            <p>Продано</p>
        </div>
        <div class="icon">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <a href="/realty/flat?RealtyFlatSearch%5Bsale_status%5D=2" class="small-box-footer">
            Перейти <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-6">
    <!--Currency Converter widget by FreeCurrencyRates.com -->

    <div id='gcw_mainFI5RtJiAw' class='gcw_mainFI5RtJiAw'></div>
    <a id='gcw_siteFI5RtJiAw' href='http://freecurrencyrates.com/ru/'>FreeCurrencyRates.com</a>
    <script>function reloadFI5RtJiAw(){ var sc = document.getElementById('scFI5RtJiAw');if (sc) sc.parentNode.removeChild(sc);sc = document.createElement('script');sc.type = 'text/javascript';sc.charset = 'UTF-8';sc.async = true;sc.id='scFI5RtJiAw';sc.src = 'http://freecurrencyrates.com/ru/widget-vertical?iso=USDEURGBPCNY&df=2&p=FI5RtJiAw&v=fits&source=cbr&width=245&width_title=0&firstrowvalue=1&thm=A6C9E2,FCFDFD,4297D7,5C9CCC,FFFFFF,C5DBEC,FCFDFD,2E6E9E,000000&title=%D0%9A%D0%BE%D0%BD%D0%B2%D0%B5%D1%80%D1%82%D0%B5%D1%80%20%D0%B2%D0%B0%D0%BB%D1%8E%D1%82&tzo=-120';var div = document.getElementById('gcw_mainFI5RtJiAw');div.parentNode.insertBefore(sc, div);} reloadFI5RtJiAw(); </script>
    <!-- put custom styles here: .gcw_mainFI5RtJiAw{}, .gcw_headerFI5RtJiAw{}, .gcw_ratesFI5RtJiAw{}, .gcw_sourceFI5RtJiAw{} -->
    <!--End of Currency Converter widget by FreeCurrencyRates.com -->
</div>

<div class="col-lg-12">
    <hr>
</div>
<div class="col-lg-4">
    <?php
        $searchModel = new ContactsCallrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'name',
            'phone',
            'time',
        ],
    ]); ?>
</div>
<div class="col-lg-4">
    <div>
        <?php
        $events = [];

        $clients = \app\modules\realty\models\RealtyClient::find()->all();

        if($clients)
        {
            foreach ($clients as $client) {
                $Event = new \yii2fullcalendar\models\Event();
                $Event->id = $client->id;
                $Event->title = $client->full_name;
                $Event->url = \yii\helpers\Url::toRoute(['/realty/client/view', 'id' => $client->id]);
                $Event->start = date('Y-m-d\TH:i:s\Z', strtotime($client->next_date));
                $events[] = $Event;
            }
        }

        ?>

        <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
            'events'=> $events,
        ));

        ?>
        <hr>
    </div>
</div>







