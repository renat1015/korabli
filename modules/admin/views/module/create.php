<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Module */
$list = \app\modules\admin\models\Module::getModulesToInstall();

$this->title = 'Create Module';
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $list,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'description',
            'class',
            'system',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{install}',
                'buttons' => [
                    'install' => function ($url, $model) {
                        return Html::a('<i class="fa fa-cubes"></i> Install module', '/admin/module/install?module='.$model['name'], [
                            'title' => Yii::t('app', 'Install module'),
                        ]);
                    },
                ],

            ],
        ],
    ])?>


</div>
