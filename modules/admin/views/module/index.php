<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ModuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Install module', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'title',
            'description:ntext',
            'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{config} {delete}',
                'buttons' => [
                    'config' => function($url, $model)
                    {
                         return Html::a('<i class="fa fa-cogs"></i>', ['/admin/module/view', 'id' => $model->id]);
                    },
                    'delete' => function($url, $model)
                    {
                        if($model->system == 1)
                            return false;
                        $options = [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];

                        return Html::a('<i class="fa fa-trash-o"></i>', $url, $options);
                    }
                ],

            ],
        ],
    ]); ?>

</div>
