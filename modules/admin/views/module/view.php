<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Module */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="module-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(count($model->config)>0) {?>
    <div class="module-form">
        <?php $form = ActiveForm::begin([
            'action' => \yii\helpers\Url::to(['/admin/module/config'])
        ]); ?>

            <?= \app\modules\admin\models\Module::buildConfigFields($model); ?>

            <?= Html::input('hidden', 'id', $model->id)?>

            <div class="form-group">
                <?= Html::submitButton('Save config', ['class' => 'btn btn-success']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
    <?php } else {
        echo Yii::t('app', 'the module has not setting');
    }?>

</div>
