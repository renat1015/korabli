<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%modules}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property integer $system
 * @property string $config
 * @property string $version
 * @property string $updated_at
 * @property string $created_at
 */
class Module extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%modules}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title', 'description'], 'required'],
            [['description', 'config'], 'string'],
            [['system'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'title', 'version'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'description' => 'Description',
            'system' => 'System',
            'config' => 'Config',
            'version' => 'Version',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     *
     */
    public function afterDelete()
    {
        Migration::migrate('down', $this->name);

        parent::afterDelete();
    }

    /**
     * @param $module
     *
     * @return bool
     */
    public static function installModule($module, $migrate = true)
    {
        $config_file = Yii::$app->basePath . DIRECTORY_SEPARATOR .'modules' .DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'config.json';
        if(file_exists($config_file))
        {
            if($migrate)
                $res = Migration::migrate('up', $module);
            else
                $res = 0;

            if($res == 0)
            {
                $json = file_get_contents($config_file);
                $data = json_decode($json, true);

                self::deleteAll(['name' => $module]);

                $model = new self();
                $model->name        = $module;
                $model->title       = $data['title'];
                $model->description = $data['description'];
                $model->config      = serialize(isset($data['config']) ? $data['config'] : []);
                $model->version     = isset($data['version']) ? $data['version'] : 'null';
                $model->system      = $data['system'];

                $model->validate();

                if($model->save())
                    return true;
            }
        }

        return false;
    }

    //    modules
    //

    /**
     * get directories list in /core/modules
     * @return array|bool
     */
    public static function getModules()
    {
        $dir = Yii::$app->basePath . "/modules/";
        $modules = scandir($dir);
        if(!$modules)
            return false;

        unset($modules[0]);
        unset($modules[1]);

        return $modules;

    }

    /**
     * @return array
     */
    public static function getJsonModulesData($only_installed = true)
    {
        $list = self::getModules();
        $modules = [];

        if($only_installed)
        {
            $i_modules = self::find()->select('name')->asArray()->all();
            $installed = [];
            if($i_modules)
                foreach ($i_modules as $mod) {
                    $installed[] = $mod['name'];
                }
        }

        foreach ($list as $module) {
            $data = false;
            if($only_installed && in_array($module, $installed))
                $data = self::getModuleJsonData($module);
            elseif(!$only_installed)
                $data = self::getModuleJsonData($module);

            if($data)
            {
                $data['name'] = $module;
                $modules[$module] = $data;
            }
        }

        usort($modules, "cmp");

        $list = false;
        foreach ($modules as $module) {
            $list[$module['name']] = $module;
        }

        return $list;
    }

    /**
     * @param $module
     *
     * @return bool|mixed
     */
    public static function getModuleJsonData($module)
    {
        $conf = Yii::$app->basePath.'/modules/'.$module.'/config.json';
        if(file_exists($conf)) {
            $json = file_get_contents($conf);
            $data = json_decode($json, true);
            return $data;
        }

        return false;
    }

    /**
     * @param $data
     *
     * @return array
     */
    public static function getMenuDataWithModulesData($data)
    {
        $menu = [];

        foreach ($data as $item) {
            if(isset($item['menu']))
            {
                foreach ($item['menu'] as $module => $menu_data) {
                    if(!isset($menu[$module]))
                        $menu[$module] = $menu_data;
                    else
                        $menu[$module][] = $menu_data[0];
                }
            }
        }

        return $menu;
    }

    /**
     * @param $menu_items
     * @param $modules_data
     *
     * @return array
     */
    public static function buildMenuWithArray($menu_items, $modules_data)
    {
        $menu = [];

        foreach ($menu_items as $module => $menu_item) {
            $data['title'] = $modules_data[$module]['title'];
            $data['href'] = isset($modules_data[$module]['href']) ? $modules_data[$module]['href'] : "#";
            $data['icon'] = isset($modules_data[$module]['icon']) ? $modules_data[$module]['icon'] : false;

            foreach ($menu_item as $item) {
                $sub_item['title'] = $item['title'];
                $sub_item['href'] = $item['href'];
                $sub_item['icon'] = $item['icon'];
                $data['items'][] = $sub_item;
            }

            $menu[] = $data;
            unset($data);
        }

        return $menu;
    }

    /**
     * @return array
     */
    public static function getModulesToInstall()
    {
        $list = [];
        $modules = self::getModules();
        $installed_modules  = self::find()->all();

        foreach ($installed_modules as $module) {
            if(($key = array_search($module->name, $modules)) !== false)
                unset($modules[$key]);
        }

        foreach ($modules as $key => $module) {
            $data = self::getModuleJsonData($module);
            if($data){
                unset($data['menu']);
                $data['name'] = $module;
                $list[] = $data;
            }
        }

        return $list;
    }


//
    public function afterFind()
    {
        $this->config = unserialize($this->config);
        parent::afterFind();
    }

    /**
     * @param $module
     *
     * @return string
     */
    public static function buildConfigFields($module)
    {
        $html = '';

        foreach ($module->config as $key=>$config) {

            $html .= Html::label($config['label'], $key .'_md', ['class' => 'control-label']);
            $html .= Html::input($config['type'], $key, $config['value'] ? $config['value'] : $config['defaultValue'], ['class' => 'form-control', 'id' => $key .'_md']);
            $html = Html::tag('div', $html, ['class' => 'form-group']);
        }

        return $html;
    }


    /**
     * @param $module
     *
     * @return bool|mixed
     */
    public static function getConfig($module)
    {
        $module = self::find()->where(['name' => $module])->one();
        if(!$module)
            return false;

        return $module->config;
    }

}
