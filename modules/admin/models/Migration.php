<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Exception;
use yii\console\Application;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "migration".
 *
 * @property string $version
 * @property integer $apply_time
 */
class Migration extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'migration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['version'], 'required'],
            [['apply_time'], 'integer'],
            [['version'], 'string', 'max' => 180]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'version' => 'Version',
            'apply_time' => 'Apply Time',
        ];
    }

    /**
     * @param string $action
     * @param bool   $module
     *
     * @return int
     */
    public static function migrate($action = 'up', $module = false)
    {
        $oldApp = Yii::$app;

        defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));
        defined('STDOUT') or define('STDOUT', fopen('php://stdout', 'w'));

        /** @noinspection PhpIncludeInspection */
        $config = require(Yii::getAlias('@app/config/console.php'));
        $consoleApp = new Application($config);

        $migration_params = [
            'interactive' => false,
        ];

        if ($module)
            $migration_params[ 'migrationPath' ] = '@app/modules/' . $module . '/migrations';

        try {
            Yii::$app->set('db', $oldApp->db);

            ob_start();

            $exitCode = $consoleApp->runAction(
                'migrate/' . $action,
                $migration_params
            );

            if($module=='user')
            {
                $consoleApp->runAction('user/install/set-roles');
            }

            $result = ob_get_clean();

            Yii::trace($result, 'console');

        } catch (Exception $e) {
            Yii::warning($e->getMessage() . ' in ' . $e->getFile() . ' on line ' . $e->getLine(), 'console');
            $exitCode = 1;
        }

        Yii::$app = $oldApp;

        return $exitCode;
    }


    /**
     * @return array|bool
     */
    public static function getModuleList()
    {
        $dir = $file = Yii::$app->basePath.'/modules/';
        $modules = scandir($dir);

        if(!$modules)
            return false;

        unset($modules[0], $modules[1]);

        return $modules;

    }
}
