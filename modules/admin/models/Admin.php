<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "migration".
 *
 * @property string $version
 * @property integer $apply_time
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function buildMenu()
    {
        $modules_data = Module::getJsonModulesData();
        $menu_data = Module::getMenuDataWithModulesData($modules_data);

        $menu = Module::buildMenuWithArray($menu_data, $modules_data);

        $html = '';
        foreach ($menu as $menu_item) {
            $html .= self::buildList($menu_item);
        }

        return $html;
    }

    /**
     * @param $menu_item
     *
     * @return string
     */
    public static function buildList($menu_item)
    {
        $title  = '';
        $items  = '';

        if(isset($menu_item['icon']))
            $title .= Html::tag('i', '', ['class' => $menu_item['icon']]);

        $title .= Html::tag('span', $menu_item['title']);

        if(isset($menu_item['label']))
            $title .= Html::tag('i', $menu_item['label']['value'], ['class' => $menu_item['label']['class']]);

        $a = Html::a($title, $menu_item['href']);

        if(isset($menu_item['items']))
            $items = self::buildSubMenu($menu_item['items']);

        return Html::tag('li', $a . $items, [ 'class' => (isset($menu_item['items']) ? 'treeview' : '') .' '. (self::checkMenuItemActive($menu_item['href']) ? 'active' : '')]);

    }

    /**
     * @param        $items
     * @param string $html
     *
     * @return string
     */
    public static function buildSubMenu($items, $html = '')
    {
        foreach ($items as $item) {
            $html .= self::buildList($item);
        }

        return Html::tag('ul', $html, ['class' => 'treeview-menu']);
    }

    static function checkMenuItemActive($url)
    {
        $requestUrl = strtok(Yii::$app->request->url,'?');

        if(Yii::$app->request->url == $url || $requestUrl==$url)
            return true;

        $requestUrlArray = explode('/',$requestUrl);
        if(is_array($requestUrlArray))
        {
            if(substr($url,1)==$requestUrlArray[1])
                return true;
            if(isset($requestUrlArray[2]))
            {
                if(substr($url,1) == $requestUrlArray[1].'/'.$requestUrlArray[2])
                    return true;
            }
        }

        return false;
    }
}