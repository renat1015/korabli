<?php

namespace app\modules\admin\controllers;

use app\common\BaseAdminController;

class DefaultController extends BaseAdminController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
