<?php

use yii\db\Schema;
use yii\db\Migration;

class m150603_112857_init_modules_table extends Migration
{
    public function up()
    {
        $this->createTable('modules',[
            'id'            => Schema::TYPE_PK,
            'name'         => Schema::TYPE_STRING . ' not null',
            'title'         => Schema::TYPE_STRING . ' not null',
            'description'   => Schema::TYPE_TEXT . ' not null',
            'system'        => Schema::TYPE_SMALLINT . ' default "0"',
            'config'        => Schema::TYPE_TEXT,
            'version'       => Schema::TYPE_STRING,
            'updated_at'    => Schema::TYPE_TIMESTAMP . ' not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'    => Schema::TYPE_TIMESTAMP . ' not null',
        ]);
    }

    public function down()
    {
        $this->dropTable('modules');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
