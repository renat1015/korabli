<?php

use yii\db\Schema;

class m140617_091138_create_page_table extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('page', [
           'id' => Schema::TYPE_PK,

            'title'         => 'string not null',
            'content'       => 'text not null',

            'updated_at'    => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'    => 'timestamp not null',
        ]);
    }

    public function down()
    {
        $this->dropTable('page');
    }
}
