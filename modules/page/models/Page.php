<?php

namespace app\modules\page\models;

use app\common\BaseModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use app\modules\seo\models\Seo;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $updated_at
 * @property string $created_at
 */
class Page extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title'         => Yii::t('page', 'Название'),
            'content'       => Yii::t('page', 'Контент'),
            'updated_at'    => Yii::t('page', 'Дата обновления'),
            'created_at'    => Yii::t('page', 'Дата добавления'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
            'seo' => [
                'class'             => 'app\modules\seo\behaviors\SeoBehavior',
                'model'             => $this->getModelName(),
                'view_category'     => '',
                'view_action'       => 'page/page/view'
            ],
        ];
    }

    /**
     * @return static
     */
    public function getSeo()
    {
        return $this->hasOne(Seo::className(), [ 'model_id' => 'id' ])->where([ 'model_name' => $this->getModelName()]);
    }
}
