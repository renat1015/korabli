<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\seo\widgets\SeoWidget;

/**
 * @var yii\web\View $this
 * @var app\modules\page\models\Page $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= SeoWidget::widget(['model' => $model]);?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'content')->widget(\yii\imperavi\Widget::className(), [
        'options' => [
            'buttonSource' => 'true',
            'minHeight' => 350,
            'imageUpload' => '/page/admin/upload',
            'uploadImageFields' => [
                Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()
            ],
            'uploadFileFields' => [
                Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()
            ]
        ],
        'plugins' => [
            'fullscreen',
            'clips',
            'fontcolor',
            'fontfamily',
            'fontsize',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
