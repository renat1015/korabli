<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\page\models\Page $model
 */

$this->title = Yii::t('page', 'Редактировать старнницу') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('page', 'Статические страницы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => $model->seoUrl];
$this->params['breadcrumbs'][] = Yii::t('page', 'Редактировать старнницу');
?>
<div class="page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
