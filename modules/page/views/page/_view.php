<?php
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 30.10.15
 * Time: 14:40
 * @var $model Page
 * @var $this View
 */
use app\modules\page\models\Page;
use yii\helpers\Html;
use yii\web\View;
//$this->context->sidebarAdvancedClass = 'full-color hide-sub';
//$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
//$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',['depends'=>'yii\web\JqueryAsset']);

$this->registerJs(<<<JS
    // $('body').css('overflow','hidden');
    //  if(isMobileBrowser) {
    //     $('.page-modal').css({width:$(document).width(), height: $(document).height()});
    //     $(window).on('resize',function(e){
    //         $('.page-modal').css({width:$(document).width(), height: $(document).height()});
    //     });
    // }
    // $('.content-block .text').mCustomScrollbar();
    // $('table tr td img').addClass('img-responsive');
JS
);
?>


<div class="kor-documents about-firm">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="documents">
        <h3><?= $model->title ?></h3>
        <div class="docs">
            <?= $model->content ?>
        </div>
    </div>
    <?= \app\modules\siteBackground\widgets\BackgroundWidget::widget() ?>

    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>
