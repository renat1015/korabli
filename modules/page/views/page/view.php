<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\modules\page\models\Page $model
 */

$this->title = Html::encode($model->title);
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(<<<JS
    $('body').css('overflow','hidden');
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('.page-modal').css({width:$(document).width(), height: $(document).height()});
        $(window).on('resize',function(e){
            $('.page-modal').css({width:$(document).width(), height: $(document).height()});
        });
    }
JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <div class="page-view">
            <h1><?= $this->title?></h1>
            <div>
                <?= $model->content?>
            </div>
        </div>
    </div>
</div>