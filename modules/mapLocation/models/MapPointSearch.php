<?php

namespace app\modules\mapLocation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\mapLocation\models\MapPoint;

/**
 * MapPointSearch represents the model behind the search form about `app\modules\mapLocation\models\MapPoint`.
 */
class MapPointSearch extends MapPoint
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'point_type', 'status'], 'integer'],
            [['title', 'image', 'description', 'latlng', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MapPoint::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'point_type' => $this->point_type,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'latlng', $this->latlng]);

        return $dataProvider;
    }
}
