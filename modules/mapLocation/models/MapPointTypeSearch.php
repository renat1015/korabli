<?php

namespace app\modules\mapLocation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\mapLocation\models\MapPointType;

/**
 * MapPointTypeSearch represents the model behind the search form about `app\modules\mapLocation\models\MapPointType`.
 */
class MapPointTypeSearch extends MapPointType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_main', 'status'], 'integer'],
            [['title', 'image', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MapPointType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_main' => $this->is_main,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
