<?php

namespace app\modules\mapLocation\models;

use app\common\BaseModel;
use app\extensions\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "map_point".
 *
 * @property integer $id
 * @property integer $point_type
 * @property string $title
 * @property string $image
 * @property string $description
 * @property string $latlng
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property MapPointType $pointType
 */
class MapPoint extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_type', 'title','description', 'latlng'], 'required'],
            [['point_type', 'status'], 'integer'],
            [['description'], 'string'],
            [['updated_at', 'created_at', 'image'], 'safe'],
            [['title', 'image', 'latlng'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_type' => 'Тип метки',
            'title' => 'Название',
            'image' => 'Изображение',
            'description' => 'Описание',
            'latlng' => 'Координаты на карте',
            'status' => 'Публиковать',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }


    public function behaviors()
    {
        return [
            'timestamp'      => [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value'      => new Expression('NOW()'),
            ],

            'uploadBehavior' => [
                'class'      => UploadBehavior::className(),
                'attributes' => [
                    'image',
                ],
                'path'       => $this->imagePath(),
                'tempPath'   => $this->imageTempPath(),
            ],

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointType()
    {
        return $this->hasOne(MapPointType::className(), ['id' => 'point_type']);
    }


    /**
     * @return bool|string
     * @throws \yii\base\Exception
     */
    public function imagePath()
    {
        $path = Yii::$app->basePath . '/../uploads/point/' . $this->id . DIRECTORY_SEPARATOR;
        FileHelper::createDirectory($path, 0777);

        return Yii::getAlias($path);
    }

    /**
     * @param null $image
     *
     * @return bool|string
     */
    public static function imageTempPath($image = null)
    {
        $path = Yii::$app->basePath . '/../uploads/tmp/point';
        if ($image !== null) {
            $path .= '/' . $image;
        }

        return Yii::getAlias($path);
    }

    /**
     * @param bool|false $with_file
     *
     * @return string
     */
    public function imageUrl($with_file = false)
    {
        $url = '/uploads/point' . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR;

        if ($with_file)
            $url .= $this->image;

        return $url;
    }
}
