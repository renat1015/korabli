<?php

namespace app\modules\mapLocation\models;

use app\common\BaseModel;
use app\extensions\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "map_point_type".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $is_main
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property MapPoint[] $mapPoints
 */
class MapPointType extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_point_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['is_main', 'status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'image' => 'Изображение',
            'is_main' => 'Главняа метка',
            'status' => 'Публиковтаь',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapPoints()
    {
        return $this->hasMany(MapPoint::className(), ['point_type' => 'id']);
    }


    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/mapLocation/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/mapLocation/' . $file_name);
            $this->image = $file_name;

            if (!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/mapLocation/' . $this->oldAttributes['image']);

        } else {
            if (!$insert)
                $this->image = $this->oldAttributes['image'];
        }

        return parent::beforeSave($insert);
    }


    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/mapLocation/' . $this->image)) {
            return '/uploads/mapLocation/' . $this->image;
        } else
            return false;
    }

    /**
     *
     */
    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/mapLocation/' . $this->image);
        parent::afterDelete();

    }

    /**
     * @return array
     */
    public static function getList()
    {
        $items = self::find()->where(['status' => self::STATUS_PUBLISHED])->all();

        $list = [];
        foreach ($items as $item) {
            $list[$item->id] = $item->title;
        }

        return $list;
    }

}
