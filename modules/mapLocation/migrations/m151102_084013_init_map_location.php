<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_084013_init_map_location extends Migration
{
    public function up()
    {
        $this->createTable('map_point_type', [
            'id'         => $this->primaryKey(),
            'title'      => $this->string(255)->notNull(),
            'image'      => $this->string(255)->notNull(),
            'is_main'    => $this->smallInteger(1)->notNull()->defaultValue('0'),
            'status'     => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);

        $this->createTable('map_point', [
            'id'          => $this->primaryKey(),
            'point_type'  => $this->integer()->notNull(),
            'title'       => $this->string(255)->notNull(),
            'image'       => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'latlng'      => $this->string(255)->notNull(),

            'status'      => $this->smallInteger(1)->defaultValue(1),

            'updated_at'  => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'  => 'timestamp not null',
        ]);

        $this->addForeignKey('fk_point_type_map', 'map_point', 'point_type', 'map_point_type', 'id');

    }

    public function down()
    {
        $this->dropForeignKey('fk_point_type_map', 'map_point');
        $this->dropTable('map_point');
        $this->dropTable('map_point_type');
    }

}
