<?php

namespace app\modules\mapLocation\controllers;

use app\common\BaseAdminController;
use app\common\BaseModel;
use app\extensions\fileapi\actions\UploadAction;
use Yii;
use app\modules\mapLocation\models\MapPoint;
use app\modules\mapLocation\models\MapPointSearch;
use yii\rest\DeleteAction;
use yii\web\NotFoundHttpException;

/**
 * AdminController implements the CRUD actions for MapPoint model.
 */
class AdminController extends BaseAdminController
{

    public function actions()
    {
        return [
            'uploadTempImage' =>
                [
                    'class'     => UploadAction::className(),
                    'path'      => MapPoint::imageTempPath(),
                    'types'     => [
                        'jpg',
                        'png',
                        'gif',
                    ],
                    'minHeight' => 85,
                    'minWidth'  => 85,
                    'maxSize'   => 3145728,
                ],
            'deleteTempImage' =>
                [
                    'class' => DeleteAction::className(),
                    'path'  => MapPoint::imageTempPath(),
                ],
        ];
    }

    /**
     * Lists all MapPoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MapPointSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MapPoint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MapPoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MapPoint();
        $model->status = $model::STATUS_PUBLISHED;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MapPoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MapPoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MapPoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MapPoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MapPoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
