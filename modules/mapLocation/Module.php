<?php

namespace app\modules\mapLocation;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\mapLocation\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
