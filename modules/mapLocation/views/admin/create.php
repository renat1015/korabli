<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\mapLocation\models\MapPoint */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Метки на карте', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-point-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
