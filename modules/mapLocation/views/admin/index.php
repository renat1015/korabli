<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\mapLocation\models\MapPointSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Метки на карте';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-point-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            [
                'attribute' => 'point_type',
                'filter' => \app\modules\mapLocation\models\MapPointType::getList(),
                'value'=>function($data){return $data->pointType->title;}
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
