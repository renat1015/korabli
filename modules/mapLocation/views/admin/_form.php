<?php

use app\extensions\fileapi\FileAPIAdvanced;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\mapLocation\models\MapPoint */
/* @var $form yii\widgets\ActiveForm */
$this->registerCss(<<<CSS

.field-mappoint-image{
    margin-bottom: 50px;
}
.field-mappoint-image .uploader .uploader-preview-cnt{
    margin-top: 0;
}
.field-mappoint-image .uploader .btn-default{
    width: 125px;
    position: absolute;
}
CSS
);
?>

<div class="map-point-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <?= $form->field($model, 'point_type')->dropDownList(\app\modules\mapLocation\models\MapPointType::getList()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image', ['template' => '{label}{input}'])->widget(FileAPIAdvanced::className(), [
        'url'              => $model->imageUrl(),
        'deleteUrl'        => Url::toRoute('/place/admin-category/delete-image?id=' . $model->id),
        'deleteTempUrl'    => Url::toRoute('/place/admin-category/delete-temp-image'),
        'crop'             => true,
        'cropResizeWidth'  => 85,
        'cropResizeHeight' => 85,
        'previewWidth'     => 85,
        'previewHeight'    => 85,
        'settings'         => [
            'url'       => Url::toRoute('uploadTempImage'),
            'imageSize' => [
                'minWidth'  => 85,
                'minHeight' => 85,
            ],
            'preview'   => [
                'el'     => '.uploader-preview',
                'width'  => 85,
                'height' => 85,
            ],
        ],
    ])
    ?>

    <?= $form->field($model, 'description')->widget(\yii\imperavi\Widget::className(), [
        'options' => [
            'buttonSource' => 'true',
            'minHeight'    => 350,
        ],
        'plugins' => [
            'fullscreen',
            'clips',
            'fontcolor',
            'fontfamily',
            'fontsize',
        ],
    ]) ?>


    <?= $form->field($model, 'latlng', ['template' => '{label}<div id="map-canvas" style="width: 100%; height: 500px"></div>{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
        [
            'pluginOptions' =>
                [
                    'size'    => 'small',
                    'onText'  => 'Да',
                    'offText' => 'Нет',
                    'state'   => $model->isNewRecord ? true : $model->status,
                ],
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1ywZ8DEQjcKi5EjV8A61j1iRwnIN5oVw&language=ru"></script>
<script>
    var cn = new google.maps.LatLng(56.3605437, 43.7966659);
    var marker_latlng = new google.maps.LatLng(<?php if($model->isNewRecord) { ?>56.286788, 43.986027<?php } else {
        $cn = explode(',', $model->latlng);
        echo $cn[0], ',' , $cn[1];
     } ?>);
    var marker;
    var map;

    function initialize() {
        var mapOptions = {
            zoom: 14,
            center: cn
        };

        map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: marker_latlng
        });

        google.maps.event.addListener(marker, 'dragend', function () {
            $('#mappoint-latlng').val(marker.getPosition().lat() + ',' + marker.getPosition().lng());
        });

        google.maps.event.addListener(marker, 'click', toggleBounce);

    }

    function toggleBounce() {
        if (marker.getAnimation() != null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

</script>

<?php $this->registerJs('initialize();') ?>
