<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\mapLocation\models\MapPointTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы меток';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-point-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',

            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::tag('div',Html::img($data->getImage()),['class'=>'text-center']);
                },
                'filter' => false,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;{delete}'
            ],
        ],
    ]); ?>

</div>
