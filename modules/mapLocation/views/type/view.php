<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\mapLocation\models\MapPointType */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Map Point Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-point-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'image',
            'is_main',
            'status',
            'updated_at',
            'created_at',
        ],
    ]) ?>

</div>
