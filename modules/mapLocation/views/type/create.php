<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\mapLocation\models\MapPointType */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Типы меток', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-point-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
