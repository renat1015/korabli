<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \app\modules\gallery\models\Gallery */

$this->title = 'Галерея';
$this->params['breadcrumbs'][] = $this->title;
$subItems = null;
if ($models) {
    $subItems = [];
    foreach ($models as $model) {
        $images = $model->getImages(277, 165);
        $imgList = '';
        foreach ($images as $image) {
            $imgList .= Html::img($image['fullSizeUrl']);
        }
        $item_content = Html::img($images[0]['url'], ['alt' => $model->title, 'class' => 'open-cp-gallery']) .
            Html::tag('strong', $model->title) .
            Html::tag('span', count($images) . ' фото', ['class' => 'img-count']) .
            Html::tag('div', $imgList, ['class' => 'img-list']);

        $subItems[] = [
            'content' => $item_content
        ];
    }
}
$this->context->galleryMenuItems = [
    'label'   => Yii::t('menu', 'Галерея'),
    'url'     => Url::to(['/site/gallery/list']),
    'options' => ['class'=>'with-sub cp-gallery'],
    'items'   => $subItems,
];
?>
    <div class="gallery-index">
        <?= \app\modules\siteBackground\widgets\BackgroundWidget::widget(); ?>
    </div>

<?php
$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',['depends'=>'yii\web\JqueryAsset']);
$this->registerCssFile(Yii::$app->request->baseUrl . '/js/fancybox/jquery.fancybox.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/fancybox/jquery.fancybox.js', ['depends' => 'yii\web\JqueryAsset']);
$redirect_link = Url::to(['/site/gallery']);
$this->registerJs(<<<JS

    $('.cp-gallery').on('click','> span, > ul .close-icon',function(){
        window.location='$redirect_link';
    });

    $('.open-cp-gallery').click(function(e){
        e.preventDefault();
        var galleryArray = [];
        $(this).parent().find('.img-list img').each(function(i,e){
            galleryArray.push({href:$(e).attr('src')});
        });
        $.fancybox.open(galleryArray, {
            padding : 0,
            maxWidth: 1200,
            tpl: {
                wrap : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                image : '<img class="fancybox-image" src="{href}" alt="" />',
                closeBtn : '<a title="Закрыть" class="close-icon" href="javascript:;"></a>',
                next : '<a title="Предидущая" class="cp-gallery-nav next" href="javascript:;"><span></span></a>',
                prev : '<a title="Следидущая" class="cp-gallery-nav prev" href="javascript:;"><span></span></a>'
            }
        });
    });

    $('.cp-gallery > ul').mCustomScrollbar();
    $('.cp-gallery > ul').append('<span class="close-icon"></span>');
JS
);
