<?php

use kartik\widgets\SwitchInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\gallery\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => true,
        'options'                => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= \app\modules\image\widgets\InputWidget::widget(['model' => $model, 'form' => $form]) ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),[
        'pluginOptions' =>
            [
                'size'    => 'small',
                'onText'  => 'Да',
                'offText' => 'Нет',
                'state'   => $model->isNewRecord ? true : $model->status,
            ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
