<?php

namespace app\modules\gallery;

class GalleryModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\gallery\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
