<?php

namespace app\modules\gallery\controllers;

use app\common\BaseAdminController;
use app\modules\gallery\GallerySearch;
use app\modules\gallery\models\Gallery;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * AdminController implements the CRUD actions for Gallery model.
 */
class AdminController extends BaseAdminController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors,[
                'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actionUpload()
    {
        $pic = UploadedFile::getInstanceByName('file');
        if (
            $pic->type == 'image/png'
            || $pic->type == 'image/jpg'
            || $pic->type == 'image/gif'
            || $pic->type == 'image/jpeg'
            || $pic->type == 'image/pjpeg'
        ) {
            $name =  md5(time()).'.jpg';
            if(!file_exists(Yii::$app->basePath.'/../uploads/banner/'))
                FileHelper::createDirectory(Yii::$app->basePath.'/../uploads/banner/', 0777);
            if(!file_exists(Yii::$app->basePath.'/../uploads/banner/imperavi'))
                FileHelper::createDirectory(Yii::$app->basePath.'/../uploads/banner/imperavi', 0777);

            if($pic->saveAs(Yii::$app->basePath.'/../uploads/banner/imperavi/'.$name))
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'filelink' => '/uploads/banner/imperavi/'.$name
                ];
            }
        }
        return false;
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->imgFiles = UploadedFile::getInstances($model,'imgFiles');
            $model->save(false);
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->imgFiles = UploadedFile::getInstances($model,'imgFiles');
            $model->save(false);
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
