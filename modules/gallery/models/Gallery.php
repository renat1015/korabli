<?php

namespace app\modules\gallery\models;

use app\common\BaseModel;
use app\modules\image\behaviors\ImagesBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 */
class Gallery extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    public $imgFiles;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'imgFiles' => 'Изображения',
            'status' => 'Публиковать',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    /**
     * @inheritdoc
     * @return GalleryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GalleryQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors,[
            'imagesBehavior' => [
                'class'     => ImagesBehavior::className(),
                'model'     => strtolower(self::getModelName()),
                'attribute' => 'imgFiles',
            ],
        ]);
    }
}
