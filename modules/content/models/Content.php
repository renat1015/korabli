<?php

namespace app\modules\content\models;

use app\common\BaseModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $short_content
 * @property string $content
 * @property string $image
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property ContentCategory $category
 */
class Content extends BaseModel
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @return array
     */
    public function behaviors() {
        return [

            'seo' => [
                'class' => 'app\modules\seo\behaviors\SeoBehavior',
                'model' => $this->getModelName(),
                'view_action' => '/content/content/view',
                'view_category' => ''
            ],

            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function()
                {
                    return date("Y-m-d H:i:s");
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title', 'content'], 'required'],
            [['category_id', 'status'], 'integer'],
            [['short_content', 'content'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['title', 'image'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'jpg, gif, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category',
            'title' => 'Title',
            'short_content' => 'Short Content',
            'content' => 'Content',
            'image' => 'Image',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ContentCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return mixed
     */
    public function buildUrl()
    {
        return $this->category->getSeoUrl();
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {

        if($this->file)
        {
            $file_name = md5(microtime()). '.' . $this->file->extension;
            $this->image = $file_name;

            if(!$insert)
                if($this->oldAttributes['image'] != $this->image)
                    FileHelper::removeDirectory($this->getUploadDir());
        }

        return parent::beforeSave($insert);
    }


    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if($this->file)
        {
            $original_dir = $this->getUploadDir() . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR;

            $this->file->saveAs($original_dir . $this->image);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function getUploadDir()
    {
        $date = date('Y_m', strtotime($this->created_at));

        $upload_dir = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR . $this->id;
        FileHelper::createDirectory($upload_dir);
        FileHelper::createDirectory($upload_dir . DIRECTORY_SEPARATOR . 'original');
        FileHelper::createDirectory($upload_dir . DIRECTORY_SEPARATOR . 'resize');

        return $upload_dir;

    }

    /**
     * @param bool $original
     *
     * @return string
     */
    public function getWebUploadDir($original = false)
    {
        $date = date('Y_m', strtotime($this->created_at));

        $upload_dir = DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR . $this->id;

        if($original)
            $upload_dir .= DIRECTORY_SEPARATOR . 'original';
        else
            $upload_dir .= DIRECTORY_SEPARATOR . 'resize';

        return $upload_dir;
    }


    /**
     * @param int $width
     * @param int $height
     *
     * @return bool|string
     */
    public function getResizeImage ($width = 215, $height = 160)
    {
        return resizeImage($this->getUploadDir() . DIRECTORY_SEPARATOR . 'original', $this->image, $this->getUploadDir() . DIRECTORY_SEPARATOR . 'resize', $width, $height, $this->getWebUploadDir());
    }


    public function afterDelete()
    {
        FileHelper::removeDirectory($this->getUploadDir());

        parent::afterDelete();
    }


}
