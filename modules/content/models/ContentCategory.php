<?php

namespace app\modules\content\models;

use app\common\BaseModel;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "content_category".
 *
 * @property integer $id
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $title
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Content[] $contents
 */
class ContentCategory extends BaseModel
{
    public $parent_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status', 'parent_id'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ContentCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContentCategoryQuery(get_called_class());
    }

    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                 'treeAttribute' => 'tree',
//                 'leftAttribute' => 'lft',
//                 'rightAttribute' => 'rgt',
//                 'depthAttribute' => 'depth',
            ],
            'seo' => [
                'class' => 'app\modules\seo\behaviors\SeoBehavior',
                'model' => $this->getModelName(),
                'view_action' => '/content/content/view-category',
                'view_category' => ''
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return string
     */
    public function buildUrl()
    {
        $url ='';
        $parent = $this->parents(1)->one();
        if($parent)
            $url = $parent->seoUrl;

        return $url;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        $parent = $this->parent()->one();
        return $parent['id'];
    }
    /**
     *
     */
    public function getAdminTitle()
    {
        return $this->depth > 0 ? str_repeat('- ', $this->depth).$this->title : Html::tag('strong',$this->title);
    }

    /**
     * @param bool $model
     * @return array
     */
    public static function getList($model = false)
    {
        if($model)
            $items = self::find()->where("id!={$model->id} AND tree!=".($model->tree != $model->id ? $model->id : $model->tree))->orderBy('tree, lft')->all();
        else
            $items = self::find()->orderBy('tree, lft')->all();

        $list = [];
        foreach ($items as $item) {
            $list[$item->id] = $item->depth > 0 ? str_repeat('- ', $item->depth).$item->title : $item->title;
        }

        return $list;
    }

    public static function getTree($parent_id = false)
    {
        $list = '';
        if(!$parent_id)
            $categories = self::find()->addOrderBy('tree, lft')->with(['seo'])->all();
        else
        {
            $root = self::findOne($parent_id);
            $categories = $root->children()->all();
        }

        $level = 0;

        foreach ($categories as $n => $category)
        {
            if ($category->depth == $level) {
                $list .= Html::endTag('li') . "\n";
            } elseif ($category->depth > $level) {
                $list .= Html::beginTag('ul') . "\n";
            } else {
                $list .= Html::endTag('li') . "\n";

                for ($i = $level - $category->depth; $i; $i--) {
                    $list .= Html::endTag('ul') . "\n";
                    $list .= Html::endTag('li') . "\n";
                }
            }

            $list .= Html::beginTag('li');
            $list .= Html::a(Html::encode($category->title), $category->seoUrl);
            $level = $category->depth;
        }

        for ($i = $level; $i; $i--) {
            $list .= Html::endTag('li') . "\n";
            $list .= Html::endTag('ul') . "\n";
        }

        return $list;
    }
}
