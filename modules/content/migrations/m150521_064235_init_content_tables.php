<?php

use yii\db\Schema;
use yii\db\Migration;

class m150521_064235_init_content_tables extends Migration
{
    public function up()
    {

        $this->createTable('content_category',[
            'id' => Schema::TYPE_PK,
            'tree'          => 'int',
            'lft'           => 'int not null',
            'rgt'           => 'int not null',
            'depth'         => 'int not null',

            'title'         => 'string not null',
            'status'        => 'tinyint not null default "1"',

            'updated_at'    => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'    => 'timestamp not null',

        ]);

        $this->createTable('content',[
            'id'            => Schema::TYPE_PK,
            'category_id'   => 'int not null',
            'title'         => 'string not null',
            'short_content' => 'text',
            'content'       => 'text not null',
            'image'         => 'string default null',
            'status'        => 'tinyint not null default "1"',
            'updated_at'    => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'    => 'timestamp not null',
        ]);

        $this->addForeignKey('fk_content_category', 'content', 'category_id', 'content_category', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_content_category', 'content');

        $this->dropTable('content');
        $this->dropTable('content_category');
    }

}
