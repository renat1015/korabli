<?php

namespace app\modules\content\controllers;

use app\common\BaseController;
use app\modules\content\models\ContentCategory;
use app\modules\content\models\ContentCategorySearch;
use Yii;
use app\modules\content\models\Content;
use app\modules\content\models\ContentSearch;
use yii\web\NotFoundHttpException;

/**
 * ContentController implements the CRUD actions for Content model.
 */
class ContentController extends BaseController
{

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex()
    {
        $tree = ContentCategory::getTree();

        $content = new ContentSearch();
        $dataProvider = $content->search([]);

        return $this->render('index', [
            'tree' => $tree,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewCategory($id)
    {
        $tree = ContentCategory::getTree($id);

        $content = new ContentSearch();
        $content->category_id = $id;
        $dataProvider = $content->search([]);

        return $this->render('view-category', [
            'model' => $this->findCategoryModel($id),
            'tree' => $tree,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Content model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Content model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Content the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     *
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findCategoryModel($id)
    {
        if (($model = ContentCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
