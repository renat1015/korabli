<?php

namespace app\modules\content\controllers;

use app\modules\menu\controllers\AdminController;
use Yii;
use app\modules\content\models\ContentCategory;
use app\modules\content\models\ContentCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdminCategoryController implements the CRUD actions for ContentCategory model.
 */
class AdminCategoryController extends AdminController
{
    /**
     * Lists all ContentCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new ContentCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContentCategory();

        if ($model->load(Yii::$app->request->post())) {

            if($model->parent_id)
            {
                $root = $this->findModel($model->parent_id);
                $model->prependTo($root);
            } else
                $model->makeRoot();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing ContentCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->parent_id)
            {
                $root = $this->findModel($model->parent_id);
                $model->prependTo($root);
            } else
                $model->makeRoot();

            return $this->redirect(['view', 'id' => $model->id]);
        }
        $parent = $model->parents(1)->one();
        $model->parent_id = $parent ? $parent->id : 0;

        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing ContentCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ContentCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContentCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContentCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
