<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\content\models\ContentCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= \app\modules\seo\widgets\SeoWidget::widget(['model' => $model])?>

    <?= $form->field($model, 'parent_id')->dropDownList(\app\modules\content\models\ContentCategory::getList($model->isNewRecord ? false : $model), ['prompt' => 'Root'])?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
