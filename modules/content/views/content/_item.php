<?php
/* @var $model app\modules\content\models\Content */
?>

<div class="content-view">
    <h3><?= \yii\helpers\Html::a($model->title, $model->seoUrl)?></h3>
    <div>
        <div>
            <?= \yii\helpers\Html::img($model->getResizeImage())?>
        </div>

        <?= $model->short_content?>
    </div>
</div>
<hr/>
