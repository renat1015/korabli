<?php

use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\content\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= \app\modules\seo\widgets\SeoWidget::widget(['model' => $model])?>

    <?= $form->field($model, 'category_id')->dropDownList(\app\modules\content\models\ContentCategory::getList(), ['prompt' => 'Select category'])?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->widget(\yii\imperavi\Widget::className(), [
        'options' => [
            'buttonSource' => 'true',
            'minHeight' => 350,
            'imageUpload' => '/content/admin/upload',
        ],
        'plugins' => [
            'fullscreen',
            'clips',
            'fontcolor',
            'fontfamily',
            'fontsize',
        ]
    ]) ?>

    <?= $form->field($model, 'file')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'initialPreview'=>[
                (!$model->isNewRecord ? Html::img($model->getResizeImage(), ['style' => 'max-width: 200px']) : null)
            ],
        ]
    ]);?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
