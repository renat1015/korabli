<?php

namespace app\modules\params;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\params\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
