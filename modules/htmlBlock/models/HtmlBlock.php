<?php

namespace app\modules\htmlBlock\models;

use app\common\BaseModel;
use Yii;

/**
 * This is the model class for table "html_block".
 *
 * @property integer $id
 * @property string $title
 * @property string $position
 * @property string $content
 * @property integer $status
 * @property integer $ordering
 * @property integer $redactor_mode
 * @property string $updated_at
 * @property string $created_at
 */
class HtmlBlock extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'html_block';
    }

    const REDACTOR_ON = 1;
    const REDACTOR_OFF = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'position', 'content'], 'required'],
            [['content'], 'string'],
            [['status', 'ordering','redactor_mode'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title', 'position'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Название',
            'position'      => 'Позиция',
            'content'       => 'Содержание',
            'redactor_mode' => 'Включить редактор',
            'status'        => 'Статус',
            'ordering'      => 'Сортировка',
            'updated_at'    => 'Дата оновлення',
            'created_at'    => 'Дата створення',
        ];
    }

    static function getModeList()
    {
        return [
            self::REDACTOR_ON => 'Так',
            self::REDACTOR_OFF => 'Ні'
        ];
    }
}
