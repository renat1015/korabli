<?php

namespace app\modules\htmlBlock;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\htmlBlock\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
