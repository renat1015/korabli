<?php

namespace app\modules\files\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $file_name
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property FileCategory $category
 */
class File extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title'], 'required'],
            [['file_name'], 'required', 'on' => 'create'],
            [['category_id', 'status'], 'integer'],
            [['updated_at', 'created_at', 'date'], 'safe'],
            [['file_name'], 'file'],
            [['title', 'author_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'title' => 'Название файла',
            'file_name' => 'Файл',
            'status' => 'Публиковать',
            'date' => 'Дата',
            'author_name' => 'Автор',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FileCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        $dir = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/files/';
        FileHelper::createDirectory($dir);

        if($this->file_name)
        {
            //фикс для киррилических названий
            $name = Inflector::slug(str_replace('.'.$this->file_name->getExtension(), '', $this->file_name->name), '_') . "_".time();
            $name_with_ext = $name . "." .$this->file_name->getExtension();

            $this->file_name->saveAs($dir . $name_with_ext);
            $this->file_name = $name_with_ext;

            if (!$insert && $this->oldAttributes['file_name'] !=  $name_with_ext)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/files/' . $this->oldAttributes['file_name']);

        } else {
            if (!$insert)
                $this->file_name = $this->oldAttributes['file_name'];
        }

        return parent::beforeSave($insert);
    }


}
