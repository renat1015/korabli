<?php

namespace app\modules\files\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "file_category".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property File[] $files
 */
class FileCategory extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'image' => 'Изображение',
            'status' => 'Публиковать',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['category_id' => 'id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/fileCategory/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/fileCategory/' . $file_name);
            $this->image = $file_name;

            if (!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/fileCategory/' . $this->oldAttributes['image']);

        } else {
            if (!$insert)
                $this->image = $this->oldAttributes['image'];
        }

        return parent::beforeSave($insert);
    }


    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/fileCategory/' . $this->image)) {
            return '/uploads/fileCategory/' . $this->image;
        } else
            return false;
    }

    /**
     *
     */
    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/fileCategory/' . $this->image);
        parent::afterDelete();

    }

    public static function getList($map=false)
    {
        $models = self::getDb()->cache(function(){
            return FileCategory::find()->where(['status' => self::STATUS_PUBLISHED])->all();
        },FileCategory::DEFAULT_CACHE_DURATION,FileCategory::getDbDependency());
        if(!$map)
            return $models;
        return ArrayHelper::map($models,'id','title');
    }

    static function getListForMenu()
    {
        $categories = self::getList();

        if($categories)
        {
            $items = [];

            foreach($categories as $category)
            {
                $sub_items = self::getFilesByCategory($category->id);
                if($sub_items)
                {
                    $items[] = [
                        'label' => Html::img($category->getImage(),['alt'=>$category->title]).$category->title,
                        'options' => ['class'=>'with-sub files-list'],
                        'itemOptions' => ['class'=>'come-class'],
                        'items' => self::getFilesByCategory($category->id)
                    ];
                }
            }
            return $items;
        }
        return null;
    }

    static function getFilesByCategory($id)
    {
        $files = File::getDb()->cache(function() use($id){
            return File::find()->where(['status'=>self::STATUS_PUBLISHED,'category_id'=>$id])->all();
        },self::DEFAULT_CACHE_DURATION,File::getDbDependency());

        if($files)
        {
            $items = [];

            foreach ($files as $file) {
                $items[] = [
                    'label' => Html::img(Yii::$app->request->baseUrl.'/img/document_icon.png',['alt'=>$file->title]).$file->title,
                    'url' => Url::to(['/files/default/download','id'=>$file->id]),
                    'template'=>'<a class="normal-redirect" href="{url}">{label}</a>'
                ];
            }
            return $items;
        }
        return null;
    }
}
