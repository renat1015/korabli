<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\files\models\FileCategory */

$this->title = 'Добавить категорию';
$this->params['breadcrumbs'][] = ['label' => 'Категории документов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
