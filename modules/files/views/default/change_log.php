<div id="rotate-device">
    <div class="middle">
        <img src="/img/rotate__device.svg">
    </div>
</div>
<div class="change-log">
    <table>
        <thead>
        <tr>
            <td class="title">Вид размещаемой информации</td>
            <td class="date">Дата и время опубликования на сайте</td>
            <td class="author">Лицо, разместившее информацию</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($models as $file):?>
            <tr>
                <td class="title">
                    <a href="<?= \yii\helpers\Url::to(['/files/default/download','id'=>$file->id])?>">
                        <?= $file->title; ?>
                    </a>
                </td>
                <td class="date">
<!--                    --><?php //var_dump($file->date)?>
                    <?= date('d.m.Y H:i:s', strtotime($file->date));?>
                </td>
                <td class="author"><?= $file->author_name; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>