<?php
$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',['depends'=>'yii\web\JqueryAsset']);

$this->registerJs(<<<JS

JS
);
?>

<div class="kor-documents">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="documents">
        <h3>Документы</h3>
        <a href="<?= \yii\helpers\Url::to(['/files/default/change-log'])?>">
            <span>Журнал изменений</span>
        </a>
        <ul class="docs">
            <?php foreach ($models as $category):?>
                <li class="category"><img src="<?= $category->getImage() ?>" alt="img"><span><?= $category->title; ?></span></li>
                <ul>
                <?php foreach ($category->files as $model): ?>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/files/default/download','id'=>$model->id])?>">
                            <img src="/img/doc.png" alt="img"><span><?= $model->title;?></span>
                        </a>
                    </li>
                <?php endforeach;?>
                </ul>
            <?php endforeach;?>
        </ul>
    </div>
    <?= \app\modules\siteBackground\widgets\BackgroundWidget::widget(); ?>

    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>
