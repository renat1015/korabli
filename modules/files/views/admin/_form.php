<?php
use kartik\widgets\DateTimePicker;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\files\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <?= $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\files\models\FileCategory::getList(), 'id', 'title')) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_name')->fileInput() ?>

    <?= $form->field($model, 'author_name')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'id' => 'files_date'])?>



    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
        [
            'pluginOptions' =>
                [
                    'size'    => 'small',
                    'onText'  => 'Да',
                    'offText' => 'Нет',
                    'state'   => $model->isNewRecord ? true : $model->status,
                ],
        ]
    ); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jquery.mask.min.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJs(<<<JS
    $('#files_date').mask('0000-00-00 00:00:00', {placeholder: "2017-12-31 14:00:00"});
JS
);
?>