<?php

namespace app\modules\files\controllers;

use app\common\BaseController;
use app\modules\files\models\File;
use app\modules\files\models\FileCategory;
use Yii;
use yii\web\NotFoundHttpException;

class DefaultController extends BaseController
{
    public function actionIndex()
    {
        $models = FileCategory::getDb()->cache(function(){
            return FileCategory::find()->all();
        },FileCategory::DEFAULT_CACHE_DURATION,FileCategory::getDbDependency());

        return $this->render('index',['models'=>$models]);
    }

    public function actionChangeLog()
    {
        $models = File::getDb()->cache(function(){
            return File::find()->all();
        },File::DEFAULT_CACHE_DURATION,File::getDbDependency());

        $this->layout = 'change_log';

        return $this->render('change_log',['models'=>$models]);
    }

    /**
     * @param $id
     *
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDownload($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->sendFile(Yii::$app->basePath.'/../uploads/files/'. $model->file_name);
        Yii::$app->end();
    }

    /**
     * @param $id
     *
     * @return null|static
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
