<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_085731_init_files extends Migration
{
    public function up()
    {
        $this->createTable('file_category', [
            'id'     => $this->primaryKey(),
            'title'  => $this->string(255)->notNull(),
            'image'  => $this->string(255)->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);

        $this->createTable('file', [
            'id'          => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'title'       => $this->string(255)->notNull(),
            'file_name'   => $this->string(255)->notNull(),

            'status' => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);

        $this->addForeignKey('fk_file_category', 'file', 'category_id', 'file_category', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_file_category', 'file');
        $this->dropTable('file');
        $this->dropTable('file_category');

    }

}
