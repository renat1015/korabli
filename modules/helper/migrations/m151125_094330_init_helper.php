<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_094330_init_helper extends Migration
{
    public function up()
    {
        $this->createTable('helper', [
            'id'     => $this->primaryKey(),
            'key'    => $this->string(255)->notNull(),
            'text'   => $this->text()->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);
    }

    public function down()
    {
        $this->dropTable('helper');
    }

}
