<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 25.11.15
 * Time: 13:05
 */

namespace app\modules\helper\widgets;


use app\common\BaseModel;
use app\modules\helper\models\Helper;
use Yii;
use yii\base\Widget;

class HelperWidget extends Widget
{
    public $key;

    public function run()
    {

        $model = Yii::$app->cache->get('helper_'.$this->key);
        if(!$model)
        {
            $model = Helper::find()->where(['key' => $this->key,'status'=>BaseModel::STATUS_PUBLISHED])->limit(1)->one();
            Yii::$app->cache->set('helper_'.$this->key,$model,BaseModel::DEFAULT_CACHE_DURATION);
        }

        if (!$model)
            return false;

        return $this->render('helper_widget',
            [
                'model' => $model,
            ]
        );

    }
}