<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 25.11.15
 * Time: 13:
 *
 * @var $model app\modules\helper\models\Helper
 */

?>

<div id="helper-block" class="open">
    <div class="icon">X</div>
    <div class="text" style="display: block;">
        <?= $model->text ?>
    </div>
</div>
