<?php

namespace app\modules\helper;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\helper\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
