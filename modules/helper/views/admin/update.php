<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\helper\models\Helper */

$this->title = 'Редактировать: ' . ' ' . $model->key;
$this->params['breadcrumbs'][] = ['label' => 'Хелпер', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="helper-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
