<?php

namespace app\modules\helper\models;

use app\common\BaseModel;
use Yii;

/**
 * This is the model class for table "helper".
 *
 * @property integer $id
 * @property string  $key
 * @property string  $text
 * @property integer $status
 * @property string  $updated_at
 * @property string  $created_at
 */
class Helper extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'helper';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'text'], 'required'],
            [['key'], 'unique'],
            [['text'], 'string'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'key'        => 'Ключ',
            'text'       => 'Текст',
            'status'     => 'Публиковать',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    public function beforeDelete()
    {
        Yii::$app->cache->delete('helper_'.$this->key);
        return parent::beforeDelete();
    }


}
