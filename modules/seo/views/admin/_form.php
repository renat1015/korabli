<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\modules\seo\models\Seo $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="seo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'keywords')->textInput(['maxlength' => 255])?>

    <?= $form->field($model, 'external_link')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'internal_link')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'model_id')->textInput() ?>
    <?= $form->field($model, 'model_name')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton(  $model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
