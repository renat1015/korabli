<?php

namespace app\modules\seo\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "seo".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $head_block
 * @property string $external_link
 * @property string $internal_link
 * @property string $external_link_with_cat
 * @property integer $noindex
 * @property integer $nofollow
 * @property integer $in_sitemap
 * @property integer $is_canonical
 * @property string $model_name
 * @property integer $model_id
 * @property integer $status
 * @property string $updated_at
 */
class Seo extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['head_block'], 'string'],
            [['external_link'], 'required'],
            [['noindex', 'nofollow', 'in_sitemap', 'is_canonical', 'model_id', 'status'], 'integer'],
            [['updated_at','created_at'], 'safe'],
            [['title', 'description', 'keywords', 'external_link', 'internal_link', 'external_link_with_cat', 'model_name'], 'string', 'max' => 255]
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                     => 'ID',
            'title'                  => Yii::t('seo', 'Page title'),
            'description'            => Yii::t('seo', 'Meta description'),
            'keywords'               => Yii::t('seo', 'Meta keywords'),
            'head_block'             => Yii::t('seo', 'Head Block'),
            'external_link'          => Yii::t('seo', 'External link'),
            'internal_link'          => Yii::t('seo', 'Internal link'),
            'external_link_with_cat' => Yii::t('seo', 'External Link With Cat'),
            'noindex'                => Yii::t('seo', 'Noindex'),
            'nofollow'               => Yii::t('seo', 'Nofollow'),
            'in_sitemap'             => Yii::t('seo', 'In Sitemap'),
            'is_canonical'           => Yii::t('seo', 'Is Canonical'),
            'model_name'             => Yii::t('seo', 'Model Name'),
            'model_id'               => Yii::t('seo', 'Model ID'),
            'status'                 => Yii::t('seo', 'Status'),
            'updated_at'           => Yii::t('seo', 'Updated Date'),
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->cache->set('seo_'.$this->external_link,$this,86400);
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     *
     */
    public function afterDelete()
    {
        Yii::$app->cache->delete('seo_'.$this->external_link);
        parent::afterDelete();
    }


}
