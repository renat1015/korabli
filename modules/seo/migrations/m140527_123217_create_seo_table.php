<?php

use yii\db\Expression;
use yii\db\Schema;

class m140527_123217_create_seo_table extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('seo',[
            'id'            => Schema::TYPE_PK,
            'title'         => 'string',
            'description'   => 'string',
            'keywords'      => 'string',
            'head_block'    => 'text',
            'external_link' => 'string not null',
            'internal_link' => 'string not null',
            'external_link_with_cat' => 'string default null',
            'noindex'       => 'tinyint not null default "0"',
            'nofollow'      => 'tinyint not null default "0"',
            'in_sitemap'    => 'tinyint not null default "1"',
            'is_canonical'  => 'tinyint not null default "0"',
            'model_name'    => 'string default null',
            'model_id'      => 'integer default null',
            'status'        => 'tinyint not null default "0"',
            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);

        $this->insert('seo',[
            'title' => 'Main page',
            'description' => 'Main page description',
            'keywords' => 'Main page keywords',
            'external_link' => '/',
            'internal_link' => 'site/index',
            'updated_at' =>  new Expression('NOW()'),
        ]);
    }

    public function down()
    {
        $this->dropTable('seo');
    }
}
