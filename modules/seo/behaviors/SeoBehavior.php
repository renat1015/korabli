<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 27.05.14
 * Time: 15:47
 */
namespace app\modules\seo\behaviors;

use app\modules\seo\models\Seo;
use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;

class SeoBehavior extends Behavior
{

    public $model;
    public $view_category;
    public $view_action;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT=> 'writeSeo',
            ActiveRecord::EVENT_AFTER_UPDATE=> 'writeSeo',
            ActiveRecord::EVENT_AFTER_DELETE=> 'deleteSeo',
        ];
    }

    /**
     * @param $event
     * @throws \Exception
     */
    public function deleteSeo($event)
    {
        $seo = Seo::find()->where(['model_name'=>$this->model, 'model_id'=>$event->sender->id])->one();
        if($seo)
            $seo->delete();

    }


    /**
     * @param $event
     */
    public function writeSeo($event){

        $seo = Seo::find()->where(['model_name'=>$this->model, 'model_id'=>$event->sender->id])->one();
        if($seo==null)
            $seo = new Seo();

        $title = trim(str_replace('  ', ' ',$event->sender->title));

        $seo->load(Yii::$app->request->post());
        if($seo->external_link == '')
        {
            $seo->external_link = $this->checkUnicUrl(($this->view_category ? $this->view_category . "/" : "") . translateIt($title), $event->sender->id);
            if(method_exists($event->sender, 'buildUrl'))
                $seo->external_link = trim($event->sender->buildUrl() . '/' . translateIt($title), '/');
        }

        $seo->title = $seo->title ? $seo->title : $title;
        $seo->internal_link = $this->view_action;

        $seo->model_name = $this->model;
        $seo->model_id = $event->sender->id;

        $seo->save(false);
    }

    /**
     * @param $url
     * @param $id
     * @return string
     */
    public function checkUnicUrl($url, $id)
    {
        $result = Seo::find()->where(['external_link'=>$url])->one();
        if($result!=null)
            if($result->id!=$id)
                return $url.'-'.$id;

        return $url;
    }
}