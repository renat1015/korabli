<?php

use yii\db\Schema;

class m140527_123205_create_news_table extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('news',[
            'id'            => Schema::TYPE_PK,
            'title'         => 'string not null',
            'content'       => 'text not null',
            'short_content' => 'text',
            'status'        => 'tinyint not null default "0"',
            'updated_at'    => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'    => 'timestamp not null',
        ]);

        $this->addColumn('news', 'image', 'string null');
    }

    public function down()
    {
        $this->dropTable('news');
    }
}
