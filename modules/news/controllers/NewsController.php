<?php

namespace app\modules\news\controllers;

use Yii;
use app\modules\news\models\News;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use app\modules\news\models\SearchNews;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\common\BaseController;
use yii\helpers\FileHelper;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BaseController
{
    public $enableCsrfValidation = false;

    /**
     * Lists all News models.
     * @param null $year
     * @return mixed
     */
    public function actionIndex($year=null)
    {
//        dump(Yii::$app->request->get('year'));
//        die();
        $query = News::find()->orderBy('created_at DESC');
        if(Yii::$app->request->get('year'))
            $query->where("DATE_FORMAT(created_at,'%Y') = '$year'");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query='SELECT DATE_FORMAT(created_at, "%Y") as m_date FROM news GROUP BY m_date ORDER BY m_date DESC;';
        $years = Yii::$app->db->createCommand($query)->queryAll();
        $main_year = $year;

//        $years = News::find()->select('DATE_FORMAT(created_at, "%Y") as m_date')->groupBy("m_date")->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'years' => $years,
            'main_year' => $main_year
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $views = Yii::$app->session->get('news',[]);

        if(!in_array($model->id, $views))
        {
            $views[] = $model->id;
            Yii::$app->session->set('news', $views);
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
