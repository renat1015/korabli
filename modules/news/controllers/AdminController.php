<?php

namespace app\modules\news\controllers;

use Yii;
use app\modules\news\models\News;
use app\modules\news\models\SearchNews;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\common\BaseAdminController;
use yii\helpers\FileHelper;
use yii\web\Response;
use app\extensions\fileapi\actions\UploadAction;
use app\extensions\fileapi\actions\DeleteAction;
class AdminController extends BaseAdminController
{
    /**
     * @return array
     */
    public function actionUpload()
    {
        $pic = UploadedFile::getInstanceByName('file');
        if (
            $pic->type == 'image/png'
            || $pic->type == 'image/jpg'
            || $pic->type == 'image/gif'
            || $pic->type == 'image/jpeg'
            || $pic->type == 'image/pjpeg'
        ) {
            $name =  md5(time()).'.jpg';
            if(!file_exists(Yii::$app->basePath.'/../uploads/news/'))
                FileHelper::createDirectory(Yii::$app->basePath.'/../uploads/news/', 0777);
            if(!file_exists(Yii::$app->basePath.'/../uploads/news/imperavi'))
                FileHelper::createDirectory(Yii::$app->basePath.'/../uploads/news/imperavi', 0777);

            if($pic->saveAs(Yii::$app->basePath.'/../uploads/news/imperavi/'.$name))
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'filelink' => '/uploads/news/imperavi/'.$name
                ];
            }
        }
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'uploadTempImage' => [
                'class' => UploadAction::className(),
                'path' => News::imageTempPath(),
                'types' => ['jpg', 'png', 'gif'],
                'minHeight' => 250,
                'maxHeight' => 200,
                'minWidth' => 250,
                'maxWidth' => 200,
                'maxSize' => 3145728
            ],
            'deleteTempImage' => [
                'class' => DeleteAction::className(),
                'path' => News::imageTempPath(),
            ]
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchNews;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);
        $model->image = '';

    }
}
