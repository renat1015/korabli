<?php

namespace app\modules\news\models;

use app\common\BaseModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use app\modules\seo\models\Seo;

use yii\helpers\FileHelper;
use app\extensions\fileapi\behaviors\UploadBehavior;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string  $title
 * @property string  $content
 * @property string  $image
 * @property string  $short_content
 * @property integer $status
 * @property string  $updated_at
 * @property string  $created_at
 *
 * @property Seo     $seo
 */
class News extends BaseModel
{
    public $file;
    public $delete_image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content', 'short_content', 'image'], 'string'],
            [['status'], 'integer'],
            [['updated_at', 'created_at', 'delete_image'], 'safe'],
//            [['file'], 'file', 'extensions' => 'jpg, jpeg, png, gif'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value'      => new Expression('NOW()'),
            ],

            'seo' => [
                'class'         => 'app\modules\seo\behaviors\SeoBehavior',
                'model'         => $this->getModelName(),
                'view_category' => 'news',
                'view_action'   => 'news/news/view',
            ],

            'uploadBehavior' => [
                'class'      => UploadBehavior::className(),
                'attributes' => ['image'],
                'path'       => $this->imagePath(),
                'tempPath'   => $this->imageTempPath(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Название',
            'content'       => 'Содержание',
            'short_content' => 'Краткое содержание',
            'status'        => 'Статус',
            'file'          => 'Картинка новости',
            'image'         => 'Картинка новости',
            'updated_at'    => 'Дата обновления',
            'created_at'    => 'Дата создания',
            'delete_image'  => 'Новость без картинки',
        ];
    }

    /**
     * @return string
     */
    public function getImage()
    {
        if (!$this->image)
            return false;

        $image = Yii::$app->request->baseUrl . '/uploads/news/' . $this->id . '/' . $this->image;
        if (file_exists(Yii::$app->basePath . '/../uploads/news/' . $this->id . '/' . $this->image))
            return $image;

        return false;
    }

    /**
     * @return string
     */
    public function imageUrl()
    {
        $url = '/uploads/news/' . $this->id . '/';

        return $url;
    }

    /**
     * @param null $image
     *
     * @return bool|string
     */
    public function imagePath($image = null)
    {
        FileHelper::createDirectory(Yii::$app->basePath . '/../uploads/news/', 0777);
        $path = Yii::$app->basePath . '/../uploads/news/';
        if ($image !== null) {
            $path .= '/' . $image;
        }

        return Yii::getAlias($path);
    }

    /**
     * @param null $image
     *
     * @return bool|string
     */
    public static function imageTempPath($image = null)
    {
        $path = Yii::$app->basePath . '/../uploads/tmp/news';
        if ($image !== null) {
            $path .= '/' . $image;
        }

        return Yii::getAlias($path);
    }

    static function getPublishYearList()
    {
        $sql = "SELECT DISTINCT YEAR(created_at) as year FROM news ORDER BY year DESC";
        $list = self::getDb()->cache(function($db) use($sql){
            return $db->createCommand($sql)->queryColumn();
        });
        $result = [];
        foreach ($list as $item) {
            $result[$item] = $item;
        }
        return $result;
    }
}
