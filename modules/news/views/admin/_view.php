<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\modules\news\models\News $model
 */

?>

<h5><?= Html::a($model->title, $model->seo->external_link)?></h5>
<div>
    <?= Html::img($model->getImage())?>
    <div>
        <?= $model->short_content?>
    </div>
</div>

