<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\seo\widgets\SeoWidget;
use yii\helpers\ArrayHelper;
use app\extensions\fileapi\FileAPIAdvanced;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var app\modules\news\models\News $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(
        [
            'options'=>[
                'enctype'=>'multipart/form-data'
            ]
        ]
    ); ?>

    <?= SeoWidget::widget(['model' => $model]);?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->widget(\yii\imperavi\Widget::className(), [
        'options' => [
            'buttonSource'      => 'true',
            'minHeight'         => 350,
            'imageUpload'       => '/news/admin/upload',
            'uploadImageFields' => [
                Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()
            ],
            'uploadFileFields'  => [
                Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()
            ]
        ],
        'plugins' => [
            'fullscreen',
            'clips',
            'fontcolor',
            'fontfamily',
            'fontsize',
        ]
    ]) ?>

    <?php echo
    $form->field($model, 'image')->widget(FileAPIAdvanced::className(), [
        'url' => $model->imageUrl(),
        'deleteUrl' => Url::toRoute('/news/admin/delete-image?id='.$model->id),
        'deleteTempUrl' => Url::toRoute('/news/admin/delete-temp-image'),
        'crop' => true,
        'cropResizeWidth' => 285,
        'cropResizeHeight' => 185,
        'previewWidth' => 285,
        'previewHeight' => 185,
        'settings' => [
            'url' => Url::toRoute('uploadTempImage'),
            'imageSize' =>  [
                'minWidth' => 285,
                'minHeight' => 185
            ],
            'preview' => [
                'el' => '.uploader-preview',
                'width' => 285,
                'height' => 185
            ]
        ]
    ])
    ?>

    <div class="form-group">
        <?= Html::submitButton(  $model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style type="text/css">
    .uploader{
        width: 285px;
    }
    .uploader .uploader-preview-cnt{
        width: 285px;
    }
</style>
