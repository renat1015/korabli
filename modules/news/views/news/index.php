<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modules\news\models\SearchNews $searchModel
 */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
$this->context->sidebarAdvancedClass = 'full-color';
//$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
//$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',['depends'=>'yii\web\JqueryAsset']);
$this->registerJs(<<<JS
    // $('body').css('overflow','hidden');

    //function initNewsSlider(){
    //    var newsBlock = $('.list-view'),
    //    viewPort = $('.list',newsBlock).height(),
    //    sliderInner = $('.slider-inner',newsBlock),
    //    itemsCount = $('.item',sliderInner).size(),
    //    currentMargin = parseInt($(sliderInner).css('margin-top')),
    //    maxMargin = $(sliderInner).height()-viewPort,
    //    upButton = $('.up',newsBlock),
    //    downButton = $('.down',newsBlock),
    //    step = parseInt($(sliderInner).height()) / itemsCount;
    //    if(itemsCount>1)
    //        step += itemsCount*45;
    //
    //    step = Math.round(step);
    //        console.log(step);
    //
    //    if(maxMargin!='0')
    //       maxMargin = -maxMargin;
    //
    //    if($(sliderInner).height() > viewPort){
    //       downButton.show();
    //    }
    //
    //    $(downButton).click(function(){
    //       var featureMargin = currentMargin - step;
    //       upButton.show();
    //       if(featureMargin < maxMargin)
    //       {
    //           currentMargin = maxMargin;
    //           $(sliderInner).animate({marginTop:currentMargin},300);
    //           downButton.hide();
    //           return false;
    //       }
    //       else{
    //           if(currentMargin - step <= maxMargin)
    //               upButton.hide();
    //           currentMargin = currentMargin - step;
    //           $(sliderInner).animate({marginTop:currentMargin},300);
    //
    //           return false;
    //       }
    //    });
    //
    //    $(upButton).click(function(){
    //       var featureMargin = currentMargin + step;
    //       downButton.show();
    //       if(featureMargin > 0)
    //       {
    //           currentMargin = 0;
    //           $(sliderInner).animate({marginTop:currentMargin},300);
    //           upButton.hide();
    //           return false;
    //       }
    //       else{
    //           if(currentMargin + step >= 0)
    //               downButton.hide();
    //           currentMargin = currentMargin + step;
    //           $(sliderInner).animate({marginTop:currentMargin},300);
    //           return false;
    //       }
    //    });
    //}
    //initNewsSlider();

    // $('.list-view .list').mCustomScrollbar();
    //
    // $('.search-block label').click(function(){
    //     if(!$(this).prev('input').is('chechked'))
    //     {
    //         $(this).parents('form').find('input').attr('checked',false);
    //         $(this).prev('input').attr('checked',true);
    //         $(this).parents('form').submit();
    //     }
    // });
JS
);
?>


<div class="dynamics news">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="title">
        <h3><?= $this->title ?></h3>
        <div class="year">
            <?php foreach ($years as $year):?>
                <a
                        href="<?= \yii\helpers\Url::to(['/news', 'year' => $year['m_date']])?>"
                        <?= ($main_year == $year['m_date']) ? 'class="active"' : ''; ?>
                ><?= $year['m_date'];?></a>
            <?php endforeach;?>
        </div>
    </div>
    <?php Pjax::begin(); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options'=>['class'=>'news-slider'],
        'itemOptions' => ['class' => 'item',],
        'itemView' => '_view',
        'layout' => '{items}',
        'pager' => [
            'hideOnSinglePage' =>true
        ],
        'sorter' => [

        ]
    ]) ?>
    <?php Pjax::end(); ?>
<!--    <div class="news-slider">-->
<!--        <div class="ns-item">-->
<!--            <div>-->
<!--                <p class="date"><span class="day">01</span><span class="month">/11</span></p>-->
<!--                <a href="#" class="news-title">Программа для работников военно - промышленного комплекса «ВПК»</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options'=>['class' => 'news_modals'],
        'itemOptions' => ['class' => 'item',],
        'itemView' => '_modal',
        'layout' => '{items}',
        'pager' => [
            'hideOnSinglePage' =>true
        ],
        'sorter' => [

        ]
    ]) ?>


    <div class="image">
        <?= \app\modules\siteBackground\widgets\BackgroundWidget::widget() ?>
<!--        <img src="/img/slide8.jpg" alt="img">-->
    </div>
    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>
