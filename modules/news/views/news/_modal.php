<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\news\models\News $model
 */
$image = ($img = $model->getImage()) ? Html::img($img,['alt'=>$model->title]) : null; ;
?>

<!--<span class="date">--><?//= Yii::$app->formatter->asDate($model->created_at,'dd.MM.yyyy') ?><!--</span>-->
<!---->
<!---->
<?//= Html::a($model->title.$image,$model->seoUrl,['data'=>['pjax'=>0]]) ?>
<!---->
<!--<div class="short-desc">--><?//= getShortText($model->short_content,190,true) ?><!--</div>-->

<div class="ns-popup" id="<?= $model->id; ?>">
    <div class="ns-content">
        <a href="#" class="btn-menu-close">
            <span class="btn-close"></span> <span>Закрыть</span>
        </a>
        <div class="nc-date"><?= Yii::$app->formatter->asDate($model->created_at,'dd.MM.yyyy') ?></div>
        <div class="nc-title"><?= $model->title;?></div>
        <div class="nc-text">
            <?= $model->content;?>
        </div>
    </div>
</div>