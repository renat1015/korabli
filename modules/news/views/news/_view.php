<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\news\models\News $model
 */
$image = ($img = $model->getImage()) ? Html::img($img,['alt'=>$model->title]) : null; ;
?>


<div class="ns-item" data-id="<?= $model->id;?>">
    <div>
        <p class="date"><span class="day"><?= Yii::$app->formatter->asDate($model->created_at,'dd') ?></span><span class="month">/<?= Yii::$app->formatter->asDate($model->created_at,'MM') ?></span></p>
        <a href="#" class="news-title"><?= $model->title;?></a>
    </div>
</div>