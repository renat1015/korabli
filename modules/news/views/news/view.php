<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\comment\widgets\CommentWidget;
use app\modules\rating\widgets\RatingWidget;


/**
 * @var yii\web\View $this
 * @var app\modules\news\models\News $model
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->context->sidebarAdvancedClass = 'full-color hide-sub';
$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',['depends'=>'yii\web\JqueryAsset']);

$this->registerJs(<<<JS
    $('body').css('overflow','hidden');

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('.page-modal').css({width:$(document).width(), height: $(document).height()});
        $(window).on('resize',function(e){
            $('.page-modal').css({width:$(document).width(), height: $(document).height()});
        });
    }
    $('.content-block .text').mCustomScrollbar();
JS
);
?>
<?= \app\modules\siteBackground\widgets\BackgroundWidget::widget() ?>
<div class="news-view page-modal right-offset">
    <div class="content-block">
        <?= Html::a('<span class="close-icon">x</span>','#',['class'=>'close-page']) ?>
        <span class="date"><?= Yii::$app->formatter->asDate($model->created_at,'dd.MM.yyyy') ?></span>
        <h1><?php echo $model->title; ?></h1>
        <div class="text">
            <?= $model->content ?>
        </div>
    </div>
</div>