<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 23.06.14
 * Time: 14:57
 * @var yii\web\View $this
 * @var \app\modules\news\models\News $model
 */
use yii\helpers\Html;
?>
<div class="last-news clearfix">
    <h3>Новости</h3>
    <ul>
        <?php foreach($models as $model):?>
            <li class="item">
                <?php echo Html::a(getShortText($model->title,50,true),[$model->seoUrl],['class'=>'title']); ?>
                <p><?php echo getShortText($model->short_content,200); ?></p>
                <?php echo Html::a('Читать далее &rarr;',[$model->seoUrl],['class'=>'read-more']); ?>
                <?php echo Html::a(Html::img($model->getImage(),['alt'=>$model->title]),[$model->seoUrl]); ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

