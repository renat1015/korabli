<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 23.06.14
 * Time: 14:56
 */

namespace app\modules\news\widgets;


use app\modules\news\models\News;
use yii\base\Widget;

class LastNewsWidget extends Widget{
    public function run()
    {
        $models = News::find()->orderBy('created_at desc')->limit(3)->all();
        if($models!=null)
            return $this->render('last_news',['models'=>$models]);
        return false;
    }
} 