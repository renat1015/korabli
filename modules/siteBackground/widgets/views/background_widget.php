<?php
use app\modules\siteBackground\models\SiteBackground;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 30.10.15
 * Time: 13:25
 * @var $model SiteBackground
 * @var $this View
 */
$this->registerJs(<<<JS
// function runBackgroundSlider()
// {
//     var autoPlay = false,
//         autoPlayDelay = 15000,
//         sliderItemClass = '.item',
//         sliderBlock = $('.site-background-widget'),
//         innerBlock = $('.inner',sliderBlock),
//         sliderItems = $('.items',sliderBlock),
//         itemsCount = $(sliderItemClass,sliderItems).size();
//
//     $(innerBlock).css({height:$(window).height()});
//
//     if(autoPlay==true)
//     {
//         playSlider(autoPlayDelay);
//     }
//
//     function playSlider(delay)
//     {
//         if(autoPlay)
//         {
//             setInterval(function(){
//                 sliderItems.find(sliderItemClass+':first').next().fadeIn(1500,function(){
//                     $(this).prev().appendTo(sliderItems).hide();
//                 });
//             },delay);
//         }
//     }
//
//     if(itemsCount>1)
//         autoPlay = true;
//
//     playSlider(autoPlayDelay);
//
//     $(window).resize(function(){
//         $(innerBlock).css({height:$(window).height()});
//     });
// }
//
// runBackgroundSlider();
JS
);
?>
<div class="slick docs-owl">
    <?php foreach($models as $model): ?>
        <?php $img_url = $model->getImage(); ?>
        <div class="item"><img src="<?= $img_url?>" alt="img"></div>
    <?php endforeach; ?>
</div>
