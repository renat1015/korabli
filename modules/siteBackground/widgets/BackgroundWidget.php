<?php
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 30.10.15
 * Time: 13:22
 */

namespace app\modules\siteBackground\widgets;


use app\common\BaseModel;
use app\modules\siteBackground\models\SiteBackground;
use yii\base\Widget;

class BackgroundWidget extends Widget
{
    public function run(){
        $models = SiteBackground::getDb()->cache(function(){
            return SiteBackground::find()->orderBy('RAND()')->limit(3)->all();
        },BaseModel::DEFAULT_CACHE_DURATION,SiteBackground::getDbDependency());

        if($models)
            return $this->render('background_widget',['models'=>$models]);
        return false;
    }
}