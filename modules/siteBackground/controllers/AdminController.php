<?php

namespace app\modules\siteBackground\controllers;

use app\common\BaseAdminController;
use app\common\BaseModel;
use Yii;
use app\modules\siteBackground\models\SiteBackground;
use app\modules\siteBackground\models\SiteBackgroundSearch;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * AdminController implements the CRUD actions for SiteBackground model.
 */
class AdminController extends BaseAdminController
{

    /**
     * Lists all SiteBackground models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteBackgroundSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new SiteBackground model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SiteBackground();
        $model->status = BaseModel::STATUS_PUBLISHED;

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstances($model, 'image');

            foreach ($model->image as $image) {
                $image_model = new SiteBackground();
                $image_model->image = $image;
                $image_model->status = BaseModel::STATUS_PUBLISHED;
                $image_model->save();
            }

            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['index']);

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SiteBackground model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SiteBackground model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteBackground the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteBackground::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
