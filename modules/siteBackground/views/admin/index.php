<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\siteBackground\models\SiteBackgroundSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фоновые изображения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-background-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Загрузить новые', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute'=>'image',
                'format'=>'raw',
                'value'=>function($data){return Html::img($data->getImage(),['style'=>'height:165px;']);},
                'filter' => false,

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}'
            ],
        ],
    ]); ?>

</div>
