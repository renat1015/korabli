<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\siteBackground\models\SiteBackground */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-background-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <?= $form->field($model, 'image[]')->widget(\kartik\file\FileInput::className(),[
        'options'=>[
            'multiple' => true,
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '',
            'allowedFileExtensions' => ['jpg','png'],
            'maxFileSize' => 5120,
            'minImageWidth' => 640,
            'minImageHeight' => 480,
//            'maxFileCount' => 1,
            'previewTemplates'=>[
                'image' => ' <img src="{data}" style="width:auto; height: 165px;" class="file-preview-image pull-left" title="{caption}" alt="{caption}"/>',
            ],
            'initialPreview' => ($model->isNewRecord) ? '' : Html::img($model->getImage(),['style'=>'width:auto; height:165px;'])
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Загрузить' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
