<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\siteBackground\models\SiteBackground */

$this->title = 'Загрузить';
$this->params['breadcrumbs'][] = ['label' => 'Фоновые изображения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-background-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
