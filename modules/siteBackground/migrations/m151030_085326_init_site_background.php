<?php

use yii\db\Schema;
use yii\db\Migration;

class m151030_085326_init_site_background extends Migration
{
    public function up()
    {
        $this->createTable('site_background', [
            'id'         => $this->primaryKey(),
            'image'      => $this->string(255)->notNull(),
            'status'     => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);
    }

    public function down()
    {
        $this->dropTable('site_background');
    }
}
