<?php

namespace app\modules\siteBackground\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "site_background".
 *
 * @property integer $id
 * @property string $image
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 */
class SiteBackground extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_background';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/background/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/background/' . $file_name);
            $this->image = $file_name;

            if(!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/background/' . $this->oldAttributes['image']);

        } else {
            if(!$insert)
                $this->image = $this->oldAttributes['image'];
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return bool|string
     */
    public function getImage()
    {
        if(!$this->image)
            return false;

        if(file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/background/' . $this->image))
        {
            return '/uploads/background/' . $this->image;
        } else
            return false;

    }

    /**
     *
     */
    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/background/' . $this->image);
        parent::afterDelete();
    }
}
