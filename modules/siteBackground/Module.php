<?php

namespace app\modules\siteBackground;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\siteBackground\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
