<?php

namespace app\modules\user\modules;

class RbacModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\user\modules\rbac';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
