<?php

namespace app\modules\user;

use yii\base\BootstrapInterface;
use yii\base\Module;
use yii\console\Application;

class UserModule extends Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\user\controllers';

    public function init()
    {
        parent::init();


    }

    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $this->controllerNamespace = 'app\modules\user\commands';
        }
    }
}
