Щоб модуль працював в protected/config/web.php

 'modules' => [
 .....

     'user' => [
         'class' => 'app\modules\user\UserModule',
     ],

 .....
 ]

'components' => [
.....

    'user' => [
        'identityClass' => 'app\modules\user\models\User',
        'enableAutoLogin' => true,
    ],
    'authManager' => [
        'class' => 'app\modules\user\modules\rbac\components\PhpManager',
    ],

....
]

Запустити екшен для прописування ролей

public function actionSetRoles()
{
      $auth = Yii::$app->authManager;

      $auth->removeAll();

      $authorRule = new \app\modules\user\modules\rbac\rules\AuthorRule;
      $auth->add($authorRule);

      $notGuestRule = new \app\modules\user\modules\rbac\rules\NotGuestRule;
      $auth->add($notGuestRule);

      $manageUsersRule = new \app\modules\user\modules\rbac\rules\ManageUsersRule;
      $auth->add($manageUsersRule);

      // add "createData" permission
      $createData = $auth->createPermission('createData');
      $createData->description = 'Create a data';
      $auth->add($createData);

      $readData = $auth->createPermission('readData');
      $readData->description = 'view news, comment, etc';
      $auth->add($readData);

      // add "updateData" permission
      $updateData = $auth->createPermission('updateData');
      $updateData->description = 'Update data';
      $auth->add($updateData);

      $deleteData = $auth->createPermission('deleteData');
      $deleteData->description = 'delete news, comment, etc';
      $auth->add($deleteData);

      $updateOwnData = $auth->createPermission('updateOwnData');
      $updateOwnData->description = 'update own data';
      $updateOwnData->ruleName = $authorRule->name;
      $auth->add($updateOwnData);

      $deleteOwnData = $auth->createPermission('deleteOwnData');
      $deleteOwnData->description = 'delete own data';
      $deleteOwnData->ruleName = $authorRule->name;
      $auth->add($deleteOwnData);

      $manageUsers = $auth->createPermission('manageUsers');
      $manageUsers->ruleName = $manageUsersRule->name;
      $manageUsers->description = 'manage users';
      $auth->add($manageUsers);

      $guest = $auth->createRole('guest');
      $auth->add($guest);
      $auth->addChild($guest, $readData);

      $user = $auth->createRole(User::ROLE_USER);
      $user->ruleName = $notGuestRule->name;
      $auth->add($user);
      $auth->addChild($user, $guest);
      $auth->addChild($user, $createData);
      $auth->addChild($user, $updateOwnData);
      $auth->addChild($user, $deleteOwnData);

      $admin = $auth->createRole(User::ROLE_ADMIN);
      $auth->add($admin);
      $auth->addChild($admin, $user);
      $auth->addChild($admin, $deleteData);
      $auth->addChild($admin, $manageUsers);
}

В екшені логіна прописати шлях до LoginModel