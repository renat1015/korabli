<?php

namespace app\modules\user\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $role
 * @property integer $status
 * @property string $auth_key
 * @property string $access_token
 * @property string $updated_at
 * @property string $created_at
 * @property string $deleted_at
 *
 */
class User extends BaseModel implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';
    public $newPassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            ['password','required','on'=>['create']],
            [['status'], 'integer'],
            ['email','email'],
            ['email','unique'],
            [['updated_at', 'created_at', 'deleted_at'], 'safe'],
            [['username', 'password', 'role', 'auth_key','access_token'], 'string', 'max' => 255],
            [['password','newPassword'],'string', 'min' => 6],
            [['email'], 'string', 'max' => 45]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['username', 'password','email','role'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'password' => 'Пароль',
            'newPassword' => 'Новый пароль',
            'email' => 'E-mail',
            'role' => 'Роль',
            'status' => 'Status',
            'auth_key' => 'Auth Key',
            'updated_at' => 'Обновлено',
            'created_at' => 'Добавлено',
            'deleted_at' => 'Удалено',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $cacheId = self::defaultCacheId($id);
        $model = self::findByCacheId($cacheId);
        if(!$model)
        {
            $model = self::findOne($id);
            Yii::$app->cache->set($cacheId,$model,BaseModel::DEFAULT_CACHE_DURATION);
        }
        return ($model!=null) ? new static($model) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $model = self::findBySql("SELECT * FROM user WHERE access_token='$token'")->one();
        if($model!=null)
            return new static($model);

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $model = self::findBySql("SELECT * FROM user WHERE username='$username'")->one();
        if($model!=null)
            return new static($model);

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public static function getRoleArray()
    {
        return [
            self::ROLE_USER => 'Пользователь',
            self::ROLE_ADMIN => 'Админ',
        ];
    }

    public function getRole()
    {
        return self::getRoleArray()[$this->role];
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (Yii::$app->getSecurity()->validatePassword($password, $this->password)) {
            return true;
        } else {
            return false;
        }
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function setAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->setAuthKey();
                $this->setPassword($this->password);
            }
            else
            {
                if($this->newPassword!='')
                {
                    $this->setPassword($this->newPassword);
                }
            }
            return true;
        }
        return false;
    }

    public function create()
    {
        if($this->validate())
        {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->save(false);

            $auth = Yii::$app->authManager;
            $userRole = $auth->getRole(self::ROLE_USER);
            $auth->assign($userRole, $user->getId());

            return $user;
        }
        return false;
    }

    static function getList()
    {
        return ArrayHelper::map(self::find()->where(['status'=>0])->all(),'id','username');
    }
}
