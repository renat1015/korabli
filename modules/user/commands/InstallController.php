<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 18.05.15
 * Time: 10:45
 */

namespace app\modules\user\commands;


use app\modules\user\models\User;
use app\modules\user\modules\rbac\rules\AuthorRule;
use app\modules\user\modules\rbac\rules\ManageSiteRule;
use app\modules\user\modules\rbac\rules\NotGuestRule;
use Yii;
use yii\console\Controller;

class InstallController extends Controller{
    public function actionSetRoles()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        $authorRule = new AuthorRule;
        $auth->add($authorRule);

        $notGuestRule = new NotGuestRule;
        $auth->add($notGuestRule);

        $manageSiteRule = new ManageSiteRule();
        $auth->add($manageSiteRule);

        // add "createData" permission
        $createData = $auth->createPermission('createData');
        $createData->description = 'Create a data';
        $auth->add($createData);

        $readData = $auth->createPermission('readData');
        $readData->description = 'View news, comment, etc';
        $auth->add($readData);

        // add "updateData" permission
        $updateData = $auth->createPermission('updateData');
        $updateData->description = 'Update data';
        $auth->add($updateData);

        $deleteData = $auth->createPermission('deleteData');
        $deleteData->description = 'Delete news, comment, etc';
        $auth->add($deleteData);

        $updateOwnData = $auth->createPermission('updateOwnData');
        $updateOwnData->description = 'Update own data';
        $updateOwnData->ruleName = $authorRule->name;
        $auth->add($updateOwnData);

        $deleteOwnData = $auth->createPermission('deleteOwnData');
        $deleteOwnData->description = 'Delete own data';
        $deleteOwnData->ruleName = $authorRule->name;
        $auth->add($deleteOwnData);

        $adminAccess = $auth->createPermission('adminAccess');
        $adminAccess->ruleName = $manageSiteRule->name;
        $adminAccess->description = 'Admin access to site';
        $auth->add($adminAccess);

        $guest = $auth->createRole('guest');
        $auth->add($guest);
        $auth->addChild($guest, $readData);

        $user = $auth->createRole(User::ROLE_USER);
        $user->ruleName = $notGuestRule->name;
        $auth->add($user);
        $auth->addChild($user, $guest);
        $auth->addChild($user, $createData);
        $auth->addChild($user, $updateOwnData);
        $auth->addChild($user, $deleteOwnData);

        $admin = $auth->createRole(User::ROLE_ADMIN);
        $auth->add($admin);
        $auth->addChild($admin, $user);
        $auth->addChild($admin, $deleteData);
        $auth->addChild($admin, $adminAccess);
    }
}