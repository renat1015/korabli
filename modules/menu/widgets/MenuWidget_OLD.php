<?php
namespace app\modules\menu\widgets;

use app\modules\files\models\FileCategory;
use app\modules\gallery\models\Gallery;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;

/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 10.11.14
 * Time: 23:20
 */

class MenuWidget extends Menu
{
    public $items = [], $galleryItems = null;

    public function run()
    {

        if($this->galleryItems==null)
        {
            $this->galleryItems = [
                'label' => Yii::t('menu','Галерея'),
                'url' => Url::to(['/site/gallery/architecture']),
                'options' => ['class'=>'folder'],
                'items' => [
                    [
                        'label' => '3D Визуализация',
                        'url' => '/site/gallery/architecture',
                    ],
//                    [
//                        'label' => 'Архитектура',
//                        'url' => ['/site/gallery/architecture']
//                    ],
//                    [
//                        'label' => 'Ход строительства',
//                        'url' => ['/site/gallery/construction-progress'],
//                    ],
//                    [
//                        'label' => 'Фото галерея',
//                        'url' => ['/site/gallery/list'],
//                    ],
                    [
                        'label' => '3D Виртуальный тур',
//                        'url' => 'http://marshal.nnovgorod3d.ru/3d_tur/ptichka/index.html',
//                        'template' => '<a href="{url}" target="_blank">{label}</a>',
                        'url' => ['/site/gallery/tour']
                    ]
                ]
            ];
        }

        $staticItems = [
            [
                'label' => Yii::t('menu','О застройщике'),
                'url' => ['/site/about/o-zastroichike']
            ],
            [
                'label' => Yii::t('menu','О проекте'),
                'url' => Url::to(['/site/about/pro-korabli']),
                'options' => ['class'=>'folder'],
                'items' => [
                    [
                        'label' => 'Расположение',
                        'url' => ['/site/map-locations']
                    ],
                    [
                        'label' => 'О ЖК КОРАБЛИ',
                        'url' => ['/site/about/pro-korabli'],
//                        'template'=>'<a class="normal-redirect" href="{url}">{label}</a>'
                    ],
                ]
            ],
            [
                'label' => Yii::t('menu','Выбор квартир'),
                'url' => ['/realty/realty'],
//                'template'=>'<a target="_blank" href="{url}">{label}</a>'
            ],
            [
                'label' => Yii::t('menu','Динамика строительства'),
                'url' => Url::to(['/site/gallery/construction-progress']),
                'options' => ['class'=>'folder'],
                'items' => [
                    [
                        'label' => 'Фотоотчеты',
                        'url' => ['/site/gallery/construction-progress']
                    ],
                    [
                        'label' => 'Онлайн камера',
                        'url' => Url::to(['/site/gallery/construction-progress', 'camera'=>'show']),
//                        'template'=>'<a class="normal-redirect" href="{url}">{label}</a>'
                    ],
                ]
            ],
            $this->galleryItems,
//            [
//                'label' => Yii::t('menu','Документы'),
//                'url' => Url::to(['/files/default/index']),
//                'options' => ['class'=>'with-sub default-type'],
//                'items' => [],
//            ],
            [
                'label' => Yii::t('menu','Новости'),
                'url' => Url::to(['/news']),
            ],
            [

                'label' => Yii::t('menu','Контакты'),
                'url' => Url::to(['/site/contact'])
            ]
        ];
        $this->items = ArrayHelper::merge($this->items,$staticItems);
        $this->options = ['class'=>'menu'];
        $this->encodeLabels = false;
        parent::run();
    }

    protected function renderItem($item)
    {
        if (isset($item['url']) && Yii::$app->request->url != $item['url']) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            return strtr($template, [
                '{url}' => Url::to($item['url']),
                '{label}' => (isset($item['content'])) ? $item['content'] : $item['label'],
            ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => (isset($item['content'])) ? $item['content'] : Html::tag('span',$item['label']),
            ]);
        }
    }

    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {

            $route = $item['url'][0];
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }
            if (ltrim($route, '/') !== $this->route) {
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1) {
                foreach (array_splice($item['url'], 1) as $name => $value) {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)) {
                        return false;
                    }
                }
            }

            return true;
        }

        if(isset($item['url']))
        {
            $requestUrl = strtok(Yii::$app->request->url,'?');

            if(Yii::$app->request->url == $item['url'] || $requestUrl==$item['url'])
                return true;

            $requestUrlArray = explode('/',$requestUrl);
            if(is_array($requestUrlArray))
            {
                if(substr($item['url'],1)==$requestUrlArray[1])
                    return true;
                if(isset($requestUrlArray[2]))
                {
                    if(substr($item['url'],1) == $requestUrlArray[1].'/'.$requestUrlArray[2])
                        return true;
                }
            }
        }

        return false;
    }
} 