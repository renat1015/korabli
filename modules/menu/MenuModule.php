<?php

namespace app\modules\menu;

use yii\base\Module;

class MenuModule extends Module
{
    public $controllerNamespace = 'app\modules\menu\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
