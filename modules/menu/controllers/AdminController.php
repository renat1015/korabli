<?php

namespace app\modules\menu\controllers;

use app\common\BaseAdminController;
use app\modules\menu\models\Menu;
use app\modules\menu\models\MenuSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * AdminController implements the CRUD actions for Menu model.
 */
class AdminController extends BaseAdminController
{
    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();
        $model->status = true;
        $model->ordering = $this->getMaxOrder($model->tableName())+1;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if($model->parent_id!=null)
            {
                $parent = Menu::findOne($model->parent_id);
                $model->prependTo($parent);
            }
            else
                $model->makeRoot();
            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(!Yii::$app->request->isPost)
        {
            if($model->tree != $model->id)
            {

                $model->parent_id = $model->tree;
            }
            else
                $model->parent_id = null;

            $model->link_choose = '/'.$model->link;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            $orderingTest = Menu::find()->where(['ordering'=>$model->ordering])->one();
//            if($orderingTest)
//            {
//                $orderingTest->ordering = self::getMaxOrder($model->tableName())+1;
//                $orderingTest->update(false,['ordering']);
//            }
            if($model->parent_id)
            {
                $target = Menu::findOne($model->parent_id);
                if ($target!=null)
                    $model->prependTo($target);
            }
            else{
                if(!$model->isRoot())
                    $model->makeRoot();
                else
                    $model->save(false);
            }

            Yii::$app->session->setFlash('humane','Сохранено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithChildren();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
