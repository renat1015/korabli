<?php

use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use kartik\widgets\TouchSpin;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\menu\models\Menu */
/* @var $form yii\widgets\ActiveForm */
$data = [];
$pages = \app\modules\page\models\Page::getList(false);
$works = \app\modules\work\models\Work::getList(false);

foreach ($works as $work) {
    $data['Роботи'][$work->seoUrl] = $work->title;
}

foreach($pages as $item)
{
   $data['Статичні сторінки'][$item->seoUrl] = $item->title;
}

?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model,'parent_id')->dropDownList($model::getTreeArray(),['prompt'=>'Нова вітка']); ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model,'link_choose')->widget(Select2::classname(), [
        'data' => $data,
        'language' => 'uk',
        'options' => ['placeholder' => 'Оберіть зі списку'],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'pluginEvents' => [
            "change" => "function(){ $('#menu-link').val($(this).val()); }",
        ]
    ]) ?>

    <?= $form->field($model,'ordering',['options'=>['class'=>'form-group ordering-field']])->widget(TouchSpin::className(),[
        'pluginOptions' => ['verticalbuttons' => true]
    ]); ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(), ['pluginOptions' => [
        'size' => 'small',
        'onText' => 'Да',
        'offText' => 'Нет',
    ]]); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Додати' : 'Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style type="text/css">
    .bootstrap-switch > div > span, .bootstrap-switch > div > label{
        height: auto;
    }
</style>
