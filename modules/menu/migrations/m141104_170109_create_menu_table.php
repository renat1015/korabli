<?php

use yii\db\Schema;
use yii\db\Migration;

class m141104_170109_create_menu_table extends Migration
{
    public function up()
    {
        $this->createTable('menu', [
            'id'         => Schema::TYPE_PK,
            'tree'       => Schema::TYPE_INTEGER,
            'lft'        => Schema::TYPE_INTEGER . ' NOT NULL',
            'rgt'        => Schema::TYPE_INTEGER . ' NOT NULL',
            'depth'      => Schema::TYPE_INTEGER . ' NOT NULL',
            'title'      => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название"',
            'link'       => Schema::TYPE_STRING . ' NOT NULL COMMENT "Ссылка"',
            'ordering'   => Schema::TYPE_SMALLINT . ' NOT NULL COMMENT "Сортировка"',
            'status'     => Schema::TYPE_SMALLINT . ' DEFAULT "1" COMMENT "Публиковать"',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('menu');
    }
}
