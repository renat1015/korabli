<?php

namespace app\modules\menu\models;

use app\common\BaseModel;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $title
 * @property string $link
 * @property integer $ordering
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 */
class Menu extends BaseModel
{
    public $parent_id, $parent, $link_choose;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering', 'status','parent_id'], 'integer'],
            [['link', 'ordering'], 'required'],
            [['updated_at', 'created_at'], 'safe'],
            [['title', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tree' => 'Tree',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
            'title' => 'Назва',
            'link' => 'Посилання',
            'parent_id' => 'Вкладеність',
            'ordering' => 'Сортування',
            'link_choose' => 'Пошук посилання (обрати зі списку існуючих посилань)',
            'status' => 'Публікувати',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                 'treeAttribute' => 'tree',
                // 'leftAttribute' => 'lft',
                // 'rightAttribute' => 'rgt',
                // 'depthAttribute' => 'depth',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new MenuQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->link = strtolower($this->link);

        if($this->link!='/' && preg_match('"^/"',$this->link))
        {
            $this->link = substr($this->link,1);
        }
        return parent::beforeSave($insert);
    }

    public static function getTreeArray($model = false)
    {
        if($model)
            $items = self::find()->where("id!={$model->id} AND tree!=".($model->tree != $model->id ? $model->id : $model->tree))->orderBy('tree, lft')->all();
        else
            $items = self::find()->orderBy('tree, lft')->all();

        $list = [];
        foreach ($items as $item) {
            $list[$item->id] = $item->depth > 0 ? str_repeat('- ', $item->depth).$item->title : $item->title;
        }

        return $list;
    }

    static function buildMenu(){
        $roots = self::find()->active()->roots()->orderBy('ordering asc')->all();
        $items = [];
        foreach($roots as $root)
        {
            $childItems = self::getItemChild($root);
            if($childItems)
            {
                $items[] = [
                    'label' => $root->title,
                    'url' => '/'.$root->link,
                    'items'=>$childItems
                ];
            }
            else
            {
                $items[] = [
                    'label' => $root->title,
                    'url' => $root->getLinkForRender(),
                ];
            }

        }
        return $items;
    }

    static function getItemChild($root)
    {
        $items = [];

        $models = $root->leaves()->all();

        if(empty($models))
            return false;
        else
        {
            foreach($models as $model)
            {
                $items[] = [
                    'label' => $model->title,
                    'url' => '/'.$model->link
                ];
            }
            return $items;
        }
    }

    private function getLinkForRender()
    {
        if(preg_match('"^http://"',$this->link) || preg_match('"^www"',$this->link))
            return $this->link;
        elseif($this->link!='/')
            return '/'.$this->link;
        else
            return '/';
    }
}
