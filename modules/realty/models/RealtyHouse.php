<?php

namespace app\modules\realty\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "realty_house".
 *
 * @property integer         $id
 * @property string          $title
 * @property string          $description
 * @property string          $image
 * @property integer         $floors
 * @property integer         $flats
 * @property integer         $plan_id
 * @property string          $cords
 * @property string          $updated_at
 * @property string          $created_at
 * @property string          $video
 * @property string          $youtube_video
 * @property integer         $status
 *
 * @property RealtyFlat[]    $realtyFlats
 * @property RealtyFloor[]   $realtyFloors
 * @property RealtyPlan      $plan
 * @property RealtySection[] $realtySections
 */
class RealtyHouse extends BaseModel
{
    public $delete_video = false;

    const STATUS_ACTIVE = 1;
    const STATUS_QUEUE  = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'realty_house';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'floors', 'flats', 'plan_id', 'cords'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['description', 'cords', 'house_number'], 'string'],
            [['floors', 'flats', 'plan_id', 'status'], 'integer'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['video'], 'file', 'skipOnEmpty' => true],
            [['updated_at', 'created_at', 'delete_video'], 'safe'],
            [['title', 'youtube_video'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Название',
            'description'   => 'Описание',
            'image'         => 'Картинка для выбора квартир/секций',
            'floors'        => 'Количество этажей',
            'flats'         => 'Количество квартир',
            'plan_id'       => 'Генеральный план',
            'cords'         => 'Координаты на генеральном плане',
            'video'         => 'Видео файл для перехода',
            'youtube_video' => 'Видео файл для перехода (ссылка на YouTube)',
            'updated_at'    => 'Дата обновления',
            'created_at'    => 'Дата добавления',
            'status'        => 'Статус дома',
            'delete_video'  => 'Убрать видео после сохранения',
            'house_number'  => 'Номер дома',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyFlats()
    {
        return $this->hasMany(RealtyFlat::className(), ['house_id' => 'id']);
    }

    public function getSaleFlats()
    {
        return $this->hasMany(RealtyFlat::className(), ['house_id' => 'id'])->where(['sale_status' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyFloors()
    {
        return $this->hasMany(RealtyFloor::className(), ['house_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(RealtyPlan::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtySections()
    {
        return $this->hasMany(RealtySection::className(), ['house_id' => 'id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        $this->cords = str_ireplace(['NaN,', ',NaN', ',,'], '', $this->cords);

        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/house/video');

        //save image
        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/house/' . $file_name);
            $this->image = $file_name;

            if (!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/house/' . $this->oldAttributes['image']);

        } else {
            if (!$insert)
                $this->image = $this->oldAttributes['image'];
        }

        //save video
        if ($this->video) {
            $file_name = md5(microtime()) . '.' . $this->video->extension;
            $this->video->saveAs('uploads/house/video/' . $file_name);
            $this->video = $file_name;

            if (!$insert && $this->oldAttributes['video'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/house/video/' . $this->oldAttributes['video']);

        } else {
            if (!$insert)
                $this->video = $this->oldAttributes['video'];
        }

        if (!$this->isNewRecord && $this->delete_video)
            $this->video = $this->youtube_video = null;

        return parent::beforeSave($insert);
    }

    /**
     * @return bool|string
     */
    public function getVideoFile()
    {
        if (!$this->video)
            return false;
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/house/video/' . $this->video)) {
            return '/uploads/house/video/' . $this->video;
        } else
            return false;
    }

    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/house/' . $this->image)) {
            return '/uploads/house/' . $this->image;
        } else
            return false;
    }

    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/house/' . $this->image);
        parent::afterDelete();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList()
    {
        return self::find()->where(['status' => BaseModel::STATUS_PUBLISHED])->all();
    }

    public function getSeoUrl()
    {
        return Url::to(['/realty/realty/view-house','id'=>$this->id]);
    }

    public function beforeDelete()
    {
        if ($this->realtyFloors)
            foreach ($this->realtyFloors as $floor) {
                $floor->delete();
            }

        if ($this->realtySections)
            foreach ($this->realtySections as $section) {
                $section->delete();
            }

        return parent::beforeDelete();
    }

    /**
     * @return bool
     */
    public function getVideoCode()
    {

        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->youtube_video, $match))
            return $match[1];

        return false;

    }

    /**
     * @return string
     */
    public function getVideoSource()
    {
        if ($this->video) {
            $videoUrl = $this->getVideoFile();
            $data = explode('.', $videoUrl);
            $ext = end($data);

            return "<source src='$videoUrl' type='video/$ext'>";
        }
        return false;
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_QUEUE  => 'В очереди',
        ];
    }

}
