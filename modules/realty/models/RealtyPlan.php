<?php

namespace app\modules\realty\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "realty_plan".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $updated_at
 * @property string $created_at
 * @property integer $status
 *
 * @property RealtyHouse[] $realtyHouses
 */
class RealtyPlan extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'realty_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['updated_at', 'created_at'], 'safe'],
            [['status'], 'integer'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'title'      => 'Название',
            'image'      => 'Изображение',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
            'status'     => 'Публиковать',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyHouses()
    {
        return $this->hasMany(RealtyHouse::className(), ['plan_id' => 'id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/plan/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/plan/' . $file_name);
            $this->image = $file_name;

            if (!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/plan/' . $this->oldAttributes['image']);

        } else {
            if (!$insert)
                $this->image = $this->oldAttributes['image'];
        }

        return parent::beforeSave($insert);
    }


    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/plan/' . $this->image)) {
            return '/uploads/plan/' . $this->image;
        } else
            return false;
    }

    /**
     *
     */
    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/plan/' . $this->image);
        parent::afterDelete();

    }

    public function getSeoUrl()
    {
        return Url::to(['/realty/realty/index']);
    }
}
