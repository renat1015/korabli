<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\RealtyFloor;

/**
 * RealtyFloorSearch represents the model behind the search form about `app\modules\realty\models\RealtyFloor`.
 */
class RealtyFloorSearch extends RealtyFloor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'house_id', 'section_id', 'status'], 'integer'],
            [['title', 'description', 'image', 'cords', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RealtyFloor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'house_id'   => $this->house_id,
            'section_id' => $this->section_id,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
            'status'     => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'cords', $this->cords]);

        return $dataProvider;
    }
}
