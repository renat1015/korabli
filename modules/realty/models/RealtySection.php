<?php

namespace app\modules\realty\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "realty_section".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $cords
 * @property integer $house_id
 * @property string $updated_at
 * @property string $created_at
 * @property integer $status
* @property integer       $ordering
 *
 * @property RealtyFlat[] $realtyFlats
 * @property RealtyFloor[] $realtyFloors
 * @property RealtyHouse $house
 */
class RealtySection extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'realty_section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'cords', 'house_id'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['description', 'cords'], 'string'],
            [['house_id', 'status', 'ordering'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'title'       => 'Название',
            'description' => 'Описание',
            'image'       => 'Изображение для выбора этажа',
            'cords'       => 'Координаты',
            'house_id'    => 'Дом',
            'updated_at'  => 'Updated At',
            'created_at'  => 'Created At',
            'ordering'  => 'Сортировка',
            'status'      => 'Публиковать',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyFlats()
    {
        return $this->hasMany(RealtyFlat::className(), ['section_id' => 'id']);
    }

    public function getSaleFlats()
    {
        return $this->hasMany(RealtyFlat::className(), ['section_id' => 'id'])->where(['sale_status' => 1]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyFloors()
    {
        return $this->hasMany(RealtyFloor::className(), ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(RealtyHouse::className(), ['id' => 'house_id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        $this->cords = str_ireplace(['NaN,', ',NaN', ',,'], '', $this->cords);

        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/section/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/section/' . $file_name);
            $this->image = $file_name;

            if (!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/section/' . $this->oldAttributes['image']);

        } else {
            if (!$insert)
                $this->image = $this->oldAttributes['image'];
        }

        //ordering
        if($this->isNewRecord)
        {
            $max = self::find()->where(['house_id' => $this->house_id])->max('ordering');
            if(!$max)
                $this->ordering = 1;
            else
                $this->ordering = $max + 1;
        }

        return parent::beforeSave($insert);
    }


    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/section/' . $this->image)) {
            return '/uploads/section/' . $this->image;
        } else
            return false;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function beforeDelete()
    {
        if ($this->realtyFloors)
            foreach ($this->realtyFloors as $floor) {
                $floor->delete();
            }

        return parent::beforeDelete();
    }


    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/section/' . $this->image);
        parent::afterDelete();
    }

    /**
     * @return string
     */
    public function getSeoUrl()
    {
        return Url::to(['/realty/realty/view-section','id'=>$this->id]);
    }
}
