<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\RealtyFlat;
use yii\helpers\ArrayHelper;

/**
 * RealtyFlatSearch represents the model behind the search form about `app\modules\realty\models\RealtyFlat`.
 */
class RealtyFlatSearch extends RealtyFlat
{

    public $price_to, $price_from, $scale_to, $scale_from;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'house_id', 'section_id', 'floor_id', 'rooms', 'status', 'sale_status', 'price_to', 'price_from', 'scale_to', 'scale_from'], 'integer'],
            [['title', 'description', 'image', 'cords', 'updated_at', 'created_at', 'rooms_scale'], 'safe'],
            [['price', 'scale', 'scale_lobby', 'scale_living', 'scale_kitchen', 'scale_bathroom'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RealtyFlat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'             => $this->id,
            'house_id'       => $this->house_id,
            'section_id'     => $this->section_id,
            'floor_id'       => $this->floor_id,
            'scale'          => $this->scale,
            'rooms'          => $this->rooms,
            'updated_at'     => $this->updated_at,
            'created_at'     => $this->created_at,
            'status'         => $this->status,
            'sale_status'    => $this->sale_status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'cords', $this->cords])
            ->andFilterWhere(['like', 'rooms_scale', $this->rooms_scale])
            ->andFilterWhere(['>', 'price', $this->price_to])
            ->andFilterWhere(['<', 'price', $this->price_from])
            ->andFilterWhere(['>', 'scale', $this->scale_to])
            ->andFilterWhere(['<', 'scale', $this->scale_from]);

        return $dataProvider;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'price_to'   => 'Цена от',
                'price_from' => 'Цена до',
                'scale_to'   => 'Площадь от',
                'scale_from' => 'Площадь до',
            ]
        );
    }

}
