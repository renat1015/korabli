<?php

namespace app\modules\realty\models;

use app\common\BaseModel;
use app\modules\image\behaviors\ImagesBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "realty_flat".
 *
 * @property integer       $id
 * @property string        $title
 * @property string        $description
 * @property string        $image
 * @property string        $cords
 * @property integer       $house_id
 * @property integer       $section_id
 * @property integer       $floor_id
 * @property double        $price
 * @property double        $scale
 * @property integer       $rooms
 * @property string        $updated_at
 * @property string        $created_at
 * @property string        $rooms_scale
 * @property integer       $status
 * @property integer       $sale_status
 *
 * @property RealtyFloor   $floor
 * @property RealtyHouse   $house
 * @property RealtySection $section
 */
class RealtyFlat extends BaseModel
{
    public $room_type, $room_scale, $rooms_array, $is_copy = false, $imgFiles;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'realty_flat';
    }

    //flat status
    const SALE_STATUS_IN_SALE  = 1;
    const SALE_STATUS_SALE     = 2;
    const SALE_STATUS_RESERVED = 3;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'cords', 'house_id', 'floor_id', 'price', 'scale'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['description', 'cords', 'image_zh'], 'string'],
            [['house_id', 'section_id', 'floor_id', 'rooms', 'status', 'sale_status', 'side', 'number'], 'integer'],
            [['price', 'scale', 'living_scale'], 'number'],
            [['updated_at', 'created_at', 'room_type', 'room_scale', 'rooms_scale', 'is_copy'], 'safe'],
            [['title', 'tour'], 'string', 'max' => 255],
        ];
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();

        return ArrayHelper::merge($behaviors, [
            'imagesBehavior' => [
                'class'     => ImagesBehavior::className(),
                'attribute' => 'imgFiles',
                'model'     => strtolower(self::getModelName()),
            ],
//            'seo'            => [
//                'class'         => 'app\modules\seo\behaviors\SeoBehavior',
//                'model'         => $this->getModelName(),
//                'view_category' => 'review',
//                'view_action'   => 'review/default/view'
//            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'title'       => 'Название',
            'description' => 'Описание',
            'image'       => 'План',
            'cords'       => 'Координаты',
            'house_id'    => 'Дом',
            'section_id'  => 'Секция',
            'floor_id'    => 'Этаж',
            'price'       => 'Цена',
            'scale'       => 'Площадь',
            'rooms'       => 'Комнат',
            'updated_at'  => 'Дата обновления',
            'created_at'  => 'Дата добавления',
            'status'      => 'Публиковать',
            'room_scale'  => 'Площадь',
            'room_type'   => 'Тип комнаты',
            'imgFiles'    => 'Вид из окна',
            'sale_status' => 'Статус квартиры',
            'living_scale' => 'Жилая площадь',
            'tour' => 'Ссылка на 3D тур',
            'side' => 'Сторона света',
            'number' => 'Номер квартиры',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFloor()
    {
        return $this->hasOne(RealtyFloor::className(), ['id' => 'floor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(RealtyHouse::className(), ['id' => 'house_id']);
    }

    /**
     * @return \yii\db\ActiveQuery0
     */
    public function getSection()
    {
        return $this->hasOne(RealtySection::className(), ['id' => 'section_id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {

        $this->cords = str_ireplace(['NaN,', ',NaN', ',,'], '', $this->cords);

        if (!$this->is_copy) {

            $rooms = [];
            if ($this->room_type) {

                foreach ($this->room_type as $key => $value) {
                    if (isset($this->room_scale[$key]))
                        $rooms[] = [
                            'name'  => $value,
                            'value' => $this->room_scale[$key],
                        ];
                }
            }

            $this->rooms_scale = serialize($rooms);

            FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/flat/');

            if (isset($this->image->extension)) {
                $file_name = md5(microtime()) . '.' . $this->image->extension;
                $this->image->saveAs('uploads/flat/' . $file_name);
                $this->image = $file_name;

                if (!$insert && $this->oldAttributes['image'] != $file_name)
                    @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/flat/' . $this->oldAttributes['image']);

            } else {
                if (!$insert)
                    $this->image = $this->oldAttributes['image'];
            }
        } else {
            $this->rooms_scale = serialize($this->rooms_scale);
        }

        return parent::beforeSave($insert);
    }


    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/flat/' . $this->image)) {
            return '/uploads/flat/' . $this->image;
        } else
            return false;
    }

    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/flat/' . $this->image);
        parent::afterDelete();
    }

    /**
     * @return array
     */
    public static function getRoomTypes()
    {
        $list = [
            'Прихожая',
            'Жилая комната',
            'Кухня',
            'Сан-узел',
            'Кладовая',
            'Балкон',
            'Лоджия',
        ];

        $items = [];
        foreach ($list as $item) {
            $items[$item] = $item;
        }

        return $items;
    }

    public function afterFind()
    {
        $this->rooms_scale = unserialize($this->rooms_scale);
        $this->rooms_array = $this->rooms_scale;
        parent::afterFind();
    }

    public function getSeoUrl()
    {
        return Url::to(['/realty/realty/view-flat','id'=>$this->id]);
    }

    /**
     * @return array
     */
    public static function getSaleStatusList()
    {
        return [
            self::SALE_STATUS_IN_SALE  => "В продаже",
            self::SALE_STATUS_SALE     => "Продано",
            self::SALE_STATUS_RESERVED => "Зарезервировано",
        ];
    }

    public function getSaleStatusName()
    {
        $list = self::getSaleStatusList();

        return $list[$this->sale_status];
    }

    public static function getApartmentsByHouseId($id)
    {
        $list = self::find()->where(['house_id'=>$id, 'sale_status'=>1, 'status'=>RealtyFlat::STATUS_PUBLISHED])->all();

        if (count($list) > 0) {
            $arr=[];
            foreach ($list as $item) {
                if (!empty($arr[$item->rooms])) {
                    $arr[$item->rooms] += 1;
                } else {
                    $arr[$item->rooms] = 1;
                }
            }
            return $arr;
        } else {
            return false;
        }
    }
}
