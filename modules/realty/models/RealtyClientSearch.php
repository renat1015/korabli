<?php

namespace app\modules\realty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\realty\models\RealtyClient;

/**
 * RealtyClientSearch represents the model behind the search form about `app\modules\realty\models\RealtyClient`.
 */
class RealtyClientSearch extends RealtyClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment', 'mortgage', 'source', 'flat_id', 'status', 'stage', 'house_id', 'section_id', 'floor_id'], 'integer'],
            [['full_name', 'phone', 'email', 'first_date', 'last_date', 'next_date', 'comment', 'files', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RealtyClient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'first_date' => $this->first_date,
            'last_date' => $this->last_date,
            'next_date' => $this->next_date,
            'payment' => $this->payment,
            'mortgage' => $this->mortgage,
            'source' => $this->source,
            'flat_id' => $this->flat_id,
            'status' => $this->status,
            'stage' => $this->stage,
            'floor_id' => $this->floor_id,
            'section_id' => $this->section_id,
            'house_id' => $this->house_id,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'files', $this->files]);

        return $dataProvider;
    }
}
