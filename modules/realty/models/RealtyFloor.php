<?php

namespace app\modules\realty\models;

use app\common\BaseModel;
use app\modules\image\models\Image;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "realty_floor".
 *
 * @property integer       $id
 * @property string        $title
 * @property string        $description
 * @property string        $image
 * @property string        $cords
 * @property integer       $house_id
 * @property integer       $section_id
 * @property string        $updated_at
 * @property string        $created_at
 * @property string        $flat_count
 * @property integer       $status
 * @property integer       $ordering
 * @property integer       $scale
 *
 * @property RealtyFlat[]  $realtyFlats
 * @property RealtyHouse   $house
 * @property RealtySection $section
 */
class RealtyFloor extends BaseModel
{
    public $is_copy = false;
    public $copy_flats;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'realty_floor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'cords', 'house_id'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['description', 'cords'], 'string'],
            [['house_id', 'section_id', 'status', 'flat_count', 'ordering'], 'integer'],
            [['updated_at', 'created_at', 'scale', 'is_copy', 'copy_flats'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'title'       => 'Название',
            'description' => 'Описание',
            'image'       => 'План этажа',
            'cords'       => 'Координаты',
            'flat_count'  => 'Количество квартир',
            'scale'       => 'Площадь',
            'house_id'    => 'Дом',
            'section_id'  => 'Секция',
            'updated_at'  => 'Дата обновления',
            'created_at'  => 'Дата добавления',
            'status'      => 'Публиковать',
            'ordering'    => 'Сортировка',
            'copy_flats'  => 'Квартиры для копирования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyFlats()
    {
        return $this->hasMany(RealtyFlat::className(), ['floor_id' => 'id']);
    }

    public function getSaleFlats()
    {
        return $this->hasMany(RealtyFlat::className(), ['floor_id' => 'id'])->where(['sale_status' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(RealtyHouse::className(), ['id' => 'house_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(RealtySection::className(), ['id' => 'section_id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        $this->cords = str_ireplace(['NaN,', ',NaN', ',,'], '', $this->cords);

        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/');

        if ($this->is_copy) {
            $original = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/' . $this->image;

            $new_name = substr(md5(microtime()), 0, 5) . $this->image;
            $new = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/' . $new_name;
            if (copy($original, $new))
                $this->image = $new_name;

        } else {
            if (isset($this->image->extension)) {
                $file_name = md5(microtime()) . '.' . $this->image->extension;
                $this->image->saveAs('uploads/floor/' . $file_name);
                $this->image = $file_name;

                if (!$insert && $this->oldAttributes['image'] != $file_name)
                    @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/' . $this->oldAttributes['image']);

            } else {
                if (!$insert)
                    $this->image = $this->oldAttributes['image'];
            }
        }

        //ordering
        if($this->isNewRecord)
        {
            $query = self::find();
            $query
                ->andFilterWhere(['house_id' => $this->house_id])
                ->andFilterWhere(['section_id' => $this->section_id])
            ;

            $max = $query->max('ordering');

            if(!$max) $this->ordering = 1;
            else $this->ordering = $max + 1;
        }

        return parent::beforeSave($insert);
    }


    public function afterSave($insert, $changedAttributes)
    {
        if ($this->is_copy) {
            if ($this->copy_flats) {
                foreach ($this->copy_flats as $flat) {
                    $f_model = RealtyFlat::findOne($flat);
                    if ($f_model) {
                        $original = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/flat/' . $f_model->image;
                        $new_name = substr(md5(microtime()), 0, 5) . $f_model->image;
                        $new = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/flat/' . $new_name;

                        if (copy($original, $new))
                            $f_model->image = $new_name;

                        $original_id = $f_model->id;

                        $f_model->is_copy = true;
                        $f_model->id = null;
                        $f_model->setIsNewRecord(true);
                        $f_model->floor_id = $this->id;
                        if($f_model->save(false))
                        {
                            Image::cloneToOther(RealtyFlat::getModelName(),$original_id,RealtyFlat::getModelName(),$f_model->id);
                        }
                    }
                }
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/' . $this->image)) {
            return '/uploads/floor/' . $this->image;
        } else
            return false;
    }

    public function getImgSize()
    {
        $path = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/' . $this->image;
        if(file_exists($path))
            return getimagesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/' . $this->image);
        return false;
    }

    public function beforeDelete()
    {
        if ($this->realtyFlats)
            foreach ($this->realtyFlats as $flat) {
                $flat->delete();
            }

        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/' . $this->image);
        parent::afterDelete();
    }

    public function getSeoUrl()
    {
        return Url::to(['/realty/realty/view-floor','id'=>$this->id]);
    }
}
