<?php

use app\modules\realty\models\RealtyClient;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyClient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="realty-client-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'house_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtyHouse::getList(), 'id', 'title'), [
                'prompt' => 'Выберите дом',
            ]) ?>
            </div>
        <div class="col-md-4">
            <?= $form->field($model, 'section_id')->dropDownList($sections) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'floor_id')->dropDownList($floors) ?>
        </div>
    </div>

    <?= $form->field($model, 'flat_id')->dropDownList($flats) ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'status')->dropDownList(RealtyClient::getStatusList()) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'stage')->dropDownList(RealtyClient::getStageList()) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'first_date')->widget(\kartik\datetime\DateTimePicker::className()) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'next_date')->widget(\kartik\datetime\DateTimePicker::className()) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'last_date')->widget(\kartik\datetime\DateTimePicker::className()) ?>
        </div>

        <div class="col-lg-8">
            <?= $form->field($model, 'payment')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'mortgage')->dropDownList([0=>'Нет', 1=>'Да']) ?>
        </div>

    </div>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'source')->dropDownList(RealtyClient::getSourceList()) ?>

    <?= $form->field($model, 'files[]')->widget(\kartik\widgets\FileInput::className(), [
        'options' => [
            'multiple' => true,
        ],
        'pluginOptions' => [
            'showPreview' => false,
            'showUpload' => false,
        ]
    ]) ?>

    <?php if(!$model->isNewRecord) {?>
        <div class="form-group field-realtyclient-source">
            <label class="control-label">Загруженные файлы</label>
            <?php
            if($model->files){
                echo Html::beginTag('ul');
                foreach ($model->files as $file) {
                    $f = str_replace(Yii::$app->basePath . '/..', '', $file);
                    echo Html::beginTag('li');
                    echo Html::a($f, $f);
                    echo '&nbsp';
                    echo Html::a('<i class="fa fa-trash-o"></i>', ['/realty/client/delete-file', 'file' => $file], ['class' => 'del-file']);
                    echo Html::endTag('li');
                }
                echo Html::endTag('ul');
            }

            ?>
        </div>
    <?php }?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$this->registerJs('
        $("#realtyclient-house_id").change(function(){
            $(\'#realtyclient-section_id\').html("");
            $(\'#realtyclient-floor_id\').html("");
            getList("section");
        });

        $("#realtyclient-section_id").change(function(){
            $(\'#realtyclient-floor_id\').html("");
            getList("floor");
        });

        $("#realtyclient-floor_id").change(function(){
            $(\'#realtyclient-flat_id\').html("");
            getList("flat");
        });

        $(".del-file").click(function(){
            deleteFile($(this));
            return false;
        });

    ');
?>
<script>

    function deleteFile(el)
    {
        $.post($(el).attr('href'), function (result) {
            $(el).parents('li').fadeOut();
        });

    }

    function getList(type) {
        var id, html;

        if (type == 'section')
            id = $('#realtyclient-house_id').val();
        else if (type == 'floor')
            id = $('#realtyclient-section_id').val();
        else if (type == 'flat')
            id = $('#realtyclient-floor_id').val();

        $.post('/realty/realty/get-data-list', {id: id, type: type}, function (result) {
            if (type == 'section') {
                if (result.sections) {
                    html = '<option value="">Выберите секцию</option>';
                    $.each(result.sections, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyclient-section_id').html(html);
                } else if (result.floors) {
                    html = '<option value="">Выберите Этаж</option>';
                    $.each(result.floors, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyclient-section_id').html('<option value="">У дома нет секций, выберите этаж...</option>');
                    $('#realtyclient-floor_id').html(html);
                }
            } else if (type == 'floor') {
                if (result.floors) {
                    html = '<option value="">Выберите Этаж</option>';
                    $.each(result.floors, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyclient-floor_id').html(html);
                } else {
                    $('#realtyclient-floor_id').html('<option value="">Нет этажей для выбора, смените секцию или дом</option>');
                }
            } else if (type == 'flat')
            {
                if (result.flats) {
                    html = '<option value="">Выберите квартиру</option>';
                    $.each(result.flats, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + ' (' + item.scale + 'кв.м, цена '+ item.price +')</option>'
                    });
                    $('#realtyclient-flat_id').html(html);
                } else {
                    $('#realtyclient-flat_id').html('<option value="">Нет квартир для выбора, смените секцию или дом</option>');
                }
            }
        }, "json");
    }
</script>
