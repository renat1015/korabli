<?php


use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\RealtyClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realty-client-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider'     => $dataProvider,
        'filterModel'      => $searchModel,
        'pjax'             => true,
        'pjaxSettings'     => [
            'neverTimeout' => true,
        ],
        'columns'          => [
            [
                'class'          => 'kartik\grid\SerialColumn',
                'contentOptions' => ['class' => 'kartik-sheet-style'],
                'width'          => '36px',
                'header'         => '',
                'headerOptions'  => ['class' => 'kartik-sheet-style'],
            ],
            [
                'class'         => 'kartik\grid\ExpandRowColumn',
                'width'         => '50px',
                'value'         => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail'        => function ($model, $key, $index, $column) {
                    return Yii::$app->controller->renderPartial('view', ['model' => $model]);
                },
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true,
            ],
            [
                'format'              => 'raw',
                'attribute'           => 'house_id',
                'filterType'          => GridView::FILTER_SELECT2,
                'filter'              => ArrayHelper::map(\app\modules\realty\models\RealtyHouse::find()->all(), 'id', 'title'),
                'filterInputOptions'  => [
                    'placeholder' => 'Все',
                ],
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'value'               => function ($model) {
                    return $model->house->title;
                },
            ],
            [
                'format'     => 'raw',
                'attribute'  => 'section_id',
                'filterType' => GridView::FILTER_SELECT2,
                'filter'     => ArrayHelper::map(\app\modules\realty\models\RealtySection::find()->where(['house_id' => $searchModel->house_id])->all(), 'id', 'title'),

                'filterInputOptions'  => [
                    'placeholder' => 'Все',
                ],
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'value'               => function ($model) {
                    if (isset($model->section->title))
                        return $model->section->title;
                },
            ],
            [
                'format'              => 'raw',
                'attribute'           => 'floor_id',
                'filterType'          => GridView::FILTER_SELECT2,
                'filter'              => ArrayHelper::map(\app\modules\realty\models\RealtyFloor::find()->andFilterWhere(['section_id' => $searchModel->section_id])->andFilterWhere(['house_id' => $searchModel->house_id])->asArray()->all(), 'id', 'title'),
                'filterInputOptions'  => [
                    'placeholder' => 'Все',
                ],
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'value'               => function ($model) {
                    return $model->floor->title;
                },
            ],
            [
                'format'              => 'raw',
                'attribute'           => 'flat_id',
                'value'               => function ($model) {
                    return Html::a($model->flat_id . ' ('.$model->flat->scale .' кв.м.)', ['/realty/realty/view-flat', 'id' => $model->flat_id], ['target' => '_blank']);
                },
            ],
            [
                'format'              => 'raw',
                'attribute'           => 'full_name',
                'filterType'          => GridView::FILTER_SELECT2,
                'filter'              => ArrayHelper::map(\app\modules\realty\models\RealtyClient::find()->groupBy('full_name')->asArray()->all(), 'full_name', 'full_name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions'  => [
                    'placeholder' => 'Все',
                ],
            ],
            [
                'format'              => 'raw',
                'attribute'           => 'email',
                'filterType'          => GridView::FILTER_SELECT2,
                'filter'              => ArrayHelper::map(\app\modules\realty\models\RealtyClient::find()->groupBy('email')->asArray()->all(), 'email', 'email'),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions'  => [
                    'placeholder' => 'Все',
                ],
            ],
            [
                'format'              => 'raw',
                'attribute'           => 'phone',
                'filterType'          => GridView::FILTER_SELECT2,
                'filter'              => ArrayHelper::map(\app\modules\realty\models\RealtyClient::find()->groupBy('phone')->asArray()->all(), 'phone', 'phone'),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions'  => [
                    'placeholder' => 'Все',
                ],
            ],
            array(
                'format'              => 'raw',
                'attribute'           => 'status',
                'value'               => function ($model) {
                    return \app\modules\realty\models\RealtyClient::getStatusList($model->status);
                },
                'filterType'          => GridView::FILTER_SELECT2,
                'filter'              => \app\modules\realty\models\RealtyClient::getStatusList(),
                'filterWidgetOptions' => array(
                    'pluginOptions' => array(
                        'allowClear' => true,
                    ),
                ),
                'filterInputOptions'  => array(
                    'placeholder' => 'Все',
                ),
            ),
            array(
                'format'              => 'raw',
                'attribute'           => 'stage',
                'value'               => function ($model) {
                    return \app\modules\realty\models\RealtyClient::getStageList($model->stage);
                },
                'filterType'          => GridView::FILTER_SELECT2,
                'filter'              => \app\modules\realty\models\RealtyClient::getStageList(),
                'filterWidgetOptions' => array(
                    'pluginOptions' => array(
                        'allowClear' => true,
                    ),
                ),
                'filterInputOptions'  => array(
                    'placeholder' => 'Все',
                ),
            ),
            [
                'format'              => 'raw',
                'attribute'           => 'source',
                'value'               => function ($model) {
                    return \app\modules\realty\models\RealtyClient::getSourceList($model->source);
                },
                'filterType'          => GridView::FILTER_SELECT2,
                'filter'              => \app\modules\realty\models\RealtyClient::getSourceList(),
                'filterWidgetOptions' => array(
                    'pluginOptions' => array(
                        'allowClear' => true,
                    ),
                ),
                'filterInputOptions'  => array(
                    'placeholder' => 'Все',
                ),
            ],
            [
                'class'     => 'kartik\grid\BooleanColumn',
                'attribute' => 'mortgage',
                'vAlign'    => 'middle',
            ],

            [
                'class' => 'kartik\grid\ActionColumn',
            ],
        ],
        'containerOptions' => ['class' => 'email-pjax-container'],
        'options'          => ['id' => 'email-pjax'],
        'panel'            => [
            'after'  => false,
            'footer' => false,
        ],
        'responsive'       => true,
        'hover'            => true,
        'export'           => [
            'fontAwesome' => true,
        ],
    ]);

    ?>

</div>
<?php $this->registerJs("
$(document).on( 'click','.dropdown-toggle', function(){
        $(this).next().toggle();
    })
") ?>

<?php
$this->registerJs('
        $("#realtyclientsearch-house_id").change(function(){
            $(\'#realtyclientsearch-section_id\').html("");
            $(\'#realtyclientsearch-floor_id\').html("");
            getList("section");
        });

        $("#realtyclientsearch-section_id").change(function(){
            $(\'#realtyclientsearch-floor_id\').html("");
            getList("floor");
        });

        $("#realtyclientsearch-floor_id").change(function(){
            $(\'#realtyclientsearch-flat_id\').html("");
            getList("flat");
        });
    ');
?>
<script>

    function getList(type) {
        var id, html;

        console.log(type);

        if (type == 'section')
            id = $('#realtyclientsearch-house_id').val();
        else if (type == 'floor')
            id = $('#realtyclientsearch-section_id').val();
        else if (type == 'flat')
            id = $('#realtyclientsearch-floor_id').val();

        console.log(id);

        $.post('/realty/realty/get-data-list', {id: id, type: type}, function (result) {
            if (type == 'section') {
                if (result.sections) {
                    html = '<option value="">Выберите секцию</option>';
                    $.each(result.sections, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyclientsearch-section_id').html(html);
                } else if (result.floors) {
                    html = '<option value="">Выберите Этаж</option>';
                    $.each(result.floors, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyclientsearch-section_id').html('<option value="">У дома нет секций, выберите этаж...</option>');
                    $('#realtyclientsearch-floor_id').html(html);
                }
            } else if (type == 'floor') {
                if (result.floors) {
                    html = '<option value="">Выберите Этаж</option>';
                    $.each(result.floors, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyclientsearch-floor_id').html(html);
                } else {
                    $('#realtyclientsearch-floor_id').html('<option value="">Нет этажей для выбора, смените секцию или дом</option>');
                }
            } else if (type == 'flat') {
                if (result.flats) {
                    html = '<option value="">Выберите квартиру</option>';
                    $.each(result.flats, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + ' (' + item.scale + 'кв.м, цена ' + item.price + ')</option>'
                    });
                    $('#realtyclientsearch-flat_id').html(html);
                } else {
                    $('#realtyclientsearch-flat_id').html('<option value="">Нет квартир для выбора, смените секцию или дом</option>');
                }
            }
        }, "json");
    }
</script>

