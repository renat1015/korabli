<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyClient */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Realty Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realty-client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'full_name',
            'phone',
            'email:email',
            'first_date',
            'last_date',
            'next_date',
            'payment',
            [
                'attribute' => 'mortgage',
                'value' => $model->mortgage ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'source',
                'value' => \app\modules\realty\models\RealtyClient::getSourceList($model->source)

            ],
            [
                'attribute' => 'status',
                'value' => \app\modules\realty\models\RealtyClient::getStatusList($model->status)
            ],
            [
                'attribute' => 'stage',
                'value' => \app\modules\realty\models\RealtyClient::getStageList($model->stage)
            ],
        ],
    ]) ?>

</div>
