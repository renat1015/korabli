<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\RealtySectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Секции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realty-section-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute'      => 'id',
                'contentOptions' => ['style' => 'width: 50px; text-align: center'],
            ],
            'title',
            [
                'attribute' => 'house_id',
                'value'     => 'house.title',
                'filter'    => \yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtyHouse::find()->select('id, title')->all(), 'id', 'title'),
            ],
            [
                'attribute' => 'image',
                'format'    => 'html',
                'value'     => function ($data) {
                    return Html::img($data->getImage(),
                        ['width' => '100px']);
                },
                'filter'    => false,
            ],
            'ordering',
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;{delete}',
            ],
        ],
    ]); ?>

</div>
