<?php

use kartik\switchinput\SwitchInput;
use kartik\touchspin\TouchSpin;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtySection */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('/js/jquery-canvas-area-draw/jquery.canvasAreaDraw.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="realty-section-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <?= $form->field($model, 'house_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtyHouse::getList(), 'id', 'title'), [
        'prompt' => 'Выберите дом',
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::className(), [
        'options'       => [
            'multiple' => false,
            'accept'   => 'image/*',
        ],
        'pluginOptions' => [
            'previewFileType'       => 'image',
            'showCaption'           => false,
            'showRemove'            => false,
            'showUpload'            => false,
            'browseClass'           => 'btn btn-primary btn-block',
            'browseIcon'            => '',
            'allowedFileExtensions' => ['jpg', 'png'],
            'maxFileSize'           => 5120,
            'minImageWidth'         => 640,
            'minImageHeight'        => 480,
            'maxFileCount'          => 1,
            'previewTemplates'      => [
                'image' => ' <img src="{data}" style="width:auto; height: 165px;" class="file-preview-image pull-left" title="{caption}" alt="{caption}"/>',
            ],
            'initialPreview'        => ($model->isNewRecord) ? '' : Html::img($model->getImage(), ['style' => 'width:auto; height:165px;']),
        ],
    ]) ?>

    <?= $form->field($model, 'cords', ['template' => '{label}&nbsp;' . Html::a('Задать координаты', '#', ['class' => 'add-coords']) . '{input}'])
        ->textarea(['rows' => 2, 'class' => 'canvas-area form-control', 'data-image-url' => '/uploads/plan/37b6efe4b87ee4ece84a26b44e986216.jpg']) ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
        [
            'pluginOptions' =>
                [
                    'size'    => 'small',
                    'onText'  => 'Да',
                    'offText' => 'Нет',
                    'state'   => $model->isNewRecord ? true : $model->status,
                ],
        ]
    ); ?>

    <?php if(!$model->isNewRecord) {?>
    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($model, 'ordering')->widget(TouchSpin::className(), [
                'pluginOptions' => ['verticalbuttons' => true]
            ])?>
        </div>
    </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div id="canvas-block" style="position: fixed; top: 0; left: 0; display: none; z-index: 5000; background: black">
    <div id="canvas-container"
         style="position: fixed; top: 0; left: 0; overflow: scroll; width: 100%; height: 100%; background: rgba(0,0,0,0.7); text-align: center"></div>
    <div class="canvas-panel" style="position: fixed; top: 0; left: 0; z-index: 6000">
        <button type="button" class="btn btn-success" onclick="hideCanvas();">Сохранить</button>
    </div>
</div>

<?php
$this->registerJs('
        $(".add-coords").click(function(){
            $("body").css("overflow", "hidden");
            $(".btn-res").prependTo(".canvas-panel");
            $("#canvas-block").show();

            $("canvas").trigger("mousedown").trigger("mouseup");
            var cordsInput = $("#realtysection-cords");
            var cordsText = $(cordsInput).val();
            cordsText = cordsText.replace(",NaN,NaN","");
            $(cordsInput).val(cordsText);
        });

        $("#realtysection-house_id").change(function(){
            setImage();
        });


    ');
if (!$model->isNewRecord) {
    $this->registerJs('
        $(document).ready(function(){
            $("#realtysection-house_id").trigger("change");
        });
    ');
}

?>
<script>
    function hideCanvas() {
        $("body").css("overflow-x", "hidden").css("overflow-y", "auto");
        var cordsInput = $("#realtysection-cords");
        var cordsText = $(cordsInput).val();
        cordsText = cordsText.replace(",NaN,NaN", "");
        $(cordsInput).val(cordsText);
        $("#canvas-block").fadeOut();
    }

    function setImage() {
        var id = $("#realtysection-house_id").val();

        $.post('/realty/realty/get-house-image', {id: id, type: 1}, function (result) {
            if (result == null || typeof(result) == 'undefined') {
            }
            else {

                $('#realtysection-cords').attr('data-image-url', result.filename);
                var canvasBlock = $('#canvas-block');
                $('canvas', canvasBlock).css({background: 'url(' + result.filename + ')'}).attr('width', result.width).attr('height', result.height);

            }
        }, "json");
    }

</script>
