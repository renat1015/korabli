<?php

use app\modules\realty\models\RealtyHouse;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyHouse */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('/js/jquery-canvas-area-draw/jquery.canvasAreaDraw.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="realty-house-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <?= $form->field($model, 'plan_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtyPlan::find()->all(), 'id', 'title')) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cords', ['template' => '{label}&nbsp;'.Html::a('Задать координаты', '#', ['class' => 'add-coords']).'{input}'])
        ->textarea(['rows' => 2, 'class' => 'canvas-area form-control', 'data-image-url' => '/uploads/plan/37b6efe4b87ee4ece84a26b44e986216.jpg']) ?>

    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::className(),[
        'options'=>[
            'multiple' => false,
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '',
            'allowedFileExtensions' => ['jpg','png'],
            'maxFileSize' => 5120,
            'minImageWidth' => 640,
            'minImageHeight' => 480,
            'maxFileCount' => 1,
            'previewTemplates'=>[
                'image' => ' <img src="{data}" style="width:auto; height: 165px;" class="file-preview-image pull-left" title="{caption}" alt="{caption}"/>',
            ],
            'initialPreview' => ($model->isNewRecord) ? '' : Html::img($model->getImage(),['style'=>'width:auto; height:165px;'])
        ]
    ]) ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'video')->fileInput() ?>
        </div>
        <div class="col-lg-6">
            <?php if (!$model->isNewRecord) { ?>
                <label class="control-label">Текущий файл</label>
                <?= Html::a($model->getVideoFile(), $model->getVideoFile()) ?>
            <?php } ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'youtube_video') ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'delete_video')->checkbox() ?>
        </div>
    </div>

    <?= $form->field($model, 'floors')->textInput() ?>

    <?= $form->field($model, 'flats')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(RealtyHouse::getStatusList()); ?>

    <?= $form->field($model, 'house_number')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div id="canvas-block" style="position: fixed; top: 0; left: 0; display: none; z-index: 5000; background: black">
    <div id="canvas-container" style="position: fixed; top: 0; left: 0; overflow: scroll; width: 100%; height: 100%; background: rgba(0,0,0,0.7); text-align: center"></div>
    <div class="canvas-panel" style="position: fixed; top: 0; left: 0; z-index: 6000">
        <button type="button" class="btn btn-success" onclick="hideCanvas();">Сохранить</button>
    </div>
</div>

<?php
    $this->registerJs('
        $(".add-coords").click(function(){
            $("body").css("overflow", "hidden");
            $(".btn-res").prependTo(".canvas-panel");
            $("#canvas-block").show();

            $("canvas").trigger("mousedown").trigger("mouseup");
            var cordsInput = $("#realtyhouse-cords");
            var cordsText = $(cordsInput).val();
            cordsText = cordsText.replace(",NaN,NaN","");
            $(cordsInput).val(cordsText);
        });

        $("#realtyhouse-plan_id").change(function(){
            setImage();
        });

        $(document).ready(function(){
            $("#realtyhouse-plan_id").trigger("change");
        });

    ')
?>

<script>
    function hideCanvas()
    {
        $("body").css("overflow-x", "hidden").css("overflow-y", "auto");
        var cordsInput = $("#realtyhouse-cords");
        var cordsText = $(cordsInput).val();
        cordsText = cordsText.replace(",NaN,NaN","");
        $(cordsInput).val(cordsText);
        $("#canvas-block").fadeOut();
    }
    function setImage()
    {
        var id = $("#realtyhouse-plan_id").val();

        $.post('/realty/realty/get-plan-image', {id:id}, function(result){

            if (result == null || typeof(result) == 'undefined') {
            }
            else {
                $('#realtyhouse-cords').attr('data-image-url', result.filename);

                var canvasBlock = $('#canvas-block');
                $('canvas',canvasBlock).css({background: 'url('+result.filename+')'}).attr('width', result.width).attr('height', result.height);
            }
        }, "json");
    }
</script>
