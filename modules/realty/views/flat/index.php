<?php

use app\modules\realty\models\RealtyFlat;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\RealtyFlatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Квартиры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realty-flat-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id'           => 'flats-table',
        'rowOptions'   => function ($model, $index, $widget, $grid) {
            if ($model->sale_status == RealtyFlat::SALE_STATUS_SALE)
                return ['class' => 'st-sale'];
            if ($model->sale_status == RealtyFlat::SALE_STATUS_IN_SALE)
                return ['class' => 'st-in-sale'];
            if ($model->sale_status == RealtyFlat::SALE_STATUS_RESERVED)
                return ['class' => 'st-reserved'];

        },
        'columns'      => [
            [
                'attribute'      => 'id',
                'contentOptions' => ['style' => 'width: 50px; text-align: center'],
                'filter'         => false,
            ],
            [
                'attribute' => 'number',
                'filter'    => false,
            ],
            [
                'attribute' => 'title',
                'filter'    => false,
            ],
            [
                'attribute' => 'house_id',
                'value'     => 'house.title',
                'filter'    => false,
            ],
            [
                'attribute' => 'section_id',
                'value'     => 'section.title',
                'filter'    => false,
            ],
            [
                'attribute' => 'floor_id',
                'value'     => 'floor.title',
                'filter'    => false,
            ],
            [
                'attribute' => 'price',
            ],
            [
                'attribute' => 'scale',
                'filter'    => false,
            ],
            [
                'attribute' => 'rooms',
                'filter'    => false,
                'value' => function($data){
                    return ($data->rooms == 0) ? 'Студия' : $data->rooms;
                }
            ],
            [
                'attribute' => 'sale_status',
                'value'     => 'saleStatusName',
                'filter'    => false,
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;{delete}',
            ],
        ],
    ]); ?>

</div>
