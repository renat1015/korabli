<?php

use kartik\switchinput\SwitchInput;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyFlat */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('/js/jquery-canvas-area-draw/jquery.canvasAreaDraw.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="realty-flat-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <div class="row">
        <div
            class="col-md-4">    <?= $form->field($model, 'house_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtyHouse::getList(), 'id', 'title'), [
                'prompt' => 'Выберите дом',
            ]) ?></div>
        <div
            class="col-md-4"><?= $form->field($model, 'section_id')->dropDownList($sections, ['disabled' => ($model->isNewRecord ? true : (count($sections) > 0 ? false : true))]) ?>
        </div>
        <div
            class="col-md-4"><?= $form->field($model, 'floor_id')->dropDownList($floors, ['disabled' => ($model->isNewRecord ? true : (count($floors) > 0 ? false : true))]) ?>
        </div>
    </div>

    <?= $form->field($model, 'sale_status')->dropDownList(\app\modules\realty\models\RealtyFlat::getSaleStatusList()) ?>

    <?= $form->field($model, 'title')->dropDownList([
        'Квартира' => 'Квартира',
        'Офис'     => 'Офис',
        'Студия'     => 'Студия',
    ], ['class' => 'flat__title']) ?>

    <?= $form->field($model, 'number')->input('number') ?>

    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::className(), [
        'options'       => [
            'multiple' => false,
            'accept'   => 'image/*',
        ],
        'pluginOptions' => [
            'previewFileType'       => 'image',
            'showCaption'           => false,
            'showRemove'            => false,
            'showUpload'            => false,
            'browseClass'           => 'btn btn-primary btn-block',
            'browseIcon'            => '',
            'allowedFileExtensions' => ['jpg', 'png'],
            'maxFileSize'           => 5120,
            'minImageWidth'         => 640,
            'minImageHeight'        => 480,
            'maxFileCount'          => 1,
            'previewTemplates'      => [
                'image' => ' <img src="{data}" style="width:auto; height: 165px;" class="file-preview-image pull-left" title="{caption}" alt="{caption}"/>',
            ],
            'initialPreview'        => ($model->isNewRecord) ? '' : Html::img($model->getImage(), ['style' => 'width:auto; height:165px;']),
        ],
    ]) ?>

    <?= $form->field($model, 'cords', ['template' => '{label}&nbsp;' . Html::a('Задать координаты', '#', ['class' => 'add-coords']) . '{input}'])
        ->textarea(['rows' => 2, 'class' => 'canvas-area form-control', 'data-image-url' => '/uploads/plan/37b6efe4b87ee4ece84a26b44e986216.jpg']) ?>

    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'price')->textInput() ?></div>
        <div class="col-md-3"><?= $form->field($model, 'scale')->textInput() ?></div>
        <div class="col-md-3 flat__rooms"><?= $form->field($model, 'rooms')->textInput() ?></div>
        <div class="col-md-3"><?= $form->field($model, 'living_scale')->textInput() ?></div>
    </div>

    <div class="row" id="rooms">
        <?php

        if (!$model->isNewRecord) {
            $add = true;
            foreach ($model->rooms_scale as $value) {
                ?>
                <div class="rooms-block">
                    <div class="col-md-4">
                        <?= Html::dropDownList('RealtyFlat[room_type][]', $value['name'], \app\modules\realty\models\RealtyFlat::getRoomTypes(), ['class' => 'form-control']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'room_scale[]', ['template' => '{input}', 'addon' => ['append' => ['content' => 'кв.м.']]])->textInput(['value' => $value['value']]) ?>
                    </div>
                    <div class="col-md-6 link-block">
                        <?php
                        if ($add) {
                            echo Html::a('<i class="fa fa-plus-circle"></i> добавить комнату', '#', [
                                'class'   => 'add-link',
                                'onClick' => 'addRoom(); return false;',
                            ]);
                            $add = false;
                        } else {
                            echo Html::a('<i class="fa fa-trash-o"></i>', '#', ['class' => 'del-room']);
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        <?php } else {
            ?>
            <div class="rooms-block">
                <div class="col-md-4">
                    <?= $form->field($model, 'room_type[]')->dropDownList(\app\modules\realty\models\RealtyFlat::getRoomTypes()) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'room_scale[]', ['addon' => ['append' => ['content' => 'кв.м.']]]) ?>
                </div>
                <div class="col-md-6 link-block">
                    <?= Html::a('<i class="fa fa-plus-circle"></i> добавить комнату', '#', [
                        'class'   => 'add-link',
                        'onClick' => 'addRoom(); return false;',
                    ]) ?>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php } ?>
    </div>

    <?= \app\modules\image\widgets\InputWidget::widget(['model'=>$model,'form'=>$form]) ?>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'tour')->textInput() ?></div>
        <div class="col-md-6">
            <?= $form->field($model, 'side')->dropDownList([
                '0' => 'север',
                '1' => 'северо-восток',
                '2' => 'восток',
                '3' => 'юго-восток',
                '4' => 'юг',
                '5' => 'юго-запад',
                '6' => 'запад',
                '7' => 'северо-запад',
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
        [
            'pluginOptions' =>
                [
                    'size'    => 'small',
                    'onText'  => 'Да',
                    'offText' => 'Нет',
                    'state'   => $model->isNewRecord ? true : $model->status,
                ],
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div id="canvas-block" style="position: fixed; top: 0; left: 0; display: none; z-index: 5000; background: black">
    <div id="canvas-container"
         style="position: fixed; top: 0; left: 0; overflow: scroll; width: 100%; height: 100%; background: rgba(0,0,0,0.7); text-align: center"></div>
    <div class="canvas-panel" style="position: fixed; top: 0; left: 0; z-index: 6000">
        <button type="button" class="btn btn-success" onclick="hideCanvas();">Сохранить</button>
    </div>
</div>

<?php
$this->registerJs('
        $(".add-coords").click(function(){
            $("body").css("overflow", "hidden");
            $(".btn-res").prependTo(".canvas-panel");
            $("#canvas-block").show();

            $("canvas").trigger("mousedown").trigger("mouseup");
            var cordsInput = $("#realtyflat-cords");
            var cordsText = $(cordsInput).val();
            cordsText = cordsText.replace(",NaN,NaN","");
            $(cordsInput).val(cordsText);
        });

        $("#realtyflat-house_id").change(function(){
            $(\'#realtyflat-section_id\').attr(\'disabled\', \'disabled\').html("");
            $(\'#realtyflat-floor_id\').attr(\'disabled\', \'disabled\').html("");
            getList("section");
        });

        $("#realtyflat-section_id").change(function(){
            $(\'#realtyflat-floor_id\').attr(\'disabled\', \'disabled\').html("");
            getList("floor");
        });

        $("#realtyflat-floor_id").change(function(){
            setImage();
        });
        $(document).on( "click", "a.del-room", function() {
            delRoom($(this));
            return false;
        });

        $(document).on("change", ".field-realtyflat-room_scale input[type=\\"text\\"]", function() {
            var summ = 0;
            $(".field-realtyflat-room_scale input[type=\"text\"]", $(document)).each(function(){
                var vl = $(this).val();
                var sc = parseFloat(vl.replace(",", "."));
                summ +=sc;
            });
            $("#realtyflat-scale").val(summ);
        });
        
        $(".flat__title").on("change", function () {
            var title = $(this).val();
            if(title == "Студия"){
                $(".flat__rooms input").attr("value", 0);
                $(".flat__rooms").hide();
            } else {
                $(".flat__rooms input").attr("value", "");
                $(".flat__rooms").show();
            }
        });
    ');

if (!$model->isNewRecord) {
    $this->registerJs('
    $("#realtyflat-floor_id").trigger("change");
    ');
}
?>

<script>
    function hideCanvas() {
        $("body").css("overflow-x", "hidden").css("overflow-y", "auto");
        var cordsInput = $("#realtyflat-cords");
        var cordsText = $(cordsInput).val();
        cordsText = cordsText.replace(",NaN,NaN", "");
        $(cordsInput).val(cordsText);
        $("#canvas-block").fadeOut();
    }

    function setImage() {
        var id = $("#realtyflat-floor_id").val();

        $.post('/realty/realty/get-floor-image', {id: id}, function (result) {
            if (result == null || typeof(result) == 'undefined') {
            }
            else {
                $('#realtyflat-cords').attr('data-image-url', result.filename);

                var canvasBlock = $('#canvas-block');
                $('canvas', canvasBlock).css({background: 'url(' + result.filename + ')'}).attr('width', result.width).attr('height', result.height);
            }
        }, "json");
    }

    function getList(type) {
        var id, html;

        if (type == 'section')
            id = $('#realtyflat-house_id').val();
        else if (type = 'floor')
            id = $('#realtyflat-section_id').val();

        $.post('/realty/realty/get-data-list', {id: id, type: type}, function (result) {
            if (type == 'section') {
                if (result.sections) {
                    html = '<option value="">Выберите секцию</option>';
                    $.each(result.sections, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyflat-section_id').attr('disabled', false).html(html);
                } else if (result.floors) {
                    html = '<option value="">Выберите Этаж</option>';
                    $.each(result.floors, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyflat-section_id').attr('disabled', 'disabled').html('<option value="">У дома нет секций, выберите этаж...</option>');
                    $('#realtyflat-floor_id').attr('disabled', false).html(html);
                }
            } else if (type == 'floor') {
                if (result.floors) {
                    html = '<option value="">Выберите Этаж</option>';
                    $.each(result.floors, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyflat-floor_id').attr('disabled', false).html(html);
                } else {
                    $('#realtyflat-floor_id').attr('disabled', 'disables').html('<option value="">Нет этажей для выбора, смените секцию или дом</option>');
                }

            }
        }, "json");

    }

    function addRoom() {
        var html = $('.rooms-block:first').clone();
        $('label', html).remove();
        $('input', html).val('');
        $('.link-block', html).html('<?= Html::a('<i class="fa fa-trash-o"></i>', '#', ['class' => 'del-room'])?> ');
        $('#rooms').append($(html).hide().fadeIn());
    }

    function delRoom(dt) {
        $(dt).parents('.rooms-block').fadeOut('fast', function () {
            $(this).remove();
        });
    }



</script>
