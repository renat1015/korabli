<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyFlat */

$this->title = 'Редактировать: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Квартиры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="realty-flat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'    => $model,
        'sections' => $sections,
        'floors'   => $floors,
    ]) ?>

</div>
