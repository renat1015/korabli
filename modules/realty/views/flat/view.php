<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyFlat */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Realty Flats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realty-flat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'number',
            'title',
            'description:ntext',
            'image',
            'cords:ntext',
            'house_id',
            'section_id',
            'floor_id',
            'price',
            'scale',
//            'rooms',
            [
                'attribute' => 'rooms',
                'value' => ($model->rooms == 0) ? 'Студия' : $model->rooms
            ],
            'scale_lobby',
            'scale_living',
            'scale_kitchen',
            'scale_bathroom',
            'updated_at',
            'created_at',
            'status',
            'rooms_scale:ntext',
        ],
    ]) ?>

</div>
