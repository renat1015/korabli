<?php

use app\modules\realty\models\RealtyFlat;
use app\modules\realty\models\RealtyFloor;
use app\modules\realty\models\RealtySection;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyFlatSearch */
/* @var $form yii\widgets\ActiveForm */
$sections = $floors = [];
if ($model->house_id) {
    $sections = \yii\helpers\ArrayHelper::map(RealtySection::find()->where(['house_id' => $model->house_id])->all(), 'id', 'title');

    if (!$sections)
        $floors = \yii\helpers\ArrayHelper::map(RealtyFloor::find()->where(['house_id' => $model->house_id])->all(), 'id', 'title');
}

if ($model->section_id)
    $floors = \yii\helpers\ArrayHelper::map(RealtyFloor::find()->where(['section_id' => $model->section_id])->all(), 'id', 'title');

?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Фильтр</h3>

        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" style="display: block;">
        <div class="realty-flat-search">

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'id') ?>
                </div>

                <div class="col-sm-3">
                    <?= $form->field($model, 'title')->dropDownList([
                        'Квартира' => 'Квартира',
                        'Офис'     => 'Офис',
                    ], ['prompt' => 'Все']) ?>
                </div>

                <div class="col-sm-3">
                    <?php echo $form->field($model, 'sale_status')->dropDownList(RealtyFlat::getSaleStatusList(), ['prompt' => 'Все']); ?>
                </div>

                <div class="col-sm-3">
                    <?php echo $form->field($model, 'rooms') ?>
                </div>

                <div class="col-sm-3">
                    <?php echo $form->field($model, 'price_to') ?>
                </div>

                <div class="col-sm-3">
                    <?php echo $form->field($model, 'price_from') ?>
                </div>

                <div class="col-sm-3">
                    <?php echo $form->field($model, 'scale_to') ?>
                </div>

                <div class="col-sm-3">
                    <?php echo $form->field($model, 'scale_from') ?>
                </div>

                <div class="col-sm-3">
                    <?php echo $form->field($model, 'house_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtyHouse::find()->select('id, title')->all(), 'id', 'title'), ['prompt' => 'Все']) ?>
                </div>
                <div class="col-sm-3">
                    <?php echo $form->field($model, 'section_id')->dropDownList($sections, ['disabled' => true, 'prompt' => 'Все']); ?>
                </div>
                <div class="col-sm-3">
                    <?php echo $form->field($model, 'floor_id')->dropDownList($floors, ['disabled' => true, 'prompt' => 'Все']); ?>
                </div>

            </div>

            <div class="form-group">
                <?= Html::submitButton('Применить', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Сбросить', ['class' => 'btn btn-default', 'onClick' => 'window.location.href="/realty/flat/index"']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<script>
    function getList(type) {
        var id, html;

        if (type == 'section')
            id = $('#realtyflatsearch-house_id').val();
        else if (type = 'floor')
            id = $('#realtyflatsearch-section_id').val();

        $.post('/realty/realty/get-data-list', {id: id, type: type}, function (result) {
            if (type == 'section') {
                if (result.sections) {
                    html = '<option value="">Все</option>';
                    $.each(result.sections, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyflatsearch-section_id').attr('disabled', false).html(html);
                } else if (result.floors) {
                    html = '<option value="">Все</option>';
                    $.each(result.floors, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyflatsearch-section_id').attr('disabled', 'disabled');
                    $('#realtyflatsearch-floor_id').attr('disabled', false).html(html);
                }
            } else if (type == 'floor') {
                if (result.floors) {
                    html = '<option value="">Все</option>';
                    $.each(result.floors, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyflatsearch-floor_id').attr('disabled', false).html(html);
                } else {
                    $('#realtyflatsearch-floor_id').attr('disabled', 'disables');
                }

            }
        }, "json");

    }
</script>

<?php
$this->registerJs('
    $("#realtyflatsearch-house_id").change(function(){
        $(\'#realtyflatsearch-section_id\').attr(\'disabled\', \'disabled\').html("");
        $(\'#realtyflatsearch-floor_id\').attr(\'disabled\', \'disabled\').html("");
        getList("section");
    });

    $("#realtyflatsearch-section_id").change(function(){
        $(\'#realtyflatsearch-floor_id\').attr(\'disabled\', \'disabled\').html("");
        getList("floor");
    });
');


if ($model->house_id) {
    $this->registerJs('
        $("#realtyflatsearch-section_id").attr(\'disabled\', false);
    ');
}

if ($model->section_id || $model->floor_id) {
    $this->registerJs('
        $("#realtyflatsearch-floor_id").attr(\'disabled\', false);
    ');
}

?>

