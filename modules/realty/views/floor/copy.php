<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyFloor */

$this->title = 'Копировать: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Этажи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Копировать';
?>
<div class="realty-floor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'sections' => $sections,
        'model'    => $model,
        'flats'    => $flats,
    ]) ?>

</div>
