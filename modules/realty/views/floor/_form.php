<?php

use kartik\switchinput\SwitchInput;
use kartik\touchspin\TouchSpin;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyFloor */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('/js/jquery-canvas-area-draw/jquery.canvasAreaDraw.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="realty-floor-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <?= $form->field($model, 'house_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtyHouse::getList(), 'id', 'title'), [
        'prompt' => 'Выберите дом',
    ]) ?>

    <?= $form->field($model, 'section_id')->dropDownList($sections, [
        'disabled' => true,
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'flat_count') ?>

    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::className(), [
        'options'       => [
            'multiple' => false,
            'accept'   => 'image/*',
        ],
        'pluginOptions' => [
            'previewFileType'       => 'image',
            'showCaption'           => false,
            'showRemove'            => false,
            'showUpload'            => false,
            'browseClass'           => 'btn btn-primary btn-block',
            'browseIcon'            => '',
            'allowedFileExtensions' => ['jpg', 'png'],
            'maxFileSize'           => 5120,
            'minImageWidth'         => 640,
            'minImageHeight'        => 480,
            'maxFileCount'          => 1,
            'previewTemplates'      => [
                'image' => ' <img src="{data}" style="width:auto; height: 165px;" class="file-preview-image pull-left" title="{caption}" alt="{caption}"/>',
            ],
            'initialPreview'        => ($model->isNewRecord && !$model->is_copy) ? '' : Html::img($model->getImage(), ['style' => 'width:auto; height:165px;']),
        ],
    ]) ?>

    <?= $form->field($model, 'cords', ['template' => '{label}&nbsp;' . Html::a('Задать координаты', '#', ['class' => 'add-coords']) . '{input}'])
        ->textarea(['rows' => 2, 'class' => 'canvas-area form-control', 'data-image-url' => '']) ?>

    <?php
    if ($model->is_copy) {
        $model->copy_flats = \yii\helpers\ArrayHelper::map($flats, 'id', 'id');
        echo $form->field($model, 'copy_flats')->checkboxList(\yii\helpers\ArrayHelper::map($flats, 'id', 'title'));
        echo Html::style('
            #realtyfloor-copy_flats label {
                display: block;
            }
        ');
    }
    ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
        [
            'pluginOptions' =>
                [
                    'size'    => 'small',
                    'onText'  => 'Да',
                    'offText' => 'Нет',
                    'state'   => $model->isNewRecord ? true : $model->status,
                ],
        ]
    ); ?>

    <?php if(!$model->isNewRecord) {?>
        <div class="row">
            <div class="col-lg-2">
                <?= $form->field($model, 'ordering')->widget(TouchSpin::className(), [
                    'pluginOptions' => ['verticalbuttons' => true]
                ])?>
            </div>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div id="canvas-block" style="position: fixed; top: 0; left: 0; display: none; z-index: 5000; background: black">
    <div id="canvas-container"
         style="position: fixed; top: 0; left: 0; overflow: scroll; width: 100%; height: 100%; background: rgba(0,0,0,0.7); text-align: center"></div>
    <div class="canvas-panel" style="position: fixed; top: 0; left: 0; z-index: 6000">
        <button type="button" class="btn btn-success" onclick="hideCanvas();">Сохранить</button>
    </div>
</div>

<?php
$this->registerJs('
        $(".add-coords").click(function(){
            $("body").css("overflow", "hidden");
            $(".btn-res").prependTo(".canvas-panel");
            $("#canvas-block").show();

            $("canvas").trigger("mousedown").trigger("mouseup");
            var cordsInput = $("#realtyhouse-cords");
            var cordsText = $(cordsInput).val();
            cordsText = cordsText.replace(",NaN,NaN","");
            $(cordsInput).val(cordsText);
        });

        $("#realtyfloor-house_id").change(function(){
            setImage();
        });

        $("#realtyfloor-section_id").change(function(){
            setSectionImage();
        });

    ');

if ((!$model->isNewRecord || $model->is_copy) && count($sections) == 0) {
    $this->registerJs('
            $(document).ready(function(){
                $("#realtyfloor-house_id").trigger("change");
            });
        ');
}
if (count($sections) > 0) {
    $this->registerJs('
            $(document).ready(function(){
                setTimeout(function(){
                    $("#realtyfloor-section_id").attr("disabled", false);
                }, 500);
            });
        ');
}

?>
<script>
    function hideCanvas() {
        $("body").css("overflow-x", "hidden").css("overflow-y", "auto");
        var cordsInput = $("#realtyfloor-cords");
        var cordsText = $(cordsInput).val();
        cordsText = cordsText.replace(",NaN,NaN", "");
        $(cordsInput).val(cordsText);
        $("#canvas-block").fadeOut();
    }

    function setImage() {
        var id = $("#realtyfloor-house_id").val();

        $.post('/realty/realty/get-house-image', {id: id}, function (result) {
            if (result == null || typeof(result) == 'undefined') {
            }
            else {
                if (result.sections) {
                    var html = '<option value="">Выберите секцию</option>';
                    $.each(result.sections, function (i, item) {
                        html += '<option value="' + item.id + '">' + item.title + '</option>'
                    });
                    $('#realtyfloor-section_id').attr('disabled', false).html(html);
                } else {
                    $('#realtyfloor-section_id').attr('disabled', 'disabled');
                    $('#realtyhouse-cords').attr('data-image-url', result.filename);
                    var canvasBlock = $('#canvas-block');
                    $('canvas', canvasBlock).css({background: 'url(' + result.filename + ')'}).attr('width', result.width).attr('height', result.height);
                }
            }
        }, "json");
    }

    function setSectionImage() {
        var id = $("#realtyfloor-section_id").val();

        $.post('/realty/realty/get-section-image', {id: id}, function (result) {
            if (result == null || typeof(result) == 'undefined') {
            }
            else {
                $('#realtyfloor-cords').attr('data-image-url', result.filename);
                var canvasBlock = $('#canvas-block');
                $('canvas', canvasBlock).css({background: 'url(' + result.filename + ')'}).attr('width', result.width).attr('height', result.height);
            }
        }, "json");

    }
</script>