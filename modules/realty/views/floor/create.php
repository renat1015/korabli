<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\realty\models\RealtyFloor */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Этажи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realty-floor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'sections' => $sections,
        'model'    => $model,
    ]) ?>

</div>
