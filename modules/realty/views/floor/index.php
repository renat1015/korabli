<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\RealtyFloorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Этажи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realty-floor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute'      => 'id',
                'contentOptions' => ['style' => 'width: 50px; text-align: center'],
            ],
            'title',
            [
                'attribute' => 'house_id',
                'value'     => 'house.title',
                'filter'    => \yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtyHouse::find()->select('id, title')->all(), 'id', 'title'),
            ],
            [
                'attribute' => 'section_id',
                'value'     => 'section.title',
                'filter'    => \yii\helpers\ArrayHelper::map(\app\modules\realty\models\RealtySection::find()->select('id, title')->all(), 'id', 'title'),
            ],
            [
                'attribute' => 'image',
                'format'    => 'html',
                'value'     => function ($data) {
                    return Html::img($data->getImage(),
                        ['width' => '100px']);
                },
                'filter'    => false,
            ],
            [
                'attribute' => 'ordering',
                'format'    => 'raw',
                'value' => function($data) {
                        return Html::textInput('ordering', $data->ordering, ['data-id' => $data->id, 'class' => 'ordering-floor','style' => 'width: 50px; text-align: center']);
                    },
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;{delete}&nbsp;{copy}',
                'buttons'  => [
                    'copy' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-clone"></i>', $url, ['title' => 'Копировать']);
                    },
                ],
            ],
        ],
    ]); ?>

</div>

<?php

    $this->registerJs("
        $('.ordering-floor').change(function(){
        sendData($(this).attr('data-id'), $(this).val());

    });
    ");
?>

<script>
    function sendData(id, val)
    {
        $.post('/realty/floor/ordering', {id: id, val: val}, function(result){

        });
    }
</script>

