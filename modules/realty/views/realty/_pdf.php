<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 07.12.15
 * Time: 14:44
 * @var $model app\modules\realty\models\RealtyFlat
 */
use yii\helpers\Html;

?>


<table style="width: 1024px; margin: auto; border-collapse: collapse">
    <tr>
        <td width="50%" style="">
            <table  style="width: 100%;border-collapse: collapse;">
                <tr style=" color: #193e73; height: 168px;">
                    <td  colspan="2">
                        <span style="display: inline-block;    height: 100%;
    overflow: hidden;"><img style="height: 217px;" src="/img/b_logo.png" alt="logo"></span>
                    </td>
                    <td colspan="8" style="text-align: right; padding-right: 30px; padding-bottom: 20px; vertical-align: bottom;">
                        <p><span style="display: block;color: #f9b723; text-decoration: none;
                        text-transform: uppercase; font-size: 14px;">www.korablinn.ru</span></p>

                        <p>
                            <span style="display: block;color: #193e73; font-weight: bold; font-size: 30px; line-height: 60px;">(831) 260-16-05</span>
                        </p>

                        <p style="color: #193e73;  font-size: 14px; line-height: 24px;">Н.Новгород, ул.Белозерская д3</p>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px;"></td>
                </tr>
                <tr>
                    <td colspan="1" style="vertical-align: top;">
                        <table>
                            <tr>
                                <td style="background: #193e73; margin-top: 0; text-align: center; line-height: 24px;  width: 100px;height: 100px;color: #fff; font-size: 18px;text-transform: uppercase;"><p style="font-size: 56px;    line-height: 62px; display: block;font-weight: bold; " ><?= $model->house->house_number ?></p>дом</td>
                            </tr>
                            <tr>
                                <td style="height: 25px;"></td>
                            </tr>
                            <?php if($model->section_id):?>
                                <tr>
                                    <td style="background: #193e73; margin-top: 0; text-align: center; line-height: 24px;  width: 100px;height: 100px;color: #fff; font-size: 18px;text-transform: uppercase;"><p style="font-size: 56px;    line-height: 80px; display: block;font-weight: bold; " ><?= $model->section->title ?></p>секция</td>
                                </tr>
                                <tr>
                                    <td style="height: 25px;"></td>
                                </tr>
                            <?php endif;?>
                            <tr>
                                <td style="background: #193e73; margin-top: 0; text-align: center; line-height: 24px;  width: 100px;height: 100px;color: #fff; font-size: 18px;text-transform: uppercase; "><p style="font-size: 56px;    line-height: 62px; display: block;font-weight: bold; margin-top: 80px;" ><?= $model->floor->title ?></p>этаж</td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="8" style="color:#193e73; vertical-align: top; padding-right: 30px;">
                        <h3 style=" font-size: 50px; margin: 0; margin-bottom: 18px;">№<?= $model->number?></h3>
                        <p style="font-size: 22px; margin-bottom: 30px; line-height: 30px; display: flex; justify-content: space-between; " class="rooms"><?= Yii::t('app', '{n, plural, =0{ без комнат} =1{# - комнатная} many{#-ти комнатная} other{#-х комнатная}}', ['n' => $model->rooms]) ?></p>
                        <p style="font-size: 22px; line-height: 30px; display: flex; justify-content: space-between; " class="charact"><span class="name-char">Общая пл.: </span><span class="value">28.3 м2</span></p>
                        <p style="font-size: 22px; line-height: 30px; display: flex; justify-content: space-between; " class="charact"><span class="name-char">Жилая пл.:</span> <span class="value">18.7 м2</span></p>
                        <?php
                        if(($rooms = $model->rooms_array) != null):
                            foreach ($rooms as $value) :?>
                                <p style="font-size: 22px; line-height: 30px; display: flex; justify-content: space-between; " class="charact"><span class="name-char"><?= $value['name'];?>:</span><span class="value"><?= $value['value'];?> м2</span></p>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px;"></td>
                </tr>
                <tr>
                    <td colspan="10" style="padding-right: 30px;">
                        <p style="color: #193e73; margin: 0; line-height: 100px; font-size: 72px; font-weight: bold;"><?= number_format($model->price, 0, '', ' ') ?> <span style=" font-size: 36px; text-transform: uppercase; font-weight: normal;">руб</span></p>
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%" style="height: 680px;text-align: center;">
            <img style="width: 400px; margin: auto" src="<?= $model->image_zh;?>" alt="plan">
        </td>
    </tr>
</table>