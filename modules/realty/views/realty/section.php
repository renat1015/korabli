<?php
use app\modules\realty\models\RealtySection;
use yii\helpers\Html;
use yii\web\View;

/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 16.11.15
 * Time: 10:18
 * @var $model RealtySection
 * @var $this View
 */
//$this->context->sidebarAdvancedClass = 'pre-closed';
$this->context->showLoading = true;
$mainImg = $model->getImage();
$imgInfo = getimagesize(Yii::$app->basePath.'/../'.$mainImg);
?>
<div class="kor-choice-home">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="title">
        <h3>Выбор этажа</h3>
        <a href="#">
            <img src="/img/inform.png" alt="img">
            <span>Выберите этаж</span>
        </a>
    </div>
    <div class="param">
        <div class="param-inner">
            <a href="#">
                <img src="/img/param.png" alt="img">
                <span>Подбор по параметрам</span>
            </a>
        </div>
    </div>
    <?= Yii::$app->controller->renderPartial('_params');?>

    <div class="selection">
        <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-house', 'id' => $model->house_id])?>" class="back"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
        <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-house', 'id' => $model->house_id])?>" class="home-num">
            <span class="value"><?= $model->house->house_number;?></span>дом</a>
<!--        <div class="stay-select">-->
<!--            <div class="div"><a href="#" class="stay-num"><span class="value">1</span>секция</a></div>-->
<!--            <div class="div"><a href="#" class="stay-num"><span class="value">2</span>секция</a></div>-->
<!--            <div class="div"><a href="#" class="stay-num"><span class="value">3</span>секция</a></div>-->
<!--            <div class="div"><a href="#" class="stay-num"><span class="value">4</span>секция</a></div>-->
<!--            <div class="div"><a href="#" class="stay-num"><span class="value">5</span>секция</a></div>-->
<!--            <div class="div"><a href="#" class="stay-num"><span class="value">6</span>секция</a></div>-->
<!--            <div class="div"><a href="#" class="stay-num"><span class="value">7</span>секция</a></div>-->
<!--        </div>-->
        <div class="stay-select">
            <?php if($prev):?>
                <a href="<?= $prev['url']; ?>" class="slick-prev slick-arrow">
                </a>
            <?php endif;?>
            <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-section', 'id' => $model->id])?>" class="section-num">
                <span class="value"><?= $model->title; ?></span>секция
            </a>
            <?php if($next):?>
                <a href="<?= $next['url']; ?>" class="slick-next slick-arrow">
                </a>
            <?php endif;?>
        </div>
        <?php if($prev):?>
            <div class="tooltip prev"><span class="tooltip-title">Переход на секцию</span> <span class="tooltip-value"><?= $prev['title']; ?></span></div>
        <?php endif;?>
        <?php if($next):?>
            <div class="tooltip next"><span class="tooltip-title">Переход на секцию</span> <span class="tooltip-value"><?= $next['title']; ?></span></div>
        <?php endif;?>
    </div>


    <div class="image main__image">
        <?php echo \yii\helpers\Html::img($mainImg,['alt'=>$model->title]) ?>
        <?php if(($floors = $model->realtyFloors) != null): ?>
            <div class="floors">
                <svg id="svg-block">
                    <?php foreach ($floors as $floor) {
                        echo \yii\bootstrap\Html::tag('polygon','',[
                            'points' => $floor->cords,
                            'title' => $floor->title,
                            'data' => [
                                'key' => $floor->id,
                                'url' => $floor->getSeoUrl(),
                                'real-points' => $floor->cords
                            ],
                        ]);
                    } ?>
                </svg>
                <div class="tooltips">
                    <?php foreach ($floors as $floor):  ?>
                        <div id="lw-tooltip<?= $floor->id ?>" class="lw-tooltip">
                            <div class="home-number floor active">
                                <form>
                                    <div class="info-house">
                                        <label>В продаже</label>
                                        <input type="text" placeholder="<?= count($floor->saleFlats); ?>">
                                    </div>
                                    <div class="number">
                                        <?= $floor->title ?>
                                        <span>этаж</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>


<?php
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/classic_test.css');
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/jquery-ui.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jquery-ui.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jQRangeSlider-min.js', ['depends' => 'yii\web\JqueryAsset']);


$this->registerJs(<<<JS
    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        imgContainer = $('.kor-choice-home .main__image'),
        mainImg = $('img',imgContainer),
        svgBlock = $('#svg-block'),
        resizeTimer;

    $(window).on('resize', function(e) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        setImgContainerPos();
        resizePolygon('$imgInfo[0]','$imgInfo[1]');
      }, 150);
    });

    readyToShow = false;

    $(document).ready(function(){
        setTimeout(function(){
            var interval = setInterval(function(){
                if(window.loaded)
                {
                    clearInterval(interval);
                    readyToShow = true;
                    
                    loadingBlockHide();
                }
            },100);
            setImgContainerPos();
            resizePolygon('$imgInfo[0]','$imgInfo[1]');
        },50);
    });

    setTimeout(function(){
         $('.sidebar .toggle').trigger('click');
    },1000);
JS
)
?>

