<?php
use app\modules\realty\models\RealtyFloor;
use yii\helpers\Html;
use yii\web\View;

/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 16.11.15
 * Time: 10:18
 * @var $model RealtyFloor
 * @var $this View
 */
$mainImg = $model->getImage();
$imgInfo = $model->getImgSize();
$this->context->sidebarAdvancedClass = 'full-color';
//$this->context->sidebarAdvancedClass = 'full-color pre-closed';
$this->context->showLoading = true;
$this->context->headerAdvanceClass = 'choise-plan';
?>
<div class="kor-choice-home choise-plan">
    <div class="logo">
        <a class="white" href="/"><img src="/img/w_logo.png" alt="img"></a>
    </div>
    <div class="title">
        <h3>Выбор планировки</h3>
        <a href="#">
            <img src="/img/inform.png" alt="img">
            <span>Выберите планировку квартиры</span>
        </a>
    </div>
    <div class="param">
        <div class="param-inner">
            <a href="#">
                <img src="/img/param.png" alt="img">
                <span>Подбор по параметрам</span>
            </a>
        </div>
    </div>
    <?= Yii::$app->controller->renderPartial('_params');?>
    <div class="selection">
        <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-house', 'id' => $model->house_id])?>" class="back"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
        <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-house', 'id' => $model->house_id])?>" class="home-num">
            <span class="value"><?= $model->house->house_number;?></span>дом</a>
        <?php if($model->section_id):?>
            <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-section', 'id' => $model->section_id])?>" class="section-num">
                <span class="value"><?= $model->section->title; ?></span>секция
            </a>
        <?php endif;?>
        <div class="stay-select">
            <?php if($prev):?>
                <a href="<?= $prev['url']; ?>" class="slick-prev slick-arrow">
                </a>
            <?php endif;?>
            <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-section', 'id' => $model->id])?>" class="section-num">
                <span class="value"><?= $model->title; ?></span>этаж
            </a>
            <?php if($next):?>
                <a href="<?= $next['url']; ?>" class="slick-next slick-arrow">
                </a>
            <?php endif;?>
        </div>

<!--        <div class="tooltip"><span class="tooltip-title">Переход на этаж</span> <span class="tooltip-value">2</span></div>-->
    </div>
    <div class="plan-floor-block">
        <div class="plan-floor-image main__image">
            <?= Html::img($model->getImage(),['alt'=>$model->title]) ?>
            <?php if(($flats = $model->realtyFlats) != null): ?>
                <svg id="svg-block"  style="width: 100%; height: 100%;">
                    <?php foreach ($flats as $flat) {
                        $class = 'flat';
                        if($flat->sale_status == 2){
                            $class .= ' sale';
                        }
                        if($flat->sale_status == 3){
                            $class .= ' sale book';
                        }

                        echo Html::tag('polygon','',[
                            'points' => $flat->cords,
                            'title' => $flat->title,
                            'data' => [
                                'key' => $flat->id,
                                'url' => ($flat->sale_status != 2 && $flat->sale_status != 3) ? $flat->getSeoUrl() : '',
                                'real-points' => $flat->cords
                            ],
                            'class' => $class
                        ]);
                    } ?>
                </svg>
                <div class="tooltips">
                    <?php foreach ($flats as $flat):  ?>
                        <div id="lw-tooltip<?= $flat->id ?>" class="lw-tooltip flat <?= ($flat->sale_status == 2) ? 'sales' : '';?>">
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

<!--            <img src="/img/choise-flat.png" alt="img">-->
<!--            <div class="flat sales"></div>-->
<!--            <div class="flat"></div>-->
        </div>
        <div class="plan-floor-info-container">
            <?php if(($flats = $model->realtyFlats) != null): ?>
                <?php
                $i=0;

                foreach ($flats as $flat):?>
                    <ul class="plan-floor-info " id="floor<?= $flat->id; ?>">
                        <?php if($flat->sale_status != 1):?>
                            <li class="number">
                                <div class="plan-floor-title">квартира</div>
                                <div class="plan-floor-value">-</div>
                            </li>
                            <li class="rooms">
                                <div class="plan-floor-title">комнат</div>
                                <div class="plan-floor-value">-</div>
                            </li>
                            <li class="area">
                                <div class="plan-floor-title">площадь, м2</div>
                                <div class="plan-floor-value">-</div>
                            </li>
                            <li class="price">
                                <div class="plan-floor-title">стоимость, РУБ</div>
                                <div class="plan-floor-value"><?= ($flat->sale_status == 2) ? 'продано' :  'бронь'?></div>
                            </li>
                        <?php else:?>
                            <li class="number">
                                <div class="plan-floor-title">квартира</div>
                                <div class="plan-floor-value">№<?= $flat->number?></div>
                            </li>
                            <li class="rooms">
                                <div class="plan-floor-title">комнат</div>
                                <div class="plan-floor-value"><?= ($flat->rooms != 0) ? $flat->rooms : 'Ст.'; ?></div>
                            </li>
                            <li class="area">
                                <div class="plan-floor-title">площадь, м<sup>2</sup></div>
                                <div class="plan-floor-value"><?= $flat->scale; ?></div>
                            </li>
                            <li class="price">
                                <div class="plan-floor-title">стоимость, РУБ</div>
                                <div class="plan-floor-value"><?= number_format($flat->price,0,'',' ') ?></div>
                            </li>
                        <?php endif;?>
                    </ul>
                    <?php
                    $i++;
                endforeach;?>
            <?php endif; ?>
        </div>

    </div>

    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
</div>

<?php
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/classic_test.css');
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/jquery-ui.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jquery-ui.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jQRangeSlider-min.js', ['depends' => 'yii\web\JqueryAsset']);


$this->registerJs(<<<JS
    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        documentHeight = $(document).height(),
        mainContainer = $('.floor-view'),
        contentBlock = $('.content-block'),
        svgBlock = $('#svg-block'),
        imgContainer = $('.kor-choice-home .main__image'),
        resizeTimer;

    $('polygon').hover(function(e){
        //objectData = $(this)[0].getBoundingClientRect();
        var key = $(this).data('key'),
            tooltip = $('#lw-tooltip'+key),
            side = 'left';

            if($(this).position().left < 400)
                side = 'right';

        $(tooltip).css({top:countTop(e, tooltip), left:countLeft(e, tooltip)}).stop().fadeIn('fast');
            
        $('#floor'+key).addClass('active');
        this.addEventListener("mousemove",function(e){
            $(tooltip).css({top:countTop(e, tooltip), left:countLeft(e, tooltip)});
        });

        function countTop(e, tooltip)
        {
            return e.clientY-tooltip.height()/2;
        }

        function countLeft(e, tooltip)
        {
            return e.clientX-tooltip.width()-30;
        }
    },function(){
        var key = $(this).data('key'),
            tooltip = $('#lw-tooltip'+key);
        $('#floor'+key).removeClass('active');
        $(tooltip).stop().fadeOut('fast');
    });

    readyToShow = false;

    $(document).ready(function(){
        setTimeout(function(){
            var imgWidth = $('img',imgContainer).width(),
                imgHeight = $('img',imgContainer).height(),
                imgInterval = setInterval(function(){
                    if(imgWidth!=0 || imgHeight!=0)
                    {
                        clearInterval(imgInterval);
                        $(imgContainer).css({width:$('img',imgContainer).width(),minHeight:$('img',imgContainer).height()});
                        readyToShow = true;
                    }
                    else {
                        imgWidth = $('img',imgContainer).width();
                        imgHeight = $('img',imgContainer).height();
                    }
                },100);

            var interval = setInterval(function(){
                if(window.loaded && readyToShow)
                {
                    var contentBlockHeight = parseInt($(contentBlock).outerHeight(true));
                    clearInterval(interval);
                    if(windowHeight < contentBlockHeight)
                    {
                        $(mainContainer).css('height',contentBlockHeight);
                    }
                    loadingBlockHide();
                    // resizePolygon('$imgInfo[0]','$imgInfo[1]');
                }
            },100);
            resizePolygon('$imgInfo[0]','$imgInfo[1]');
        },150);
    });

    $(window).on('resize', function(e) {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
            var contentBlockHeight = parseInt($(contentBlock).outerHeight(true)),
                windowHeight = $(window).height();

            if(windowHeight < contentBlockHeight)
            {
                $(mainContainer).css('height',contentBlockHeight);
            }
            else{
                $(mainContainer).css('height','inherit');
            }
            
        }, 150);
        resizePolygon('$imgInfo[0]','$imgInfo[1]');
    });

    setTimeout(function(){
        $('.sidebar .toggle').trigger('click');
    },1000);
JS
)
?>

