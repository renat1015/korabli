<?php
use app\modules\realty\models\RealtyFlat;
use yii\helpers\Html;
use yii\web\View;

/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 16.11.15
 * Time: 10:18
 * @var $model RealtyFlat
 * @var $this View
 */
$this->registerCssFile(Yii::$app->request->baseUrl . '/js/fancybox/jquery.fancybox.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/fancybox/jquery.fancybox.js', ['depends' => 'yii\web\JqueryAsset']);

$panorama_images = $model->getImages();
$this->context->sidebarAdvancedClass = 'full-color';
//$this->context->sidebarAdvancedClass = 'full-color pre-closed';
$this->context->showLoading = true;
$this->context->headerAdvanceClass = 'choise-plan';
?>

<div class="kor-choice-home choise-plan">
    <div class="logo">
        <a class="white" href="/"><img src="/img/w_logo.png" alt="img"></a>
    </div>
    <div class="title">
        <h3>Выбор планировки</h3>
        <a href="#">
            <img src="/img/inform.png" alt="img">
            <span>Выберите планировку квартиры</span>
        </a>
    </div>
    <div class="param">
        <div class="param-inner">
            <a href="#">
                <img src="/img/param.png" alt="img">
                <span>Подбор по параметрам</span>
            </a>
        </div>
    </div>
    <?= Yii::$app->controller->renderPartial('_params');?>
    <div class="selection">
        <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-house', 'id' => $model->house_id])?>" class="back"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
        <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-house', 'id' => $model->house_id])?>" class="home-num">
            <span class="value"><?= $model->house->house_number;?></span>дом
        </a>
    </div>
    <div class="plan-block">
        <div class="plan-charact">
            <h3 class="plan-title">№<?= $model->number?></h3>
            <p class="rooms">
                <?= Yii::t('app', '{n, plural, =0{ Студия} =1{# - комнатная} many{#-ти комнатная} other{#-х комнатная}}', ['n' => $model->rooms]) ?>
            </p>
            <div class="charact-block">
                <p class="charact"><span class="name-char">Общая пл.: </span><span class="value"><?= $model->scale ?> м<sup>2</sup></span></p>
                <p class="charact"><span class="name-char">Жилая пл.:</span> <span class="value"><?= $model->living_scale ?> м<sup>2</sup></span></p>
                <?php
                if(($rooms = $model->rooms_array) != null):
                    foreach ($rooms as $value) :?>
                        <p class="charact">
                            <span class="name-char"><?= $value['name'];?>:</span>
                            <span class="value"><?= $value['value'];?> м<sup>2</sup></span>
                        </p>
                <?php
                    endforeach;
                endif;
                ?>
            </div>
            <p class="charact price">
                <span class="name-char">Цена:</span>
                <span class="value">
                    <?= number_format($model->price,0,'',' ') ?>
                </span>руб.
            </p>
            <a class="calc-linc" href="<?= \yii\helpers\Url::to(['/ipoteka', 'price' => $model->price]) ?>" onclick="ym(45591978,'reachGoal','ipoteka_calculator_click')">ипотечный калькулятор</a>
        </div>
        <div class="plan-image">
<!--            --><?//= Html::img($model->getImage(),['alt'=>$model->title]) ?>
            <img src="<?= $model->image_zh; ?>" alt="img">
<!--            <img class="print" src="/img/plan-inner-black.png" alt="img">-->
        </div>
        <div class="plan-lincs">
            <?= Html::a('<span>заявка на бронь</span>', ['/contacts/default/reserve'], ['class' => 'bron open-modal', 'onclick' => "ym(45591978,'reachGoal','zabronirovat_click')"]) ?>
            <?php if($panorama_images)
            {
                echo Html::a('вид из окна','#',['class'=>'window']);
            }
            ?>
            <?php if($model->tour):?>
                <a class="tour-linc" href="<?= \yii\helpers\Url::to(['/realty/realty/tour', 'id' => $model->id])?>">3D тур</a>
            <?php endif;?>
            <a target="_blank" href="<?= \yii\helpers\Url::to(['/realty/realty/pdf', 'id' => $model->id]) ?>"
               class="print-link downloard" >скачать</a>
            <a class="compas" href="#" style="background-image: url('/img/compas/<?= $model->side; ?>.png');"></a>
        </div>
    </div>
    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
</div>

<?php if($panorama_images):?>
<div class="lw-modal-body window-view-modal">
    <a href="#" class="btn-menu-close">
        <span class="btn-close lw-modal-close close-icon"></span> <span>Закрыть</span>
    </a>
    <div class="lw-modal-content">
        <div class="window-view">
            <?php
            foreach ($panorama_images as $image) :?>
                <div class="image">
                    <?php echo Html::img($image->getOriginalUrl()); ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="counter"><span class="slide-count">1</span> / <div class="slides-all"></div></div>
    </div>
</div>
<?php endif;?>
<?php
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/classic_test.css');
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/jquery-ui.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jquery-ui.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jQRangeSlider-min.js', ['depends' => 'yii\web\JqueryAsset']);


$this->registerJs(<<<JS
    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        documentHeight = $(document).height(),
        mainContainer = $('.flat-view'),
        contentBlock = $('.content-block'),
        imgContainer = $('.img-container'),
        resizeTimer;

    $(document).ready(function(){
        var dynamicArea = $('.area.dynamic');
        if(dynamicArea)
        {
            $(dynamicArea).css({
                borderRadius:$(dynamicArea).width()
            });
        }
        setTimeout(function(){
            var interval = setInterval(function(){
                if(window.loaded)
                {
                    var contentBlockHeight = parseInt($(contentBlock).outerHeight(true));
                    clearInterval(interval);
                    readyToShow = true;
                    if(windowHeight < contentBlockHeight)
                    {
                        $(mainContainer).css('height',contentBlockHeight);
                    }
                    loadingBlockHide();
                }
            },100);
        },150);
    });

     $(window).on('resize', function(e) {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
            var contentBlockHeight = parseInt($(contentBlock).outerHeight(true)),
                windowHeight = $(window).height();

            if(windowHeight < contentBlockHeight)
            {
                $(mainContainer).css('height',contentBlockHeight);
            }
            else{
                $(mainContainer).css('height','inherit');
            }
        }, 150);
    });

    $('.panorama-link').click(function(e){
        e.preventDefault();
        var galleryArray = [];
        $('.panorama-img img').each(function(i,e){
            galleryArray.push({href:$(e).attr('src')});
        });
        $.fancybox.open(galleryArray, {
            padding : 0,
            maxWidth: 1200,
            tpl: {
                wrap : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                image : '<img class="fancybox-image" src="{href}" alt="" />',
                closeBtn : '<a title="Закрыть" class="close-icon" href="javascript:;"></a>',
                next : '<a title="Предидущая" class="cp-gallery-nav next" href="javascript:;"><span></span></a>',
                prev : '<a title="Следидущая" class="cp-gallery-nav prev" href="javascript:;"><span></span></a>'
            }
        });
    });

    setTimeout(function(){
         $('.sidebar .toggle').trigger('click');
    },1000);
JS
)
?>

