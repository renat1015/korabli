<?php
use app\modules\realty\models\RealtyHouse;
use yii\helpers\Html;
use yii\web\View;

/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 16.11.15
 * Time: 10:18
 * @var $model RealtyHouse
 * @var $houses RealtyHouses
 * @var $this View
 */
//$this->context->sidebarAdvancedClass = 'pre-closed';
$this->context->showLoading = true;
$mainImg = $model->getImage();
$imgInfo = array();
$imgInfo[] = '';
$imgInfo[] = '';
//    $imgInfo = getimagesize(Yii::$app->basePath.'/../'.$mainImg);
//$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
//$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',['depends'=>'yii\web\JqueryAsset']);

$flats = array();

foreach(array_reverse($model->realtyFloors) as $floor):
    if(($floor->realtyFlats) != null):
        foreach ($floor->realtyFlats as $flat):
            $flats[] = $flat;
        endforeach;
    endif;
endforeach;

$min_rooms = $max_rooms = $flats[0]->rooms;
$min_scale = $max_scale = $flats[0]->scale;
$min_price = $max_price = $flats[0]->price;
$rooms = array();

foreach ($flats as $flat){
    $rooms[$flat->rooms] = $flat->rooms;

    if ($flat->scale > $max_scale) {
        $max_scale = $flat->scale;
    } else if ($flat->scale < $min_scale) {
        $min_scale = $flat->scale;
    }

    if ($flat->price > $max_price) {
        $max_price = $flat->price;
    }else if ($flat->price < $min_price) {
        $min_price = $flat->price;
    }
}

?>

<div class="kor-choice-home">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="title">
        <h3>Выбор этажа</h3>
        <a href="#">
            <img src="/img/inform.png" alt="img">
            <span>Выберите этаж</span>
        </a>
    </div>
    <div class="param">
        <div class="param-inner">
            <a href="#">
                <img src="/img/param.png" alt="img">
                <span>Подбор по параметрам</span>
            </a>
        </div>
    </div>
    <?php // echo Yii::$app->controller->renderPartial('_params');?>

    <div class="selection">
        <a href="<?= \yii\helpers\Url::to(['/realty/realty'])?>" class="back"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
        <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-house', 'id' => $model->id])?>" class="home-num">
            <span class="value"><?= $model->house_number;?></span>дом
        </a>
    </div>

    <?php if(($floors = $model->realtyFloors) != null): ?>
        <div class="house house__table">
            <div class="house-filter">
                <form method="get" class="house_filter_form" action="#">
                    <div class="status_choose">
                        <div class="status_choose-item">
                            <label for="status-all" class="container radio">Все квартиры
                                <input type="radio" id="status-all" name="status" value="all" checked>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="status_choose-item">
                            <label for="status-1" class="container radio">Свободные
                                <input type="radio" id="status-1" name="status" value="1">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <?php foreach ($houses as $house):
                        if($house->status != $house::STATUS_ACTIVE) {
                            continue;
                        }
                        ?>
                        <a href="<?= \yii\helpers\Url::to(['/realty/realty/view-house', 'id' => $house->id])?>" class="house__link <?= ($house->id == $model->id) ? 'active' : ''?>"><?= $house->house_number; ?></a>
                    <?php endforeach;?>
                    <div class="rooms-choosing">
                    <?php
                    sort($rooms);
                    foreach ($rooms as $room): ?>
                        <div class="rooms-choose">
<!--                            <label for="rooms---><?//= $room; ?><!--">-->
<!--                                --><?php
//                                if($room > 0){
//                                    echo $room . 'K';
//                                } else {
//                                    echo 'Ст';
//                                }
//                                ?>
<!--                            </label>-->
<!--                            <input type="checkbox" id="rooms---><?//= $room; ?><!--" name="rooms" value="--><?//= $room; ?><!--" checked>-->


                            <label for="rooms-<?= $room; ?>" class="container">
                                <?php
                                if($room > 0){
                                    echo $room . 'K';
                                } else {
                                    echo 'Ст';
                                }
                                ?>
                                <input type="checkbox" id="rooms-<?= $room; ?>" name="rooms" value="<?= $room; ?>" checked>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    <?php endforeach;?>
                    </div>
<!--                    <div class="slider-holder">-->
<!--                        <div id="slider_1"></div>-->
<!--                    </div>-->
                    <div class="slider-holder">
                        <div id="slider_3"></div>
                    </div>
                    <div class="slider-holder">
                        <div id="slider_4"></div>
                    </div>

<!--                    <p>-->
<!--                        <label for="amount">Price range:</label>-->
<!--                        <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">-->
<!--                    </p>-->
<!--                    <div id="slider-range"></div>-->
                </form>
            </div>
            <div class="table-wrap">
                <div class="table-item">
                    <div class="table-status">
                        <span class="curr-status">Свободно</span>
                        <span class="curr-status curr-status_book">Бронь</span>
                        <span class="curr-status curr-status_reserv">Продано</span>
                    </div>
                    <div class="section-table">
                        <div class="section-table__nums">
                            <?php foreach(array_reverse($floors) as $floor):
                                if(($floor->realtyFlats) != null): ?>
                                    <div class="section-table__num"><?= $floor->title; ?></div>
                                <?php endif;?>
                            <? endforeach; ?>
                        </div>
                        <div class="section-table__rows">
                            <?php foreach(array_reverse($floors) as $floor):
                                if(($floor->realtyFlats) != null): ?>
                                    <div class="section-table__row">
                                        <?php foreach ($floor->realtyFlats as $flat):?>
                                            <a href="<?= ($flat->sale_status != 2 && $flat->sale_status != 3) ? $flat->getSeoUrl() : '#'; ?>" class="section-table__num <?= ($flat->sale_status == 2) ? 'sale' : ''; ?><?= ($flat->sale_status == 3) ? 'reserved' : ' '; ?>" data-price="<?= $flat->price; ?>" data-rooms="<?= $flat->rooms; ?>" data-scale="<?= $flat->scale; ?>" data-status="<?= $flat->sale_status; ?>">
                                                <span class="count_rooms">
                                                    <?php
                                                        if($flat->rooms > 0){
                                                            echo $flat->rooms . 'K';
                                                        } else {
                                                            echo 'Ст';
                                                        }
                                                    ?>
                                                </span>
                                                <span class="scale hide"><?= $flat->scale . ' м²'?></span>
                                                <span class="price">
                                                    <?= round($flat->price / 1000000, 2) . ' млн'?>
                                                </span>
                                                <span class="number hide"><?= 'кв ' . $flat->number; ?></span>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif;?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="image main__image">
        <?php // echo \yii\helpers\Html::img($mainImg,['alt'=>$model->title]) ?>
        <?php if(($floors = $model->realtyFloors) != null): ?>
            <div class="floors">
                <svg id="svg-block">
                    <?php foreach ($floors as $floor) {
                        echo \yii\bootstrap\Html::tag('polygon','',[
                            'points' => $floor->cords,
                            'title' => $floor->title,
                            'data' => [
                                'key' => $floor->id,
                                'url' => $floor->getSeoUrl(),
                                'real-points' => $floor->cords
                            ],
                        ]);
                    } ?>
                </svg>
                <div class="tooltips">
                    <?php foreach ($floors as $floor):  ?>
                        <div id="lw-tooltip<?= $floor->id ?>" class="lw-tooltip">
                            <div class="home-number floor active">
                                <form>
                                    <div class="info-house">
                                        <label>В продаже</label>
                                        <input type="text" placeholder="<?= count($floor->saleFlats); ?>">
                                    </div>
                                    <div class="number">
                                        <?= $floor->title ?>
                                        <span>этаж</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>

<!--
<div class="house-view">
    <div class="top-menu clearfix right-offset">
        <div class="pull-left">
            <?= Html::a('.',($model->plan->getSeoUrl()),['class'=>'back-link','data'=>[
                'toggle'=>'tooltip',
                'placement'=>'bottom',
                'title' => 'Вернуться к Ген-плану'
            ]]) ?>
        </div>
        <div class="pull-left">
            <h1><?= $model->title ?></h1>
        </div>
    </div>
    <div class="img-container">
        <?= \yii\helpers\Html::img($mainImg,['alt'=>$model->title]) ?>
        <?php if(($floors = $model->realtyFloors) != null): ?>
            <div class="floors">
                <svg id="svg-block">
                    <?php foreach ($floors as $floor) {
                        echo \yii\bootstrap\Html::tag('polygon','',[
                            'points' => $floor->cords,
                            'title' => $floor->title,
                            'data' => [
                                'key' => $floor->id,
                                'url' => $floor->getSeoUrl(),
                                'real-points' => $floor->cords
                            ],
                        ]);
                    } ?>
                </svg>
                <div class="tooltips">
                    <?php foreach ($floors as $floor):  ?>
                        <div id="lw-tooltip<?= $floor->id ?>" class="lw-tooltip">
                            <div class="tooltip-content">
                                <div class="text">
                                    <h2><?= $floor->title ?></h2>
                                </div>
                                <div class="info">
                                    <span>Смотреть план этажа</span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
-->

<?php
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/classic_test.css');
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/jquery-ui.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jquery-ui.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jQRangeSlider-min.js', ['depends' => 'yii\web\JqueryAsset']);


$this->registerJs(<<<JS
    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        imgContainer = $('.kor-choice-home .main__image'),
        mainImg = $('img',imgContainer),
        svgBlock = $('#svg-block'),
        resizeTimer;

    $(window).on('resize', function(e) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        setImgContainerPos();
//        resizePolygon('$imgInfo[0]','$imgInfo[1]');
      }, 150);
    });

    readyToShow = false;

    $(document).ready(function(){
        setTimeout(function(){
            var interval = setInterval(function(){
                if(window.loaded)
                {
                    clearInterval(interval);
                    readyToShow = true;
                    // setImgContainerPos();
                    loadingBlockHide();
                }
            },100);
            setImgContainerPos();
//            resizePolygon('$imgInfo[0]','$imgInfo[1]');
        },50);
        
        $("#slider_3").rangeSlider({
            bounds:{min: $min_scale, max: $max_scale},
            defaultValues:{min: $min_scale, max: $max_scale},
            step: 0.1,
            formatter:function(val){
                var value = val.toFixed(1),
                decimal = value - Math.round(val);
                
                return value.toString();
            }
        });
        
        $("#slider_4").rangeSlider({
            bounds:{min: $min_price, max: $max_price},
            defaultValues:{min: $min_price, max: $max_price},
            step: 100,
            formatter:function(val){
                // var value = Math.round(val * 5) / 5,
                // decimal = value - Math.round(val);
                return val.toLocaleString();
            }
        });
        
        $('#slider_3, #slider_4').on('valuesChanged', function () {
            $( ".house_filter_form" ).submit();
        });
        
        $('input[name="rooms"]').change(function() {
            $( ".house_filter_form" ).submit();      
        });
        
        $('input[name="status"]').change(function() {
            $( ".house_filter_form" ).submit();      
        });
        
        $('.house_filter_form').on('submit', function () {
            var scaleMin = $("#slider_3").rangeSlider("min"),
                scaleMax = $("#slider_3").rangeSlider("max"),
                priceMin = $("#slider_4").rangeSlider("min"),
                priceMax = $("#slider_4").rangeSlider("max"),
                status = $('input[name="status"]:checked').val(),
                roomsA = [];
            
            $('input[name="rooms"]').each(function(){
                if($(this).is(':checked')){
                    roomsA.push($(this).val());
                }
            });
            
            $('.table-wrap .section-table__rows .section-table__num').each(function(){
                var rooms = $(this).data('rooms'),
                    scale = $(this).data('scale'),
                    price = $(this).data('price'),
                    statusR = $(this).data('status');
                
                var scaleB = roomsB = priceB = statusB = false;
                
                $.each(roomsA, function (index, value) {
                    if(rooms == value) roomsB = true;
                    
                    if (roomsB)
                        return false;
                });
                
                if(scaleMin <= scale && scale <= scaleMax){
                    scaleB = true;
                }
                if(priceMin <= price && price <= priceMax){
                    priceB = true;
                }
                
                
                if($.trim(statusR) == status){
                    statusB = true;
                } 
                
                if(status == 'all') {
                    if(scaleB && priceB && roomsB){
                        $(this).removeClass('clear');
                    } else {
                        $(this).addClass('clear');
                    }
                } else {
                    if(scaleB && priceB && roomsB && statusB){
                        $(this).removeClass('clear');
                    } else {
                        $(this).addClass('clear');
                    }
                }
            });

            return false;
        });
    });

    setTimeout(function(){
         $('.sidebar .toggle').trigger('click');
    },1000);
JS
)
?>

