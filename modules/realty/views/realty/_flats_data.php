<?php
use app\modules\realty\controllers\RealtyController;
?>

<?php foreach ($flats as $flat):?>
    <?php $rooms = $flat->rooms_array;
    $result1 = '';
    $result2 = '';
    $one_result = '';
    if(in_array('balcon', $data['rooms']) && in_array('lodgy', $data['rooms'])){
        $result1 = RealtyController::search($rooms,'Балкон');
        $result2 = RealtyController::search($rooms,'Лоджия');
    } else if(in_array('balcon', $data['rooms'])){
        $one_result = RealtyController::search($rooms,'Балкон');
    } else if(in_array('lodgy', $data['rooms'])){
        $one_result = RealtyController::search($rooms,'Лоджия');
    }
    ?>
    <?php if(
        (intval($flat->floor->title) <= $data['floorMax'] && intval($flat->floor->title) >= $data['floorMin']) &&
        (($result1 && $result2) || $one_result)
    ):?>
        <a class="tr" href="<?= $flat->getSeoUrl(); ?>">
            <span class="image-hide">
                <?php if($flat->getImage() != '/uploads/flat/'):?>
                    <img src="<?= $flat->getImage();?>" alt="img"  class="mCS_img_loaded">
                <?php endif;?>
            </span>
            <span class="td"><?= $flat->number;?></span>
            <span class="td"><?= $flat->floor->title;?></span>
            <span class="td">
                <?= Yii::t('app', '{n, plural, =0{ Студия} =1{# - комнатная} many{#-ти комнатная} other{#-х комнатная}}', ['n' => $flat->rooms]) ?>
            </span>
            <span class="td"><?= $flat->scale;?></span>
            <span class="price td"><?= number_format($flat->price,0,'',' ') ?></span>
        </a>
    <?php endif;?>
<?php endforeach;?>