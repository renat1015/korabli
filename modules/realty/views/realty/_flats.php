<?php foreach ($flats as $flat):?>
    <a class="tr" href="<?= $flat->getSeoUrl(); ?>">
        <span class="image-hide">
            <?php if($flat->getImage() != '/uploads/flat/'):?>
                <img src="<?= $flat->getImage();?>" alt="img" class="mCS_img_loaded">
            <?php endif;?>
        </span>
        <span class="td"><?= $flat->number;?></span>
        <span class="td"><?= $flat->floor->title;?></span>
        <span class="td">
            <?= Yii::t('app', '{n, plural, =0{ Студия} =1{# - комнатная} many{#-ти комнатная} other{#-х комнатная}}', ['n' => $flat->rooms]) ?>
        </span>
        <span class="td"><?= $flat->scale;?></span>
        <span class="price td"><?= number_format($flat->price,0,'',' ') ?></span>
    </a>
<?php endforeach;?>
