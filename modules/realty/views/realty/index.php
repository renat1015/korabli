<?php
/* @var $this yii\web\View
 * @var $genPlan RealtyPlan
 */
use app\modules\realty\models\RealtyPlan;

$builds = $genPlan->realtyHouses;
$mainImg = $genPlan->getImage();
$imgInfo = getimagesize(Yii::$app->basePath.'/../'.$mainImg);
//$this->context->sidebarAdvancedClass = 'pre-closed';
$this->context->showLoading = true;

?>
<style>
    
</style>
<div class="kor-choice-home height-wh">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="title">
        <h3>Выбор дома</h3>
        <a href="#">
            <img src="/img/inform.png" alt="img">
            <span>Выберите дом</span>
        </a>
    </div>
    <div class="param">
        <div class="param-inner">
            <a href="#">
                <img src="/img/param.png" alt="img">
                <span>Подбор по параметрам</span>
            </a>
        </div>
    </div>
    <?= Yii::$app->controller->renderPartial('_params' );?>
    <!--Эти блоки должны оставаться непосредственными соседями для корректной работы анимации-->

    <div class="image main__image">
        <?= \yii\helpers\Html::img($mainImg,['alt'=>Yii::$app->name.' - Генеральный план']) ?>

        <?php if($builds): ?>
            <div class="builds">
                <svg id="svg-block" style="height: 100%; width: 100%;">
                    <?php foreach ($builds as $build)
                    {
                        if($build->status != $build::STATUS_ACTIVE) {
                            continue;
                        }
                        echo \yii\bootstrap\Html::tag('polygon','',[
                            'points' => $build->cords,
                            'title' => $build->title,
                            'data' => [
                                'key' => $build->id,
                                'url' => $build->getSeoUrl(),
                                'real-points' => $build->cords,
                                'show-video'=>($build->video) ? true : false,
                                'in-query' => ($build->status == $build::STATUS_ACTIVE) ? false : true,
                            ],
                        ]);
                    }
                    ?>
                </svg>
                <div class="tooltips">
                    <?php foreach ($builds as $build):  ?>
                        <div id="lw-tooltip<?= $build->id ?>" class="lw-tooltip">
<!--                            <div class="tooltip-content inline-content">-->
                                <div class="home-number <?= ($build->status == $build::STATUS_ACTIVE) ? 'active' : ''?>">
                                    <form>
                                        <?php if($build->status == $build::STATUS_ACTIVE): ?>

                                            <div class="info-house">
                                                <label>Дом</label>
                                                <label>В продаже</label>
                                                <input type="text" placeholder="<?= count($build->saleFlats); ?>">
                                            </div>
                                        <?php endif; ?>

                                        <div class="number">
                                            <span><?= $build->house_number; ?></span>
                                        </div>
                                    </form>
                                </div>

                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>


<?php foreach($builds as $build): ?>
    <video id="video-<?= $build->id ?>" class="pre-redirect-video" data-redirect-url="<?= $build->getSeoUrl() ?>" preload="auto" onended="redirectPageAfterVideo($(this).data('redirect-url'))">
        <?= $build->getVideoSource() ?>
    </video>
<?php endforeach; ?>
<?php

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/classic_test.css');
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/jquery-ui.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jquery-ui.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jQRangeSlider-min.js', ['depends' => 'yii\web\JqueryAsset']);

//$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
//$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',['depends'=>'yii\web\JqueryAsset']);
$this->registerJs(<<<JS
    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        documentHeight = $(document).height(),
        mainContainer = $('.floor-view'),
        imgContainer = $('.kor-choice-home .main__image'),
        mainImg = $('img',imgContainer),
        svgBlock = $('#svg-block'),
        resizeTimer;

    $(window).on('resize', function(e) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        setImgContainerPos();
        resizePolygon('$imgInfo[0]','$imgInfo[1]');
      }, 150);
    });

    readyToShow = false;

    $(document).ready(function(){
        setTimeout(function(){
            var interval = setInterval(function(){
                if(window.loaded)
                {
                    clearInterval(interval);
                    readyToShow = true;
                    // setImgContainerPos();
                    loadingBlockHide();
                }
            },100);
            setImgContainerPos();
            resizePolygon('$imgInfo[0]','$imgInfo[1]');
        },100);
    });

    setTimeout(function(){
        $('.sidebar .toggle').trigger('click');
    },1000);

    //$('.img-container').css({height:windowHeight}).mCustomScrollbar({
    //    scrollbarPosition : 'outside',
    //    autoHideScrollbar: false,
    //    setTop: '-500px'
    //});

    //$('.img-container').mCustomScrollbar('scrollTo','bottom');
JS
)
?>