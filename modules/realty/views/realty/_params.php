<?php

$flats = \app\modules\realty\models\RealtyFlat::find()->where(['sale_status' => 1])->all();
$cnt = count($flats);
// определяем начальные значения
$min_rooms = $max_rooms = $flats[0]->rooms;
$min_floor = $max_floor = $flats[0]->floor->title;
$min_scale = $max_scale = $flats[0]->scale;
$min_price = $max_price = $flats[0]->price;
$is_lodgy = $is_balcon = false;
foreach ($flats as $flat){
    if ($flat->rooms > $max_rooms) {
        $max_rooms = $flat->rooms;
    } else if ($flat->rooms < $min_rooms) {
        $min_rooms = $flat->rooms;
    }

    if (intval($flat->floor->title) > $max_floor) {
        $max_floor = intval($flat->floor->title);
    } else if ($flat->floor->title < $min_floor) {
        $min_floor = intval($flat->floor->title);
    }

    if ($flat->scale > $max_scale) {
        $max_scale = $flat->scale;
    } else if ($flat->scale < $min_scale) {
        $min_scale = $flat->scale;
    }

    if ($flat->price > $max_price) {
        $max_price = $flat->price;
    }else if ($flat->price < $min_price) {
        $min_price = $flat->price;
    }

    if(($rooms = $flat->rooms_array) != null){
        foreach ($rooms as $value){
            if($value['name'] == 'Лоджия'){
                $is_lodgy = true;
                break;
            }
        }

        foreach ($rooms as $value){
            if($value['name'] == 'Балкон'){
                $is_balcon = true;
            }
        }
    }

}

?>

<div class="kor-param  plan">
    <a href="#" class="btn-menu-close">
        <span class="btn-close"></span> <span>Закрыть</span>
    </a>
    <h4>Подобрать квартиру</h4>

    <div class="param-form">
        <form method="post" class="range-slider" action="#">
            <div class="slider-holder">
                <div id="slider_1"></div>
            </div>
            <div class="slider-holder">
                <div id="slider_2"></div>
            </div>
            <div class="slider-holder">
                <div id="slider_3"></div>
            </div>
            <div class="slider-holder">
                <div id="slider_4"></div>
            </div>
            <div class="row">
                <?php if($is_lodgy):?>
                    <div class="check-block">
                        <input type="checkbox" value="lodgy">
                        <label>Лоджия</label>
                    </div>
                <?php endif;?>
                <?php if($is_balcon):?>
                    <div class="check-block">
                        <input type="checkbox" value="balcon">
                        <label>Балкон</label>
                    </div>
                <?php endif;?>
            </div>
        </form>
        <div class="table">
            <div class="plan-img">
                <div  id="td1" class="image"><img src="/img/plan1.jpg" alt="img"></div>
            </div>
            <div class="thead">
                <div class="tr">
                    <span class="image-hide"></span>
                    <div class="td default" ><span>№ квартиры</span> </div>
                    <div class="td default" ><span>Этаж</span></div>
                    <div class="td default" ><span>Комнат</span> </div>
                    <div class="td default" ><span>Площадь, м<span class="top-i">2</span></span></div>
                    <div class="td default price"><span>Стоимость, руб</span></div>
                </div>
            </div>
            <div class="tbody"></div>
        </div>
    </div>
</div>

<?php



$this->registerJs(<<<JS
    $(function () {
        $("#slider_1").rangeSlider({
            bounds:{min: $min_rooms, max: $max_rooms},
            defaultValues:{min: $min_rooms, max: $max_rooms},
            step: 1,
            formatter:function(val){
                // var value = Math.round(val * 5) / 5,
                // decimal = value - Math.round(val);
                return val == 0 ? 'Ст.' : val;
            }
        });
        $("#slider_2").rangeSlider({
            bounds:{min: $min_floor, max: $max_floor},
            defaultValues:{min: $min_floor, max: $max_floor},
            step: 1
        });
        $("#slider_3").rangeSlider({
            bounds:{min: $min_scale, max: $max_scale},
            defaultValues:{min: $min_scale, max: $max_scale},
            step: 0.1,
            formatter:function(val){
                var value = val.toFixed(1),
                decimal = value - Math.round(val);
                
                return value.toString();
            }
        });
        
//        console.log('slider-range');
//        $( "#slider-range" ).slider({
//          range: true,
//          min: $min_scale,
//          max: $max_scale,
//          values: [ $min_scale, $max_scale ],
//          slide: function( event, ui ) {
//            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
//          }
//        });
        // $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +" - $" + $( "#slider-range" ).slider( "values", 1 ) );
        
        $("#slider_4").rangeSlider({
            bounds:{min: $min_price, max: $max_price},
            defaultValues:{min: $min_price, max: $max_price},
            step: 1000,
            formatter:function(val){
                // var value = Math.round(val * 5) / 5,
                // decimal = value - Math.round(val);
                return val.toLocaleString();
            }
        });
        
        $('#slider_1, #slider_2, #slider_3, #slider_4').on('valuesChanged', function () {
            $( ".range-slider" ).submit();
        });
        
        // Set values when user uses the slider.
        // $("#slider_4").bind("valuesChanging", function(e, data) {
        //     $("#slider_4 .ui-rangeSlider-rightLabel .ui-rangeSlider-label-value").attr("data-value", data.values.max.toLocaleString());
        // });
    });

JS
)
?>