<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\realty\models\RealtyPlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Генеральный план';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realty-plan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'      => 'id',
                'contentOptions' => ['style' => 'width: 50px; text-align: center'],
            ],
            'title',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img($data->getImage(),
                        ['width' => '100px']);
                },
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;{delete}'
            ],
        ],
    ]); ?>

</div>
