<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_083559_init_realty extends Migration
{
    /**
     *
     */
    public function up()
    {

        $this->createTable('realty_plan', [
            'id'         => $this->primaryKey(),
            'title'      => $this->string(255)->notNull(),
            'image'      => $this->string(255)->notNull(),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
            'status'     => $this->smallInteger(1)->defaultValue(1),
        ]);

        $this->createTable('realty_house', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string(255)->notNull(),
            'description' => $this->text(),
            'image'       => $this->string(255)->notNull(),
            'floors'      => $this->integer()->notNull(),
            'flats'       => $this->integer()->notNull(),
            'plan_id'     => $this->integer()->notNull(),

            'cords'       => $this->text()->notNull(),

            'updated_at'  => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'  => 'timestamp not null',
            'status'      => $this->smallInteger(1)->defaultValue(1),
        ]);

        $this->createTable('realty_section', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string(255)->notNull(),
            'description' => $this->text(),
            'image'       => $this->string(255)->notNull(),
            'cords'       => $this->text()->notNull(),
            'house_id'    => $this->integer()->notNull(),

            'updated_at'  => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'  => 'timestamp not null',
            'status'      => $this->smallInteger(1)->defaultValue(1),
        ]);

        $this->createTable('realty_floor', [
            'id'          => $this->primaryKey(),

            'title'       => $this->string(255)->notNull(),
            'description' => $this->text(),
            'image'       => $this->string(255)->notNull(),
            'cords'       => $this->text()->notNull(),
            'house_id'    => $this->integer()->notNull(),
            'section_id'  => $this->integer()->defaultValue(null),

            'updated_at'  => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'  => 'timestamp not null',

            'status'      => $this->smallInteger(1)->defaultValue(1),
        ]);

        $this->createTable('realty_flat', [
            'id'             => $this->primaryKey(),

            'title'          => $this->string(255)->notNull(),
            'description'    => $this->text(),
            'image'          => $this->string(255)->notNull(),
            'cords'          => $this->text()->notNull(),

            'house_id'       => $this->integer()->notNull(),
            'section_id'     => $this->integer()->defaultValue(null),
            'floor_id'       => $this->integer()->notNull(),

            'price'          => $this->float()->notNull(),
            'scale'          => $this->float()->notNull(),
            'rooms'          => $this->integer()->notNull(),

            'updated_at'     => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at'     => 'timestamp not null',
            'status'         => $this->smallInteger(1)->defaultValue(1),
        ]);


        $this->addForeignKey('fk_house_plan', 'realty_house', 'plan_id', 'realty_plan', 'id');

        $this->addForeignKey('fk_section_house', 'realty_section', 'house_id', 'realty_house', 'id');

        $this->addForeignKey('fk_floor_house', 'realty_floor', 'house_id', 'realty_house', 'id');
        $this->addForeignKey('fk_floor_section', 'realty_floor', 'section_id', 'realty_section', 'id');

        $this->addForeignKey('fk_flat_house', 'realty_flat', 'house_id', 'realty_house', 'id');
        $this->addForeignKey('fk_flat_section', 'realty_flat', 'section_id', 'realty_section', 'id');
        $this->addForeignKey('fk_flat_floor', 'realty_flat', 'floor_id', 'realty_floor', 'id');
    }

    /**
     *
     */
    public function down()
    {

        $this->dropForeignKey('fk_flat_house', 'realty_flat');
        $this->dropForeignKey('fk_flat_section', 'realty_flat');
        $this->dropForeignKey('fk_flat_floor', 'realty_flat');

        $this->dropForeignKey('fk_floor_house', 'realty_floor');
        $this->dropForeignKey('fk_floor_section', 'realty_floor');

        $this->dropForeignKey('fk_section_house', 'realty_section');

        $this->dropForeignKey('fk_house_plan', 'realty_house');

        $this->dropTable('realty_flat');
        $this->dropTable('realty_floor');
        $this->dropTable('realty_section');
        $this->dropTable('realty_house');
        $this->dropTable('realty_plan');

    }

}
