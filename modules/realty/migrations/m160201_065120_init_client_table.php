<?php

use yii\db\Schema;
use yii\db\Migration;

class m160201_065120_init_client_table extends Migration
{
    public function up()
    {
        $this->createTable('realty_client', [
            'id'         => $this->primaryKey(),
            'full_name'  => $this->string()->notNull(),
            'phone'      => $this->string()->notNull(),
            'email'      => $this->string()->notNull(),
            'first_date' => $this->timestamp(),
            'last_date'  => $this->timestamp(),
            'next_date'  => $this->timestamp(),
            'comment'    => $this->text(),
            'payment'    => $this->integer()->defaultValue(null),
            'mortgage'   => $this->smallInteger(1)->defaultValue(1),
            'source'     => $this->smallInteger(1)->defaultValue(null),
            'files'      => $this->text(),

            'flat_id'    => $this->integer()->notNull(),
            'floor_id'   => $this->integer()->notNull(),
            'section_id' => $this->integer()->defaultValue(null),
            'house_id'   => $this->integer()->notNull(),

            'status' => $this->smallInteger(1)->defaultValue(1),
            'stage'  => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',

        ]);

        $this->addForeignKey('fk_client_house', 'realty_client', 'flat_id', 'realty_flat', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_client_house', 'realty_client');
        $this->dropTable('realty_client');
    }

}
