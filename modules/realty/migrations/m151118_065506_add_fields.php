<?php

use yii\db\Schema;
use yii\db\Migration;

class m151118_065506_add_fields extends Migration
{
    public function up()
    {
        $this->addColumn('realty_floor', 'flat_count', 'INT(11)');
        $this->addColumn('realty_floor', 'scale', 'FLOAT(10,2)');

        $this->addColumn('realty_flat', 'rooms_scale', 'TEXT');
    }

    public function down()
    {
        $this->dropColumn('realty_floor', 'flat_count');
        $this->dropColumn('realty_floor', 'scale');
        $this->dropColumn('realty_flat', 'rooms_scale');
    }

}
