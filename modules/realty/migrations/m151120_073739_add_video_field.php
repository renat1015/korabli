<?php

use yii\db\Schema;
use yii\db\Migration;

class m151120_073739_add_video_field extends Migration
{
    public function up()
    {
        $this->addColumn('realty_house', 'video', 'varchar(255) default NULL');
    }

    public function down()
    {
        $this->dropColumn('realty_house', 'video');
    }

}
