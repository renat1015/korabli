<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_074524_add_fields extends Migration
{
    public function up()
    {
        $this->addColumn('realty_flat', 'sale_status', 'smallint(1) default 0');
    }

    public function down()
    {
        $this->dropColumn('realty_flat', 'sale_status');
    }
}
