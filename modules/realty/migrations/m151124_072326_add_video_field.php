<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_072326_add_video_field extends Migration
{
    public function up()
    {
        $this->addColumn('realty_house', 'youtube_video', 'varchar(255) default NULL');
    }

    public function down()
    {
        $this->dropColumn('realty_house', 'youtube_video');
    }

}
