<?php

namespace app\modules\realty\controllers;

use app\common\BaseAdminController;
use app\common\BaseModel;
use app\modules\realty\models\RealtyHouse;
use app\modules\realty\models\RealtySection;
use Yii;
use app\modules\realty\models\RealtyFlat;
use app\modules\realty\models\RealtyFlatSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * FlatController implements the CRUD actions for RealtyFlat model.
 */
class FlatController extends BaseAdminController
{


    /**
     * Lists all RealtyFlat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RealtyFlatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RealtyFlat model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RealtyFlat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RealtyFlat();
        $model->scenario = 'create';
        $model->status = BaseModel::STATUS_PUBLISHED;

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->imgFiles = UploadedFile::getInstances($model,'imgFiles');
            if ($model->save()) {
                Yii::$app->session->setFlash('humane', 'Сохранено');
                return $this->redirect(['update', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model'    => $model,
            'sections' => [],
            'floors'   => [],
        ]);
    }

    /**
     * Updates an existing RealtyPlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->imgFiles = UploadedFile::getInstances($model,'imgFiles');
            if ($model->save(false)) {
                Yii::$app->session->setFlash('humane', 'Сохранено');
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {

            $house = RealtyHouse::find()->where(['id' => $model->house_id])->one();

            $sections = ArrayHelper::map($house->realtySections, 'id', 'title');
            $floors = ArrayHelper::map($house->realtyFloors, 'id', 'title');

            return $this->render('update', [
                'model'    => $model,
                'sections' => $sections,
                'floors'   => $floors,
            ]);
        }
    }

    /**
     * Deletes an existing RealtyFlat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RealtyFlat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return RealtyFlat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RealtyFlat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionResave()
    {
        $items = RealtyFlat::find()->limit(500)->all();

        if ($items) {
            foreach ($items as $item) {
                $item->is_copy = true;

                $list = [];
                foreach ($item->rooms_array as $key => $value) {
                    $list[] = [
                        'name'  => $key,
                        'value' => $value,
                    ];
                }
                $item->rooms_scale = $list;

                dump($item->save(false));
            }
        }

    }
}
