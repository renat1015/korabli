<?php

namespace app\modules\realty\controllers;

use app\common\BaseAdminController;
use app\common\BaseModel;
use app\modules\realty\models\RealtySection;
use Yii;
use app\modules\realty\models\RealtyFloor;
use app\modules\realty\models\RealtyFloorSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FloorController implements the CRUD actions for RealtyFloor model.
 */
class FloorController extends BaseAdminController
{

    /**
     * Lists all RealtyFloor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RealtyFloorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RealtyFloor model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCopy($id)
    {
        $model = $this->findModel($id);
        $flats = $model->realtyFlats;
        $image = $model->image;
        $model->id = null;

        $model->setIsNewRecord(true);
        $model->is_copy = true;

        if ($model->load(Yii::$app->request->post())) {
            $model->image = $image;
            $model->save();
        }

        $sections = ArrayHelper::map(RealtySection::find()->where(['house_id' => $model->house_id])->all(), 'id', 'title');

        return $this->render('copy', [
            'model'    => $model,
            'sections' => $sections,
            'flats'    => $flats,
        ]);

    }

    /**
     * Creates a new RealtyFloor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RealtyFloor();
        $model->scenario = 'create';
        $model->status = BaseModel::STATUS_PUBLISHED;

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->save()) {
                Yii::$app->session->setFlash('humane', 'Сохранено');

                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model'    => $model,
            'sections' => [],
        ]);
    }

    /**
     * Updates an existing RealtyPlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->save()) {
                Yii::$app->session->setFlash('humane', 'Сохранено');

                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {

            $sections = ArrayHelper::map(RealtySection::find()->where(['house_id' => $model->house_id])->all(), 'id', 'title');

            return $this->render('update', [
                'model'    => $model,
                'sections' => $sections,
            ]);
        }
    }

    /**
     * Deletes an existing RealtyFloor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RealtyFloor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return RealtyFloor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RealtyFloor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionOrdering()
    {
        $id = Yii::$app->request->post('id');
        $ordering = Yii::$app->request->post('val', 0);

        $model = $this->findModel($id);

        if($model)
        {
            $model->ordering = $ordering;
            $model->save(false);
        }
    }

    public function actionReorder()
    {
        $items = RealtyFloor::find()->all();
        if($items)
        {
            foreach ($items as $item) {
                $data = explode(' ', $item->title);
                $item->ordering = $data[0];
                $item->save(false);
            }
        }

    }
}
