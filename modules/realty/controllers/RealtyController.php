<?php

namespace app\modules\realty\controllers;

use app\common\BaseController;
use app\modules\realty\models\RealtyFlat;
use app\modules\realty\models\RealtyFloor;
use app\modules\realty\models\RealtyHouse;
use app\modules\realty\models\RealtyPlan;
use app\modules\realty\models\RealtySection;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class RealtyController extends BaseController
{
    /**
     * @return string
     */
    public function actionGetPlanImage()
    {
        $id = Yii::$app->request->post('id');
        $plan = $this->findPlan($id);

        Yii::$app->response->format = Response::FORMAT_JSON;

        $img = getimagesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/plan/' . $plan->image);

        return [
            'filename' => $plan->getImage(),
            'width'    => $img[0],
            'height'   => $img[1],
        ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetHouseImage()
    {

        $id = Yii::$app->request->post('id');
        $type = Yii::$app->request->post('type');
        $house = $this->findHouse($id);

        Yii::$app->response->format = Response::FORMAT_JSON;

        $img = getimagesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/house/' . $house->image);
        $sections = $house->realtySections;

        if ($type == 1)
            $sections = false;

        return [
            'filename' => $house->getImage(),
            'width'    => $img[0],
            'height'   => $img[1],
            'sections' => $sections ? $sections : false,
        ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetSectionImage()
    {
        $id = Yii::$app->request->post('id');
        $section = $this->findSection($id);

        Yii::$app->response->format = Response::FORMAT_JSON;

        $img = getimagesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/section/' . $section->image);

        return [
            'filename' => $section->getImage(),
            'width'    => $img[0],
            'height'   => $img[1],
        ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetFloorImage()
    {
        $id = Yii::$app->request->post('id');
        $floor = $this->findFloor($id);

        Yii::$app->response->format = Response::FORMAT_JSON;

        $img = getimagesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/floor/' . $floor->image);

        return [
            'filename' => $floor->getImage(),
            'width'    => $img[0],
            'height'   => $img[1],
        ];
    }


    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetDataList()
    {
        $id = Yii::$app->request->post('id');
        $type = Yii::$app->request->post('type');
//
        if (!$type || !$id)
            throw new NotFoundHttpException('The requested page does not exist.');

        $sections = $floors = $flats = [];

        switch ($type) {
            case 'section':
                $sections = RealtySection::find()->where(['house_id' => $id])->select(['id', 'title'])->all();
                if (!$sections) {
                    $floors = RealtyFloor::find()->where(['house_id' => $id])->select(['id', 'title'])->all();
                    $sections = false;
                }
                break;
            case 'floor':
                $floors = RealtyFloor::find()->where(['section_id' => $id])->select(['id', 'title'])->all();
                if (!$floors)
                    $floors = false;
                break;
            case 'flat':
                $flats = RealtyFlat::find()->where(['floor_id' => $id])->select(['id', 'title', 'scale', 'rooms', 'price'])->all();
                if (!$flats)
                    $flats = false;
                break;
        }


        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'sections' => $sections,
            'floors'   => $floors,
            'flats'    => $flats,
        ];
    }

    public function actionPdf($id)
    {
        $model = $this->findFlat($id);
        $content = $this->renderPartial('_pdf', [
            'model' => $model,
        ]);
//
//        $customFontsConfig = '/core/config/mpdf_config.php';
//        $customFonts = '/fonts';
//        define("_MPDF_SYSTEM_TTFONTS_CONFIG", $customFontsConfig);
//        define("_MPDF_SYSTEM_TTFONTS", $customFonts);

        $pdf = new Pdf([
            'mode'        => Pdf::MODE_UTF8,
            'format'      => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'content'     => $content,
            'cssFile'     => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'filename'    => 'ЖК Корабли. Квартира - №' . $model->number . '.pdf',

            'options' => [
                'title' => 'ЖК Корабли. Квартира - №' . $model->number,
            ],
        ]);
        $pdf->render();
    }


    public function actionTour($id)
    {
        $model = $this->findFlat($id);
        
        return $this->render('tour', ['flat' => $model]);
    }
    /**
     * @return string
     */
    public function actionIndex()
    {
        $genPlan = RealtyPlan::getDb()->cache(function () {
            return RealtyPlan::find()->where(['status' => RealtyPlan::STATUS_PUBLISHED])->one();
        }, RealtyPlan::DEFAULT_CACHE_DURATION, RealtyPlan::getDbDependency());

        return $this->render('index', ['genPlan' => $genPlan]);
    }


    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionViewHouse($id)
    {
        $house = $this->findHouse($id);
        $houses = \app\modules\realty\models\RealtyHouse::find()->all();
        $view = ($house->realtySections) ? 'house_sections' : 'house';

        return $this->render($view, ['model' => $house, 'houses' => $houses]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewSection($id)
    {
        if(!Yii::$app->user->id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $section = $this->findSection($id);
        $prev = RealtySection::getDb()->cache(function () use ($section) {
            $result = RealtySection::find()->where(['ordering' => $section->ordering - 1, 'house_id' => $section->house_id])->limit(1)->one();
            if ($result)
                return ['url' => $result->seoUrl, 'title' => $result->title];

            return null;
        }, RealtySection::DEFAULT_CACHE_DURATION, RealtySection::getDbDependency());

        $next = RealtySection::getDb()->cache(function () use ($section) {
            $result = RealtySection::find()->where(['ordering' => $section->ordering + 1, 'house_id' => $section->house_id])->limit(1)->one();
            if ($result)
                return ['url' => $result->seoUrl, 'title' => $result->title];

            return null;
        }, RealtySection::DEFAULT_CACHE_DURATION, RealtySection::getDbDependency());

        return $this->render('section', [
            'model' => $section,
            'prev'  => $prev,
            'next'  => $next,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewFloor($id)
    {
        if(!Yii::$app->user->id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = $this->findFloor($id);
        $prev = RealtyFloor::getDb()->cache(function () use ($model) {
            $result = RealtyFloor::find()->where(['ordering' => $model->ordering - 1, 'section_id' => $model->section_id, 'house_id' => $model->house_id])->limit(1)->one();
            if ($result)
                return ['url' => $result->seoUrl, 'title' => $result->title];

            return null;
        }, RealtyFloor::DEFAULT_CACHE_DURATION, RealtyFloor::getDbDependency());

        $next = RealtyFloor::getDb()->cache(function () use ($model) {
            $result = RealtyFloor::find()->where(['ordering' => $model->ordering + 1, 'section_id' => $model->section_id, 'house_id' => $model->house_id])->limit(1)->one();
            if ($result)
                return ['url' => $result->seoUrl, 'title' => $result->title];

            return null;
        }, RealtyFloor::DEFAULT_CACHE_DURATION, RealtyFloor::getDbDependency());

        return $this->render('floor', [
            'model' => $model,
            'prev'  => $prev,
            'next'  => $next,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewFlat($id)
    {
        $model = $this->findFlat($id);

        return $this->render('flat', ['model' => $model]);
    }


    /**
     * @param $id
     *
     * @return null|static
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findPlan($id)
    {
        if (($model = RealtyPlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * @param $id
     *
     * @return null|static
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findSection($id)
    {
        if (($model = RealtySection::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * @param $id
     *
     * @return null|static
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findHouse($id)
    {
        if (($model = RealtyHouse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     *
     * @return null|static
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findFloor($id)
    {
        if (($model = RealtyFloor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     *
     * @return null|static
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findFlat($id)
    {
        if (($model = RealtyFlat::find()->where(['id' => $id])->limit(1)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        if ($this->action->id == 'params')
        {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);

    }

    public function actionParams(){

        $data = Yii::$app->request->post();

        if(Yii::$app->request->isAjax && !$data){
            $flats = RealtyFlat::getDb()->cache(function () {
                return RealtyFlat::find()->where(['sale_status' => 1])->all();
            }, RealtyFlat::DEFAULT_CACHE_DURATION, RealtyFlat::getDbDependency());

            $content = $this->renderPartial('_flats', compact('flats'));

            return $content;
        } else if(Yii::$app->request->isAjax && $data){
            $flats = RealtyFlat::getDb()->cache(function () use ($data) {
                return RealtyFlat::find()
                    ->where(['<=', 'scale', doubleval($data['scaleMax'])])
                    ->andWhere(['>=', 'scale', doubleval($data['scaleMin'])])
                    ->andWhere(['<=', 'rooms', intval($data['flatsMax'])])
                    ->andWhere(['>=', 'rooms', intval($data['flatsMin'])])

                    ->andWhere(['<=', 'price', intval($data['price'])])
//                    ->joinWith(['floor f'])
//                    ->andWhere(['<=', 'f.title', $data['floorMin']])
                    ->andWhere(['sale_status' => 1])
                    ->all();
            }, RealtyFlat::DEFAULT_CACHE_DURATION, RealtyFlat::getDbDependency());

            if(isset($data['rooms']))
                $view = '_flats_data';
            else
                $view = '_flats_data2';
            $content = $this->renderPartial($view, compact('flats', 'data'));

            return $content;
        }

        return false;

    }

    public static function search($array,$login)
    {
        $i=0;
        do
            if($array[$i]['name'] == $login)
                return true;
        while(++$i<count($array));

        return false;
    }

}
