<?php

namespace app\modules\realty\controllers;

use app\common\BaseAdminController;
use app\modules\realty\models\RealtyFlat;
use app\modules\realty\models\RealtyFloor;
use app\modules\realty\models\RealtyHouse;
use Yii;
use app\modules\realty\models\RealtyClient;
use app\modules\realty\models\RealtyClientSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ClientController implements the CRUD actions for RealtyClient model.
 */
class ClientController extends BaseAdminController
{

    /**
     * Lists all RealtyClient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RealtyClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RealtyClient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RealtyClient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RealtyClient();

        if ($model->load(Yii::$app->request->post())) {
            $model->files = UploadedFile::getInstances($model, 'files');

            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'sections' => [],
                'floors'   => [],
                'flats' => [],
            ]);
        }
    }

    /**
     * Updates an existing RealtyClient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->files = UploadedFile::getInstances($model, 'files');

            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        }

        $house = RealtyHouse::find()->where(['id' => $model->house_id])->one();
        $sections = ArrayHelper::map($house->realtySections, 'id', 'title');

        $query = RealtyFloor::find();
        $query->andFilterWhere(['house_id' => $house->id]);

        if($model->section_id)
            $query->andFilterWhere(['section_id' => $model->section_id]);

        $floors = ArrayHelper::map($query->all(),'id', 'title');

        $list = [];
        $flats =RealtyFlat::find()->where(['floor_id' => $model->floor_id])->all();

        if($flats)
            foreach ($flats as $flat) {
                $list[$flat->id] = $flat->title . " ({$flat->scale} кв.м., комнат: {$flat->rooms}, цена: {$flat->price})";
            }

        return $this->render('update', [
            'sections' => $sections ? $sections : ['У дома нет секций, выберите этаж...'],
            'floors'   => $floors,
            'model' => $model,
            'flats' => $list,
        ]);

    }

    /**
     * Deletes an existing RealtyClient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RealtyClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RealtyClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RealtyClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionDeleteFile($file)
    {
        @unlink($file);
    }
}
