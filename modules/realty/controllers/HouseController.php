<?php

namespace app\modules\realty\controllers;

use app\common\BaseAdminController;
use Yii;
use app\modules\realty\models\RealtyHouse;
use app\modules\realty\models\RealtyHouseSearch;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * HouseController implements the CRUD actions for RealtyHouse model.
 */
class HouseController extends BaseAdminController
{

    /**
     * Lists all RealtyHouse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RealtyHouseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RealtyHouse model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RealtyHouse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new RealtyHouse();
        $model->scenario = 'create';
        $model->status = $model::STATUS_ACTIVE;

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->video = UploadedFile::getInstance($model, 'video');
            if ($model->save()) {
                Yii::$app->session->setFlash('humane', 'Сохранено');

                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RealtyPlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->video = UploadedFile::getInstance($model, 'video');
            if ($model->save()) {
                Yii::$app->session->setFlash('humane', 'Сохранено');

                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RealtyHouse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RealtyHouse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RealtyHouse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RealtyHouse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
