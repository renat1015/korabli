<?php

namespace app\modules\virtualTour;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\virtualTour\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
