<?php

namespace app\modules\virtualTour\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\virtualTour\models\TourItem;

/**
 * TourItemSearch represents the model behind the search form about `app\modules\virtualTour\models\TourItem`.
 */
class TourItemSearch extends TourItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tour_map_id', 'status'], 'integer'],
            [['title', 'url', 'width', 'height', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TourItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'tour_map_id' => $this->tour_map_id,
            'status'      => $this->status,
            'updated_at'  => $this->updated_at,
            'created_at'  => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'width', $this->width])
            ->andFilterWhere(['like', 'height', $this->height]);

        return $dataProvider;
    }
}
