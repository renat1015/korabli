<?php

namespace app\modules\virtualTour\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "tour_map".
 *
 * @property integer    $id
 * @property string     $title
 * @property string     $image
 * @property integer    $status
 * @property string     $updated_at
 * @property string     $created_at
 *
 * @property TourItem[] $tourItems
 */
class TourMap extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tour_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'title'      => 'Название',
            'image'      => 'Изображение для карты',
            'status'     => 'Публдиковать',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourItems()
    {
        return $this->hasMany(TourItem::className(), ['tour_map_id' => 'id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/tour_map/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/tour_map/' . $file_name);
            $this->image = $file_name;

            if (!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/tour_map/' . $this->oldAttributes['image']);

        } else {
            if (!$insert)
                $this->image = $this->oldAttributes['image'];
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/tour_map/' . $this->image)) {
            return '/uploads/tour_map/' . $this->image;
        } else
            return false;

    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList()
    {
        $items = self::find()->asArray()->all();

        return $items;
    }
}
