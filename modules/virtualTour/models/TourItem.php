<?php

namespace app\modules\virtualTour\models;

use app\common\BaseModel;
use Yii;

/**
 * This is the model class for table "tour_item".
 *
 * @property integer $id
 * @property integer $tour_map_id
 * @property string  $title
 * @property string  $url
 * @property string  $width
 * @property string  $height
 * @property integer $status
 * @property string  $updated_at
 * @property string  $created_at
 * @property string  $point_x
 * @property string  $point_y
 *
 * @property TourMap $tourMap
 */
class TourItem extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tour_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tour_map_id', 'title', 'url', 'width', 'height', 'point_x', 'point_y'], 'required'],
            [['tour_map_id', 'status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title', 'url', 'width', 'height'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'tour_map_id' => 'Карта для меток',
            'title'       => 'Название',
            'url'         => 'Ссылка на тур',
            'width'       => 'Width',
            'height'      => 'Height',
            'status'      => 'Публиковать',
            'updated_at'  => 'Updated At',
            'created_at'  => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourMap()
    {
        return $this->hasOne(TourMap::className(), ['id' => 'tour_map_id']);
    }
}
