<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_083221_tour_init extends Migration
{
    public function up()
    {
        $this->createTable('tour_map', [
            'id'     => $this->primaryKey(),
            'title'  => $this->string(255)->notNull(),
            'image'  => $this->string(255)->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);

        $this->createTable('tour_item', [
            'id'          => $this->primaryKey(),
            'tour_map_id' => $this->integer()->notNull(),
            'title'       => $this->string(255)->notNull(),
            'url'         => $this->string(255)->notNull(),
            'width'       => $this->string(50)->notNull(),
            'height'      => $this->string(50)->notNull(),
            'point_x'     => $this->string(50)->notNull(),
            'point_y'     => $this->string(50)->notNull(),

            'status' => $this->smallInteger(1)->defaultValue(1),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);

        $this->addForeignKey('fk_tour_tour_item', 'tour_item', 'tour_map_id', 'tour_map', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_tour_tour_item', 'tour_item');
        $this->dropTable('tour_item');
        $this->dropTable('tour_map');

    }

}
