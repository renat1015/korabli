<?php
/**
 * @var $tour app\modules\virtualTour\models\TourMap
 */
use yii\helpers\Html;

$this->context->sidebarAdvancedClass = 'hide-sub';
$size = getimagesize(Yii::$app->basePath . '/../' . $tour->getImage());
$this->context->showLoading = true;
?>


    <div class="tour dynamics" style="background-image: url(<?= $tour->getImage(); ?>);">
        <div id="rotate-device">
            <div class="middle">
                <img src="/img/rotate__device.svg">
            </div>
        </div>
        <div class="title">
            <h3>3D тур</h3>
            <p>Для просмотра панорамы, выберите интересующую вас точку (синий маркер) и нажмите по нему кнопкой мыши</p>
        </div>

        <?php
        foreach ($tour->tourItems as $item) :?>
            <div class="tour-point" style="left: <?= $item->width; ?>%; top: <?= $item->height; ?>%">
                <?php echo Html::a(Html::tag('span', '3D', [
                    'data'  => [
                        'toggle'    => 'tooltip',
                        'placement' => 'top',
                        'w'         => $item->width,
                        'h'         => $item->height,
                    ],
                ]), ['/virtualTour/default/view', 'id' => $item->id]);?>
            </div>
        <?php endforeach;
        ?>

        <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
    </div>


<?php
$this->registerJS(<<<JS
    var resizeTimer;
    $('#point-container .point-block a span').hover(
        function(){
            $(this).parent('a').addClass('active');
        },
        function(){
            $(this).parent('a').removeClass('active');
        }
    );

    function setSize(){
        var left_m = (" . $size[0] ." - window.innerWidth)/2;
        var top_m = (" . $size[1] ." - window.innerHeight)/2;
        $('.point-block').css({'margin-top':'-'+top_m+'px','margin-left':'-'+left_m+'px' });
    }

    setTimeout(function(){
        $('.sidebar .toggle').trigger('click');
    },1000);

    $(document).ready(function(){
        setTimeout(function(){
            var interval = setInterval(function(){
                if(window.loaded)
                {
                    clearInterval(interval);
                    readyToShow = true;
                    setSize();
                    setImgContainerPos();
                    loadingBlockHide();
                }
            },100);
        },100);
    });

    $(window).on('resize', function(e) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        setImgContainerPos();
        setSize();
      }, 150);
    });
JS
);
?>