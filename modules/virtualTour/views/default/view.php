<?php
/**
 * @var $tour app\modules\virtualTour\models\TourMap
 * @var $this View
 */
use yii\helpers\Html;
use yii\web\View;

$this->context->sidebarAdvancedClass = 'virtual__tour';
//$this->context->showLoading = true;
$this->registerJs(<<<JS

    setTimeout(function(){
        $('.sidebar .toggle').trigger('click');
    },1000);
JS
);
?>
<div class="tour-iframe dynamics">
    <?= Html::a('<span class="btn-close"></span>',['/virtualTour/default/index'],['class'=>'tour-close']) ?>
    <iframe src="<?= $url ?>" frameborder="0"></iframe>
</div>