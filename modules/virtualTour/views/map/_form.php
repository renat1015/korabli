<?php

use kartik\widgets\SwitchInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\virtualTour\models\TourMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-map-form">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::className(), [
        'options'       => [
            'multiple' => false,
            'accept'   => 'image/*',
        ],
        'pluginOptions' => [
            'previewFileType'       => 'image',
            'showCaption'           => false,
            'showRemove'            => false,
            'showUpload'            => false,
            'browseClass'           => 'btn btn-primary btn-block',
            'browseIcon'            => '',
            'allowedFileExtensions' => ['jpg', 'png'],
            'maxFileSize'           => 5120,
            'minImageWidth'         => 640,
            'minImageHeight'        => 480,
            'maxFileCount'          => 1,
            'previewTemplates'      => [
                'image' => ' <img src="{data}" style="width:auto; height: 165px;" class="file-preview-image pull-left" title="{caption}" alt="{caption}"/>',
            ],
            'initialPreview'        => ($model->isNewRecord) ? '' : Html::img($model->getImage(), ['style' => 'width:auto; height:165px;']),
        ],
    ]) ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
        [
            'pluginOptions' =>
                [
                    'size'    => 'small',
                    'onText'  => 'Да',
                    'offText' => 'Нет',
                    'state'   => $model->isNewRecord ? true : $model->status,
                ],
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
