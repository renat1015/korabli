<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\virtualTour\models\TourMap */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Карты для туров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
