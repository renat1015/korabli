<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\virtualTour\models\TourMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Карты для туров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-map-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'title',
            [
                'attribute' => 'image',
                'format'    => 'raw',
                'value'     => function ($data) {
                    return Html::img($data->getImage(), ['style' => 'height:165px;']);
                },
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>

</div>
