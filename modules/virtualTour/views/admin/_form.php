<?php

use kartik\widgets\SwitchInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\virtualTour\models\TourItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tour_map_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\virtualTour\models\TourMap::getList(), 'id', 'title')) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'width', ['template' => '{input}'])->hiddenInput() ?>

    <?= $form->field($model, 'height', ['template' => '{input}'])->hiddenInput() ?>

    <?= $form->field($model, 'point_x', ['template' => '{input}'])->hiddenInput() ?>

    <?= $form->field($model, 'point_y', ['template' => '{input}'])->hiddenInput() ?>

    <div style="width: 100%; height: 500px; overflow: scroll">
        <div id="point-block" style="position: relative; width: 100%; height: 300px; background: #ccc">
            <div id="point"
                 style="position: absolute;width: 33px; height: 45px; cursor: pointer; transform: translate(<?= $model->point_x ?>px, <?= $model->point_y ?>px);"
                 data-x="<?= $model->point_x ?>" data-y="<?= $model->point_y ?>">
                <img src="/img/point_with_flag.png" alt="">
            </div>
        </div>
    </div>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
        [
            'pluginOptions' =>
                [
                    'size'    => 'small',
                    'onText'  => 'Да',
                    'offText' => 'Нет',
                    'state'   => $model->isNewRecord ? true : $model->status,
                ],
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


    <script src="/js/interact.min.js"></script>
    <script>
        // target elements with the "draggable" class
        interact('#point')
            .draggable({
                inertia: true,
                restrict: {
                    restriction: "parent",
                    endOnly: true,
                    elementRect: {top: 0, left: 0, bottom: 1, right: 1}
                },
                autoScroll: true,

                // call this function on every dragmove event
                onmove: dragMoveListener,
                onend: function (event) {
                    var el_width = $('#point-block').width(),
                        el_height = $('#point-block').height(),
                        point_x = $('#point').attr('data-x'),
                        point_y = $('#point').attr('data-y'),
                        pr_width = point_x * 100 / el_width,
                        pr_height = point_y * 100 / el_height;

                    $('#touritem-point_x').val(point_x);
                    $('#touritem-point_y').val(point_y);
                    $('#touritem-width').val(pr_width);
                    $('#touritem-height').val(pr_height);

                }
            });

        function dragMoveListener(event) {
            var target = event.target,
            // keep the dragged position in the data-x/data-y attributes
                x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

            // translate the element
            target.style.webkitTransform =
                target.style.transform =
                    'translate(' + x + 'px, ' + y + 'px)';

            // update the posiion attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        }

        // this is used later in the resizing and gesture demos
        window.dragMoveListener = dragMoveListener;

        function setImage() {
            var id = $("#touritem-tour_map_id").val();

            $.post('/virtualTour/map/get-map-image', {id: id}, function (result) {

                if (result == null || typeof(result) == 'undefined') {

                }
                else {
                    $('#realtyhouse-cords').attr('data-image-url', result.filename);
                    $('#point-block').css({
                        background: 'url(' + result.filename + ')',
                        width: result.width,
                        height: result.height
                    });
                }
            }, "json");
        }

    </script>
</div>

<?php $this->registerJs('
    $(document).ready(function(){
        $("#touritem-tour_map_id").trigger("change");
    });

    $("#touritem-tour_map_id").change(function(){
       setImage();
    });

') ?>

