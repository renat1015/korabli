<?php

namespace app\modules\virtualTour\controllers;

use app\common\BaseController;
use app\modules\virtualTour\models\TourItem;
use app\modules\virtualTour\models\TourMap;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class DefaultController extends BaseController
{
    public function actionIndex()
    {
        $tour = TourMap::findOne(1);

        return $this->render('index', [
            'tour' => $tour,
        ]);
    }
    public function actionView()
    {

        $id = \Yii::$app->request->get('id');

//        var_dump(\Yii::$app->request->get());
        if ($id) {
            $item = $this->findModel($id);

            return $this->render('view', [
                'url' => $item->url,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModel($id)
    {
        if (($model = TourItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
