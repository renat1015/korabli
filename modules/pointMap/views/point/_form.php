<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use kartik\switchinput\SwitchInput;
use app\extensions\fileapi\FileAPIAdvanced;

$this->registerJs(
    'const container = document.querySelector(".container-image");

    container.addEventListener("click", e => {
        const x = ((100 * e.offsetX)/container.width).toFixed(2);
        const y = ((100 * e.offsetY)/container.height).toFixed(2)

        document.getElementById("point-point_x").value = x;
        document.getElementById("point-point_y").value = y;

        $(".container-point").css({"top":y+"%", "left":x+"%", "background-color":"red"});
    }, false);',
    View::POS_END
);

/* @var $this yii\web\View */
/* @var $model app\modules\pointMap\models\Point */
/* @var $form yii\widgets\ActiveForm */

$items = \app\modules\realty\models\RealtyHouse::getList();
$list = [];
$list[0] = "Не дом";
foreach ($items as $item) {
    $list[$item->id] = $item->title;
}
?>

<div class="point-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'point_type')->dropDownList(\app\modules\pointMap\models\PointType::getList()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(\yii\imperavi\Widget::className(), [
        'options' => [
            'buttonSource' => 'true',
            'minHeight'    => 150,
        ],
        'plugins' => [
            'fullscreen',
            'clips',
            'fontcolor',
            'fontfamily',
            'fontsize',
        ],
    ]) ?>

    <?= $form->field($model, 'home_num')->dropDownList($list) ?>

    <div class="xy" style="position:relative;">
        <img src="<?= $imageSlide->getImage() ?>" alt="img" class="container-image" style="width:100%;">
        <div class="container-point" style="left: <?=$model->point_x?>%; top: <?=$model->point_y?>%; position: absolute; width: 10px; height: 10px; border-radius: 50%; <?= (!empty($model->point_x)) ? "background-color: red;" : ""; ?>"></div>
    </div>

    <?= $form->field($model, 'point_x')->textInput() ?>

    <?= $form->field($model, 'point_y')->textInput() ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
        [
            'pluginOptions' =>
            [
                'size'    => 'small',
                'onText'  => 'Да',
                'offText' => 'Нет',
                'state'   => $model->isNewRecord ? true : $model->status,
            ],
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>