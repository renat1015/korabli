<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\pointMap\models\PointType */

$this->title = 'Create Point Type';
$this->params['breadcrumbs'][] = ['label' => 'Point Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
