<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\pointMap\models\PointType */

$this->title = 'Update Point Type: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Point Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="point-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
