<?php

namespace app\modules\pointMap;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\pointMap\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
