<?php

namespace app\modules\pointMap\models;

use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "point_type".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 */
class PointType extends \app\common\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'point_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'image' => 'Изображение',
            'status' => 'Публиковтаь',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/pointMap/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/pointMap/' . $file_name);
            $this->image = $file_name;

            if (!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/pointMap/' . $this->oldAttributes['image']);

        } else {
            if (!$insert)
                $this->image = $this->oldAttributes['image'];
        }

        return parent::beforeSave($insert);
    }


    /**
     * @return bool|string
     */
    public function getImage()
    {
        if (file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/pointMap/' . $this->image)) {
            return '/uploads/pointMap/' . $this->image;
        } else
        return false;
    }

    /**
     *
     */
    public function afterDelete()
    {
        @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/pointMap/' . $this->image);
        parent::afterDelete();

    }

    /**
     * @return array
     */
    public static function getList()
    {
        $items = self::find()->where(['status' => self::STATUS_PUBLISHED])->all();

        $list = [];
        foreach ($items as $item) {
            $list[$item->id] = $item->title;
        }

        return $list;
    }

    public static function getTypePoint($id)
    {
        return self::find()->where(['id'=>$id, 'status'=>PointType::STATUS_PUBLISHED])->one();
    }
}
