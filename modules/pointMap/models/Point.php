<?php

namespace app\modules\pointMap\models;

use Yii;

/**
 * This is the model class for table "point".
 *
 * @property integer $id
 * @property integer $point_type
 * @property string $title
 * @property string $description
 * @property integer $home_num
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 * @property double $point_x
 * @property double $point_y
 */
class Point extends \app\common\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_type', 'title', 'home_num', 'status', 'point_x', 'point_y'], 'required'],
            [['point_type', 'home_num', 'status'], 'integer'],
            [['description'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['point_x', 'point_y'], 'number'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_type' => 'Тип точки',
            'title' => 'Название',
            'description' => 'Описание',
            'home_num' => 'Номер дома',
            'status' => 'Публиковать',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
            'point_x' => 'Точка X',
            'point_y' => 'Точка Y',
        ];
    }
}
