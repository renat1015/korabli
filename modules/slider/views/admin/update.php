<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\slider\models\Slider */

$this->title = 'Редактировать слайд: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Слайдер для главной', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="slider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
