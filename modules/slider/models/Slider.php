<?php

namespace app\modules\slider\models;

use app\common\BaseModel;
use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $ordering
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 * @property string $image
 */
class Slider extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['text', 'description'], 'string'],
            [['ordering', 'status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название слайда',
            'text' => 'Текст слайда',
            'ordering' => 'Сортировка',
            'status' => 'Публиковать',
            'description' => 'Описание',
            'image' => 'Изображения для слайда',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
        ];
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/slider/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/slider/' . $file_name);
            $this->image = $file_name;

            if(!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/slider/' . $this->oldAttributes['image']);

        } else {
            if(!$insert)
                $this->image = $this->oldAttributes['image'];
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return bool|string
     */
    public function getImage()
    {
        if(!$this->image)
            return false;

        if(file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/slider/' . $this->image))
        {
            return '/uploads/slider/' . $this->image;
        } else
            return false;

    }

    /**
     * @return mixed
     */
    public static function getLastOrder()
    {
        $last_item = self::find()->orderBy('ordering DESC')->one();

        return $last_item->ordering + 1;
    }
}
