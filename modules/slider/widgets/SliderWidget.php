<?php
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 28.10.15
 * Time: 11:53
 */

namespace app\modules\slider\widgets;


use app\common\BaseModel;
use app\modules\slider\models\Slider;
use app\modules\pointMap\models\Point;
use app\modules\pointMap\models\PointType;
use app\modules\realty\models\RealtyHouse;
use app\modules\realty\models\RealtyFlat;
use yii\base\Widget;

class SliderWidget extends Widget
{
    public function run()
    {
        $models = Slider::getDb()->cache(function(){
            return Slider::find()->where(['ordering'=>1, 'status'=>Slider::STATUS_PUBLISHED])->orderBy('ordering ASC')->all();
        },BaseModel::DEFAULT_CACHE_DURATION,Slider::getDbDependency());

        $points = Point::find()->where(['status'=>Point::STATUS_PUBLISHED])->all();

        if($models)
            return $this->render('slider_widget',['models'=>$models, 'points'=>$points]);

        return false;
    }

    static function getTypePoint($id)
    {
        return PointType::find()->where(['id'=>$id, 'status'=>PointType::STATUS_PUBLISHED])->one();
    }

    static function getApartmentsByHouseId($id)
    {
        return RealtyFlat::find()->where(['house_id'=>$id, 'sale_status'=>1, 'status'=>PointType::STATUS_PUBLISHED])->all();
    }
}