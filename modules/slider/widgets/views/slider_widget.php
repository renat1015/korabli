<?php
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 28.10.15
 * Time: 13:06
 * @var $model Slider
 */
use app\modules\slider\models\Slider;
use yii\web\View;

$this->registerJsFile("@web/js/show.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCss(<<<CSS
.tooltip_box{
	display: none;
	background-color: #fff;
	padding: 10px;
	border-radius: 10px;
	position: absolute;
	object-fit: contain;
	margin-left: 25px;
	margin-top: 25px;
	z-index: 1002;
	font-size: 12px;
}
CSS
)
?>
<div class="slick-control">
	<a href="#" class="arrow prev"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
	<a href="#" class="arrow next"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
</div>
<div class="slick main-owl">
	<?php foreach($models as $model): ?>
		<div class="item">
			<?php if($model->ordering == 1): ?>
				<div class="map-xxx" id="map_xxx" style="width:fit-content; min-width:100%; position:relative;">
					<img src="<?= $model->getImage() ?>" alt="img" style="min-width: 100%; width: auto;">
					<?php $i = 1; ?>
					<?php foreach($points as $point): ?>
						<?php $point_type = app\modules\slider\widgets\SliderWidget::getTypePoint($point->point_type); ?>
						<img src="<?= $point_type->getImage() ?>" id="point-on-map<?= $i ?>" class="point-on-map" alt="img" style="left: <?= $point->point_x ?>%; top: <?= $point->point_y ?>%; position: absolute; width: 50px; height: 50px; object-fit: contain; margin-left: -25px; margin-top: -25px; z-index: 1000;" data_title="<?= $point->title ?>" data_type="<?= $point_type->title ?>">
						<div class="tooltip_box" style="left: <?= $point->point_x ?>%; top: <?= $point->point_y ?>%;">
							<span><b><?= $point->title ?></b></span>
							<br>
							<span><?= $point->description ?></span>
							<?php if($point->home_num != 0): ?>
								<?php $vacant_apartment = app\modules\slider\widgets\SliderWidget::getApartmentsByHouseId($point->home_num); ?>
								<?php if(count($vacant_apartment) > 0): ?>
									<?php $arr=[]; ?>
									<?php foreach($vacant_apartment as $item): ?>
										<?php if(!empty($arr[$item->rooms])): ?>
											<?php $arr[$item->rooms] += 1; ?>
										<?php else: ?>
											<?php $arr[$item->rooms] = 1; ?>
										<?php endif; ?>
									<?php endforeach; ?>
									
									<span>Квартир в продаже:</span><br>
										<?php foreach($arr as $key=>$value): ?>
											<?php if($key == 0): ?>
												<span>Студий: </span>
											<?php elseif($key != 0): ?>
												<span><?= $key ?>-комнатных: </span>
											<?php endif; ?>
											<?= $value; ?><br>
									<?php endforeach; ?>
								<?php endif; ?>

							<?php endif; ?>
						</div>
						<?php $i++; ?>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>