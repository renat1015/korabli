<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 11.05.15
 * Time: 19:20
 */

namespace app\modules\image\widgets;


use yii\base\Widget;

class InputWidget extends Widget{
    public $model;
    public $form;
    public function run()
    {
        return $this->render('input_widget',['model'=>$this->model,'form'=>$this->form]);
    }
}