<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 11.05.15
 * Time: 19:22
 * @var yii\web\View $this
 */

use kartik\widgets\FileInput;
$model_class_name = $reflect = new ReflectionClass($model::className());
$model_class_name =  strtolower($reflect->getShortName());
$field_class = 'field-'.$model_class_name.'-imgfiles';
$preview_width = 230;
$preview_height = 148;
echo $form->field($model, 'imgFiles[]')->widget(FileInput::classname(), [
    'options'       => [
        'accept'   => 'image/*',
        'multiple' => true,
        'language' => Yii::$app->language,
    ],
    'pluginOptions' => [
        'overwriteInitial'      => false,
        'allowedFileExtensions' => ['jpg', 'gif', 'png',],
//        'maxImageWidth'         => 1920,
//        'maxImageHeight'        => 1080,
        'maxFileCount'          => 30,
        'showCaption'           => false,
        'showRemove'            => false,
        'showUpload'            => false,
        'browseClass'           => 'btn btn-primary btn-block',
        'browseIcon'            => '<i class="glyphicon glyphicon-camera"></i> ',
        'initialPreview'        => ($model->isNewRecord) ? '' : \app\modules\image\models\Image::getImagesPreview($model->id, $model::getModelName(), $preview_width, $preview_height),
        'previewTemplates'      => [
            'image' => '<div class="file-preview-frame" id="{previewId}"xmlns="http://www.w3.org/1999/html">
                    <img src="{data}" style="width:'.$preview_width.'px; height: '.$preview_height.'px;" class="file-preview-image" title="{caption}" alt="{caption}"/>
                    <span class="delete-photo"><i class="glyphicon glyphicon-trash"></i></span>
                    </div>'
        ],
        'previewSettings'       => [
            'image'  => '{width: "'.$preview_width.'px", height: "'.$preview_height.'px"}',
            'object' => '{width: "'.$preview_width.'px", height: "'.$preview_height.'px"}',
            'html'   => '{width: "'.$preview_width.'px", height: "'.$preview_height.'px"}',
        ],
    ],
    'pluginEvents' => [
        'fileloaded' =>  'function(event, file, previewId, index, reader)
        {
            if(deletedArr.length > 0){
                jQuery.each(deletedArr,function(i,val){
                    $("#"+val).remove();
                });
            }
            $("#"+previewId).prependTo(".'.$field_class.' .file-preview .file-preview-thumbnails");
        }',
    ]
]);
$css_preview_height = $preview_height.'px';
$css = <<<CSS
.fileinput-remove{display:none;}
.file-preview-frame img{display: block; margin-bottom: 5px;}
.file-preview-frame .delete-photo{display: block; cursor: pointer; color: #db6967; padding-top: 2px}
.file-preview-frame .delete-photo:hover{background: #db6967; color: #fff;}
.file-preview-image {height: $css_preview_height}
CSS;
$this->registerCss($css);


$this->registerJs(<<<JS
    var deletedArr = [];

    $(document).on('click','.delete-photo',function(e){
        var element = $(this).parents('.file-preview-frame');
        if($(this).attr('data-id'))
        {
            var href = '/image/admin/delete-image';
            if($(this).attr('data-href'))
                href = $(this).attr('data-href');
            $.ajax({
                url: href+'?id='+$(this).attr('data-id'),
                type: 'delete',
                dataType: 'json',
                data: {_csrf: yii.getCsrfToken()},
                success: function(result)
                {
                    if(result.result == true)
                    {
                        deletedArr.push($(element).attr('id'));
                        $(element).remove();
                    }
                }
            });
        }
        else
        {
            $(element).remove();
        }
    });
JS
);