<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 11.05.15
 * Time: 15:58
 */

namespace app\modules\image\behaviors;

use app\modules\image\models\Image;
use Imagine\Image\Box;
use Yii;
use yii\base\Behavior;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\imagine\Image as ImageHelper;

class ImagesBehavior extends Behavior
{

    public $model;
    public $attribute;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT  => 'saveImages',
            ActiveRecord::EVENT_AFTER_UPDATE  => 'saveImages',
            ActiveRecord::EVENT_BEFORE_DELETE => 'deleteImages',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        parent::attach($owner);

        if (empty($this->attribute)) {
            throw new InvalidParamException("Invalid or empty \"{$this->attribute}\" array");
        }
    }

    /**
     * @param $event
     */
    public function saveImages($event)
    {
        $attribute = $this->attribute;
        if ($this->owner->$attribute != null) {
            $basePath = Yii::$app->basePath . '/../uploads/images/' . $this->model . '/' . $event->sender->id . '/';
            FileHelper::createDirectory($basePath);
            foreach ($event->sender->imgFiles as $file) {

                $name = substr(md5(microtime()), 0, 15) . '.' . $file->extension;
                $imageModel = new Image();
                $imageModel->model_id = $event->sender->id;
                $imageModel->model_name = $this->model;
                $imageModel->image = $name;
                $imageModel->ordering = $imageModel->getMaxOrder($imageModel->tableName());
                $imageModel->save(false);
                $file->saveAs($basePath . $name);
                $originalPath = $basePath . $name;
                $imgInfo = getimagesize($originalPath);
                if($imgInfo[0] > 1920 || $imgInfo[1] > 1080)
                {
                    ImageHelper::getImagine()->open($originalPath)->resize(new Box(1920,1080))->save($originalPath,['quality' => 100]);
                }
            }
        }
    }

    public function deleteImages($event)
    {
        $id = $this->owner->id;
        Yii::$app->db->createCommand("DELETE FROM image WHERE model_id=$id AND model_name='" . $this->model . "'")->execute();
        FileHelper::removeDirectory(Yii::$app->basePath . '/../uploads/images/' . $this->model . '/' . $event->sender->id . '/');
    }
}