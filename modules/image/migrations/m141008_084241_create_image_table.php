<?php

use yii\db\Schema;
use yii\db\Migration;

class m141008_084241_create_image_table extends Migration
{
    public function up()
    {
        $this->createTable('image', [
            'id'         => Schema::TYPE_PK,
            'model_name' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название модели"',
            'model_id'   => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Id модели"',
            'image'      => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название изображения"',
            'is_main'    => Schema::TYPE_SMALLINT . '(1) NULL DEFAULT "0" COMMENT "Главное изображение"',
            'ordering'   => Schema::TYPE_INTEGER . ' NULL DEFAULT "1" COMMENT "Сортировка"',
            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);
    }

    public function down()
    {
        $this->dropTable('image');
    }
}
