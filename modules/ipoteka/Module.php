<?php

namespace app\modules\ipoteka;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\ipoteka\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
