<?php

namespace app\modules\ipoteka\models;

use app\common\BaseModel;
use app\modules\image\models\Image;
use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $ordering
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 * @property string $image
 */
class Banks extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stavka'], 'required'],
            [['stavka'], 'number'],
            [['min_credit_period', 'max_credit_period', 'min_payment_int', 'max_payment_int'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['image'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stavka' => 'Ставка',
            'image' => 'Изображения',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата добавления',
            'min_credit_period' => 'Минимальный период кредитования',
            'max_credit_period' => 'Максимальный период кредитования',
            'min_payment_int' => 'Минимальный процент взноса',
            'max_payment_int' => 'Максимальный процент взноса',
        ];
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        FileHelper::createDirectory(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/banks/');

        if ($this->image) {
            $file_name = md5(microtime()) . '.' . $this->image->extension;
            $this->image->saveAs('uploads/banks/' . $file_name);
            $this->image = $file_name;

            if(!$insert && $this->oldAttributes['image'] != $file_name)
                @unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/banks/' . $this->oldAttributes['image']);

        } else {
            if(!$insert)
                $this->image = $this->oldAttributes['image'];
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function getImage($width=null,$height=null)
    {
        if(!$this->image)
            return false;
        $model_base_path = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads/banks/';
        $base_file_path = $model_base_path . $this->image;
        if(file_exists($base_file_path))
        {
            if(!$width && !$height)
                return '/uploads/banks/' . $this->image;
            else
            {
                return Image::getThumb($width,$height,self::getModelName(),$this->id,$this->image,[
                    'original_file_path' => '/uploads/banks/'.$this->image,
                    'base_path' =>  '/uploads/banks/'.$this->id.'/'
                ]);
            }
        } else
            return false;
    }

}
