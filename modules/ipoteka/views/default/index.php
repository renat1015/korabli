<?php
use app\modules\ipoteka\models\Banks;
use yii\web\View;
use yii\bootstrap\ActiveForm;

/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 03.11.15
 * Time: 10:50
 * @var $this View
 * @var $model Architecture
 */
$this->context->sidebarAdvancedClass = 'closed';
$this->context->headerAdvanceClass = 'calculator';
?>

<div class="kor-choice-home calculator param-open">
    <div id="rotate-device">
        <div class="middle">
            <img src="/img/rotate__device.svg">
        </div>
    </div>
    <div class="title">
        <h3>Калькулятор ипотеки</h3>
        <a href="#">
                <span>Расчеты носят приблизительный характер,
пожалуйста, уточняйте условия кредитования в банках</span>
        </a>
    </div>
    <div class="kor-param calculator">
        <div class="param-form">
            <form method="post" action="#" class="param-calculator" id="getBanks">
                <?php $form = ActiveForm::begin([
                    'id' => 'getBanks',
                    'options' => [
                        'class' => 'param-calculator',
                    ]
                ])?>
                <div class="param-price form_input">
                    <h6 class="param-title" >Стоимость квартиры:</h6>
                    <p>
                        <?php $value = ($price) ? number_format($price,0,'',' ') : '1 500 000'?>
                        <input class="param-input" type="text" id="amount-max" placeholder="0" value="<?= $value;?>">
                        <label><i class="fa fa-rub" aria-hidden="true"></i></label>
                    </p>
                    <div id="slider-price"></div>
                </div>
                <div class="param-oun-cash form_input">
                    <h6 class="param-title">
                        <span class="media-1920">Собственные средства:</span>
                        <span class='media-1366'>Собств. ср-ва:</span>
                        <span class="part">88%</span>
                    </h6>
                    <p>
                        <input class="param-input" type="text" id="amount-max-first" placeholder="0" value="400 000">
                        <label><i class="fa fa-rub" aria-hidden="true"></i></label>
                    </p>
                    <div id="slider-oun-cash"></div>
                </div>
                <div class="param-years form_input">
                    <h6 class="param-title">Срок кредита:</h6>
                    <p>
                        <input class="param-input" type="text" id="amount-max-year" readonly placeholder="0" value="15">
                        <label>лет</label>
                    </p>
                    <div id="slider-years"></div>
                </div>
                <div class="param-resalt">
                    <h6 class="param-title">Сумма кредита</h6>
                    <div class="resalt"><span class="value">0</span><label><i class="fa fa-rub" aria-hidden="true"></i></label> </div>
                </div>
                <button type="submit" onclick="ym(45591978,'reachGoal','ipoteka_rasschet_click')">Рассчитать</button>
            </form>
            <div class="table param-calculator">
                <div class="thead">
                    <div class="tr">
                        <div class="td" >Банк</div>
                        <div class="td" >Сумма кредита <span class="top-i">руб.</span></div>
                        <div class="td" >Срок <span class="top-i">месяц</span></div>
                        <div class="td" >Ставка <span class="top-i">%</span></div>
                        <div class="td price-text">Ежемесячный платеж <span class="top-i">руб</span></div>
                    </div>
                </div>
                <div class="tbody">
                </div>
            </div>
        </div>
    </div>
    <div class="brush"><a href="/kvartiryi-s-otdelkoy"><img src="/img/brush.png" alt="img"><span>Квартиры с отделкой</span></a></div>
</div>

<?php

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/desktop/jquery-ui.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/desktop/jquery-ui.min.js', ['depends' => 'yii\web\JqueryAsset']);

if($price) {

$this->registerJs(<<<JS
    $(document).ready(function(){
        $('#getBanks').submit();
    });
JS
);

}

?>