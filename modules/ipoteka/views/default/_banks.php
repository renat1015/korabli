<?php if(!empty($models)): ?>
	<?php foreach ($models as $model):?>
	<div class="tr">
	    <span class="td">
	        <img src="<?= $model->getImage();?>" alt="img">
	    </span>
	    <span class="td bank_credit"></span>
	    <span class="td total_credit_time"></span>
	    <span class="td interest_rate"><?= $model->stavka;?></span>
	    <span class="price td monthly_pay"></span>
	</div>
	<?php endforeach;?>
<?php else: ?>
    <div style="margin-top: 15%;">
	    <span class="td">
	        К сожалению по вашим параметрам ничего не найдено!
	    </span>
	    <br>
	    <span class="td">
	        Попробуйте изменить условия!
	    </span>
    </div>
<?php endif; ?>