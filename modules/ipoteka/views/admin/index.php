<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Банки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить банк', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'title',
            [
                'attribute'=>'image',
                'format'=>'raw',
                'value'=>function($data){return Html::img($data->getImage(),['style'=>'height:50px;']);}
            ],
            'stavka',
            'min_credit_period',
            'max_credit_period',
            'min_payment_int',
            'max_payment_int',
//            [
//                'attribute'=>'status',
//                'filter'=>$searchModel::getStatusList(),
//                'value'=>function($data){return $data->getStatusName();}
//            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
