<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\slider\models\Slider */

$this->title = 'Добавить банк';
$this->params['breadcrumbs'][] = ['label' => 'Банки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
