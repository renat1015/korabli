<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\slider\models\Slider */

$this->title = 'Редактировать банк: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Банки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="slider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
