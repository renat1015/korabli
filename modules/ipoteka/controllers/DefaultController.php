<?php
/**
 * Created by PhpStorm.
 * User: saniok
 * Date: 03.11.15
 * Time: 10:49
 */

namespace app\modules\ipoteka\controllers;


use app\common\BaseController;
use app\common\BaseModel;
use app\modules\ipoteka\models\Banks;

class DefaultController extends BaseController
{
    public function actionIndex()
    {
        $models = Banks::getDb()->cache(function(){
            return Banks::find()->all();
        },BaseModel::DEFAULT_CACHE_DURATION,Banks::getDbDependency());

        $price = \Yii::$app->request->get('price');

        return $this->render('index',['models'=>$models, 'price'=>$price]);
    }

    public function actionBanks(){

        $data = \Yii::$app->request->post();
        if(\Yii::$app->request->isAjax && $data){

            $models = Banks::getDb()->cache(function() use($data){
                return Banks::find()
                    ->where(['<=', 'min_credit_period', $data['credit_time']])
                    ->andWhere(['>=', 'max_credit_period', $data['credit_time']])
                    ->andWhere(['<=', 'min_payment_int', $data['first_pay']])
                    ->andWhere(['>=', 'max_payment_int', $data['first_pay']])
                    ->all();
            },BaseModel::DEFAULT_CACHE_DURATION,Banks::getDbDependency());


            $content =  $this->renderPartial('_banks', compact('models'));

            return $content;
        }

        return false;

    }

    public function beforeAction($action)
    {
        if ($this->action->id == 'banks')
        {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);

    }
}