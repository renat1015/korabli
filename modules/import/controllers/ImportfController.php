<?php

namespace app\modules\import\controllers;

use app\common\BaseAdminController;
use app\common\BaseController;
use app\modules\import\models\ImportForm;
use app\modules\realty\models\RealtyFlat;
use app\modules\realty\models\RealtyFloor;
use app\modules\realty\models\RealtyHouse;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ContentController implements the CRUD actions for Content model.
 */
class ImportfController extends BaseController
{


    public function actionCron(){
        $xml = file_get_contents('https://korablinn.ru/xml-1c/1c_obmen.xml');

        $import_array = new \SimpleXMLElement($xml);

        foreach ($import_array->Дом as $item){
            if(!$house = RealtyHouse::findOne(intval($item->Код))){
                $house =  new RealtyHouse();
                $house->id = intval($item->Код);
            }

            $house->plan_id = 1;
            $house->id = intval($item->Код);
            if(!$house->title){
                $house->title = strval($item->Наименование);
            }
            if(strval($item->Изображение) != '' && strval($item->Изображение) != ' '){
                $house->image = strval($item->Изображение);
            } else {
                $house->image = '';
            }

            if(!$house->cords){
                $house->cords = '';
            }
            if(!$house->flats){
                $house->flats = 1;
            }
            $house->floors = intval($item->Количество_этажей);

            $house->save(false);
        }

        foreach ($import_array->Этаж as $item){
            if(!$floor = RealtyFloor::findOne(intval($item->Код))){
                $floor =  new RealtyFloor();
                $floor->house_id = intval($item->Код_дома);
            }

            $floor->id = intval($item->Код);

            $floor->title = strval($item->Наименование);

            if(strval($item->Изображение) != '' && strval($item->Изображение) != ' '){
                $floor->image = strval($item->Изображение);
            }
            if(!$floor->cords){
                $floor->cords = '';
            }

            $floor->save(false);
        }

        foreach ($import_array->Квартира as $item){
            if(!$flat = RealtyFlat::findOne(intval($item->Код))){
                $flat =  new RealtyFlat();
                $flat->id = intval($item->Код);
            }

            $flat->floor_id = intval($item->Код_этажа);
            foreach ($import_array->Этаж as $floor){
                if(intval($floor->Код) == intval($item->Код_этажа)){
                    $flat->house_id = intval($floor->Код_дома);
                }
            }
            $flat->title = strval($item->Тип_Помещения);
            if(!$flat->number){
                $flat->number = intval($item->Наименование);
            }
            if(strval($item->Изображение) != '' && strval($item->Изображение) != ' '){
                $flat->image = strval($item->Изображение);
            }
            $flat->price = doubleval($item->Цена);
            if(strval($item->Тип_Помещения) == 'Студия'){
                $flat->rooms = 0;
            } else {
                $flat->rooms = intval($item->Количество_комнат);
            }

            $flat->scale = doubleval($item->Общая_площадь);
            $flat->living_scale = doubleval($item->Жилая_площадь);
            if(strval($item->Продано) != 'false'){
                $flat->sale_status = 2;
            } else {
                $flat->sale_status = 1;
            }
            if($item->Площадь_лоджии){
                $flat->room_type['Лоджия'] = 'Лоджия';
                $flat->room_scale['Лоджия'] = doubleval($item->Площадь_лоджии);
            }
            if($item->Площадь_кухни){
                $flat->room_type['Кухня'] = 'Кухня';
                $flat->room_scale['Кухня'] = doubleval($item->Площадь_кухни);
            }

            $flat->save(false);
        }
    }

    public function actionZhilstroynn()
    {
        $xml = file_get_contents('https://korablinn.ru/xml-1c/VTorpedo_zhilstroynn.xml');

        $import_array = new \SimpleXMLElement($xml);

        $i = 0;

        foreach ($import_array->offer as $item){
            if($i >= 1) break;


            if(!$flat = RealtyFlat::findOne(intval($item->attributes()['internal-id']))){
                $flat =  new RealtyFlat();
                $flat->id = intval($item->attributes()['internal-id']);
            }


//                var_dump($item->attributes()['internal-id']);

//
//                $flat->floor_id = intval($item->Код_этажа);
//                foreach ($import_array->Этаж as $floor){
//                    if(intval($floor->Код) == intval($item->Код_этажа)){
//                        $flat->house_id = intval($floor->Код_дома);
//                    }
//                }
//                $flat->title = strval($item->Тип_Помещения);
//                if(!$flat->number){
//                    $flat->number = intval($item->Наименование);
//                }
//                var_dump($item->image);
            if(strval($item->image) == '' || strval($item->image) == ' '){
                $flat->image_zh = '';
            } else {
                $flat->image_zh = strval($item->image);
            }
            $flat->price = doubleval($item->price->value);
//                if(strval($item->Тип_Помещения) == 'Студия'){
//                    $flat->rooms = 0;
//                } else {
//                    $flat->rooms = intval($item->Количество_комнат);
//                }
//
//                $flat->scale = doubleval($item->Общая_площадь);
//                $flat->living_scale = doubleval($item->Жилая_площадь);
            switch ($item->condition) {
                case 'свободно' : {
                    $flat->sale_status = 1;
                    break;
                }
                case 'продано' : {
                    $flat->sale_status = 2;
                    break;
                }
                case 'забронировано' : {
                    $flat->sale_status = 3;
                    break;
                }
                default : {
                    $flat->sale_status = 2;
                    break;
                }
            }
//                if(strval($item->Продано) != 'false'){
//                    $flat->sale_status = 2;
//                } else {
//                    $flat->sale_status = 1;
//                }
//                if($item->Площадь_лоджии){
//                    $flat->room_type['Лоджия'] = 'Лоджия';
//                    $flat->room_scale['Лоджия'] = doubleval($item->Площадь_лоджии);
//                }
//                if($item->Площадь_кухни){
//                    $flat->room_type['Кухня'] = 'Кухня';
//                    $flat->room_scale['Кухня'] = doubleval($item->Площадь_кухни);
//                }
//
//                var_dump($flat);

//            var_dump($flat->save(false));
            $flat->save(false);

//                $i++;
        }
    }

    public function actionTorpedo()
    {
        $xml = file_get_contents('https://korablinn.ru/xml-1c/%D0%92Torpedo_zhilstroynn.xml');

        $import_array = new \SimpleXMLElement($xml);

        foreach ($import_array->offer as $item){

            if(!$house = RealtyHouse::find()->where(['title' => $item->{'building-name'}])->one()){
                $house =  new RealtyHouse();
                $house->title = strval($item->{'building-name'});
                $house->house_number = strval($item->{'building-section'});
                $house->status = 1;
                $house->plan_id = 1;
                $house->flats = 1;
                $house->floors = strval($item->{'floors-total'});

                $house->save(false);
            }

            if(!$floor = RealtyFloor::find()->where(['title' => strval($item->{'floor'}), 'house_id' => $house->id])->one()){
                $floor = new RealtyFloor();

                $floor->title = strval($item->{'floor'});
                $floor->house_id = $house->id;
                $floor->status = 1;
                $floor->ordering = strval($item->{'floor'});

                $floor->save(false);
            }

            if(!$flat = RealtyFlat::findOne(intval($item->attributes()['internal-id']))){
                $flat =  new RealtyFlat();
                $flat->id = intval($item->attributes()['internal-id']);

                $flat->rooms = intval($item->rooms);
                $flat->scale = doubleval($item->area->value);
                $flat->number = intval($item->{'number-flat'});
                $flat->status = 1;
                $flat->house_id = $house->id;
                $flat->floor_id = $floor->id;
            }

            if(strval($item->studio) == 'true'){
                $flat->title = 'Студия';
                $flat->rooms = 0;
            } else {
                $flat->title = 'Квартира';
            }

            if(strval($item->image) == '' || strval($item->image) == ' '){
                $flat->image_zh = '';
            } else {
                $flat->image_zh = strval($item->image);
            }
            $flat->price = doubleval($item->price->value);

            switch ($item->condition) {
                case 'свободно' : {
                    $flat->sale_status = 1;
                    break;
                }
                case 'продано' : {
                    $flat->sale_status = 2;
                    break;
                }
                case 'забронировано' : {
                    $flat->sale_status = 3;
                    break;
                }
                default : {
                    $flat->sale_status = 2;
                    break;
                }
            }


            $flat->save(false);
        }
    }

}
