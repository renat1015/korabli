<?php
/**
 * Created by PhpStorm.
 * User: Vadym
 * Date: 30.03.2018
 * Time: 16:53
 */

namespace app\modules\import\models;

use yii\base\Model;
use yii\web\UploadedFile;

class ImportForm extends Model
{

    public $file;
    public $only_status = false;

    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xml'],
            [['only_status'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
            'only_status' => 'Только статус',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/import/' . $this->file->baseName . '.' . $this->file->extension);
            return $this->file;
        } else {
            return false;
        }
    }

}