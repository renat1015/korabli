<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\content\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Import';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">
        <?php $form = ActiveForm::begin(
            [
                'options'=>[
                    'enctype'=>'multipart/form-data'
                ]
            ]
        ); ?>

        <?php echo $form->field($model, 'file')->fileInput(['class' => '']) ;?>

        <?php echo $form->field($model, 'only_status')->checkbox() ;?>

        <button type="submit" class="btn btn-success">Импортировать1</button>

        <?php ActiveForm::end(); ?>

</div>
