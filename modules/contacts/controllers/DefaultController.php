<?php

namespace app\modules\contacts\controllers;

use app\common\BaseController;
use app\modules\contacts\models\ContactsCall;
use app\modules\contacts\models\ContactsLetter;
use app\modules\contacts\models\ContactsReserve;
use Yii;

class DefaultController extends BaseController
{

    public function actionLetter()
    {
        $model = new ContactsLetter();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('humane', 'Ваш запрос отправлен.');

            return $this->redirect(['/uspeh']);
        } else {
            return $this->renderAjax('letter', [
                'model' => $model,
            ]);
        }
    }


    public function actionCall()
    {
        $model = new ContactsCall();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('humane', 'Ваш запрос отправлен.');

            return $this->redirect(['/uspeh']);
        } else {
            return $this->renderAjax('call', [
                'model' => $model,
            ]);
        }
    }

    public function actionReserve()
    {
        $model = new ContactsReserve();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('humane', 'Ваш запрос отправлен.');

            return $this->redirect(['/uspeh']);
        } else {
            return $this->renderAjax('reserve', [
                'model' => $model,
            ]);
        }
    }
}
