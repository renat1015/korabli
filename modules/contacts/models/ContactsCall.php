<?php

namespace app\modules\contacts\models;

use app\common\BaseModel;
use Yii;

/**
 * This is the model class for table "contacts_call".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $phone
 * @property string  $time
 * @property string  $updated_at
 * @property string  $created_at
 */
class ContactsCall extends BaseModel
{
    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts_call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required','message'=>'Укажите '.$this->getAttributeLabel('phone')],
            ['name','required','message'=>'Укажите '.$this->getAttributeLabel('name')],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'phone', 'time', 'url'], 'string', 'max' => 255],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6Lfa9L0ZAAAAAHhviP4u8Itx5EujZVWmFY6LAWGG', 'uncheckedMessage' => 'Please confirm that you are not a bot.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Имя',
            'phone'      => 'Телефон',
            'time'       => 'Удобное время',
            'updated_at' => 'Дата добавления',
            'created_at' => 'Created At',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->mailer->compose("_back_call", [
            'model' => $this,
        ])
            ->setTo('korablinn2020@yandex.ru')
            ->setFrom(['korablinn2020@yandex.ru' => Yii::$app->name])
            ->setSubject('Сообщение с сайта '.Yii::$app->name)
            ->send();
        parent::afterSave($insert, $changedAttributes);
    }


}
