<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "contacts_letter".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $mail
 * @property string  $message
 * @property string  $updated_at
 * @property string  $created_at
 */
class ContactsReserve extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts_reserve';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required','message'=>'Укажите '.$this->getAttributeLabel('phone')],
            ['name','required','message'=>'Укажите '.$this->getAttributeLabel('name')],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'phone', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Имя',
            'mail'       => 'E-mail',
            'updated_at' => 'Дата добавления',
            'created_at' => 'Created At',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->mailer->compose("_back_reserve", [
            'model' => $this,
        ])
            ->setTo('korablinn2020@yandex.ru')
            ->setFrom(['korablinn2020@yandex.ru' => Yii::$app->name])
            ->setSubject('Сообщение с сайта '.Yii::$app->name)
            ->send();
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }


}
