<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_073821_init_contacts extends Migration
{
    public function up()
    {
        $this->createTable('contacts_letter', [
            'id'      => $this->primaryKey(),
            'name'    => $this->string()->notNull(),
            'mail'    => $this->string()->notNull(),
            'message' => $this->text()->notNull(),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);

        $this->createTable('contacts_call', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'time'  => $this->string()->defaultValue(null),

            'updated_at' => 'timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'created_at' => 'timestamp not null',
        ]);
    }

    public function down()
    {
        $this->dropTable('contacts_letter');
        $this->dropTable('contacts_call');
    }

}
