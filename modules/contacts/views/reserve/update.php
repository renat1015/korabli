<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\contacts\models\ContactsLetter */

$this->title = 'Update Contacts Reserve: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Contacts Letters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contacts-letter-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
