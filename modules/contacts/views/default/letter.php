<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\contacts\models\ContactsLetter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-letter-form">

    <h1>НАПИСАТЬ ПИСЬМО<span>Оставьте свои пожелания или задайте вопрос</span></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'url')->input('hidden', ['id' => 'url'])->label(false); ?>

    <?= $form->field($model, 'name', ['template' => '{input}{error}'])->textInput(['maxlength' => true, 'placeholder' => 'Ваше имя']) ?>

    <?= $form->field($model, 'mail', ['template' => '{input}{error}'])->textInput(['maxlength' => true, 'placeholder' => 'Ваш e-mail']) ?>

    <?= $form->field($model, 'message', ['template' => '{input}{error}'])->textarea(['rows' => 6, 'placeholder' => 'Текст сообщения']) ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['onclick' => "ym(45591978,'reachGoal','obratnaya_svyaz_send')"]) ?>
    </div>

    <p>Нажимая на кнопку, Вы принимаете <a href="#">Положение</a> и
        <a href="#">согласие</a> на обработку персональных данных.</p>

    <?php ActiveForm::end(); ?>

</div>
