<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\contacts\models\ContactsLetter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-letter-form">

    <h1>Заказать звонок <span>Оставьте свои данные и мы Вам перезвоним</span></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'url')->input('hidden', ['id' => 'url'])->label(false); ?>

    <?= $form->field($model, 'name', ['template' => '{input}{error}'])->textInput(['maxlength' => true, 'placeholder' => 'Ваше имя']) ?>

    <?= $form->field($model, 'phone', ['template' => '{input}{error}'])->widget(\yii\widgets\MaskedInput::className(), [
        'mask'    => '+7 (999) 999-99-99',
        'options' => [
            'placeholder' => '+7 (___) ___-__-__',
            'class' => 'form-control',
        ],
    ]) ?>

    <?= $form->field($model, 'time', ['template' => '{input}{error}'])->textInput(['maxlength' => true, 'placeholder' => 'Удобное время']) ?>

    <?= $form->field($model, 'reCaptcha')->widget(
        \himiklab\yii2\recaptcha\ReCaptcha::className(),
        ['siteKey' => '6Lfa9L0ZAAAAAEmoSkitBtO_77nXPP_ptRfDKU9K']
    )->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['onclick' => "ym(45591978,'reachGoal','callback_send')"]) ?>
    </div>

    <p>Нажимая на кнопку, Вы принимаете <a href="#">Положение</a> и
        <a href="#">согласие</a> на обработку персональных данных.</p>
    <?php ActiveForm::end(); ?>

</div>
