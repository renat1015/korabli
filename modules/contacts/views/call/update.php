<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\contacts\models\ContactsCall */

$this->title = 'Update Contacts Call: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Contacts Calls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contacts-call-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
