<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\contacts\models\ContactsCallrSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обратный звонок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-call-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'name',
            'phone',
            'time',
            'url',
            'updated_at',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{delete}',

            ],
        ],
    ]); ?>

</div>
