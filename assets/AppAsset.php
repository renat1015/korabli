<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,600i,700,700i,800&amp;subset=cyrillic',
        'https://fonts.googleapis.com/css?family=Lato:300,400,700,900',
        'https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i',
        'css/desktop/slick.css',
        'css/desktop/font-awesome.min.css',
        'css/desktop/jquery.mCustomScrollbar.css',
        'css/desktop/magnific-popup.css',
        'css/desktop/main.css',
        'css/desktop/media.css',
    ];
    public $js = [
        '//code.jquery.com/jquery-migrate-1.2.1.min.js',
        'js/desktop/slick.min.js',
        'js/desktop/sticky_kit.js',
        'js/desktop/jquery.magnific-popup.js',
//        'js/desktop/jQRangeSlider-min.js',
        'js/desktop/jquery.mask.min.js',
        'js/desktop/jquery.mCustomScrollbar.concat.min.js',
        'js/desktop/script.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
