<?php
include(__DIR__ . '/../common/Helper.php');
$modules = include('modules.php');
$rootDir = dirname(dirname(__DIR__));
//$params = require $rootDir . '/core/config/params.php';
Yii::setAlias('root', $rootDir);

$config = [
    'id'             => 'basic',
    'basePath'       => dirname(__DIR__),
    'bootstrap'      => ['log'],
    'language'       => 'ru',
    'sourceLanguage' => 'ru-RU',
    'name'           => 'ЖК Корабли',
    'modules'        => $modules,
    'components'     => [
        'i18n'         => [
            'translations' => [
                '*' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                ],
            ],
        ],
        'view'         => [
            'title' => 'ЖК Корабли',
        ],
        'assetManager' => [
            'linkAssets' => true,
        ],
        'request'      => [
            'cookieValidationKey' => 'dkjfhSJDFLSDLfSDf8sdfsd9fsadLHSKDJf',
        ],
        'cache'        => [
            'class' => 'yii\caching\FileCache',
        ],
        'user'         => [
            'identityClass'   => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager'  => [
            'class' => 'app\modules\user\modules\rbac\components\PhpManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer'       => [
            'class'            => 'yii\swiftmailer\Mailer',
//            'useFileTransport' => (YII_DEBUG) ? true : false,
            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.yandex.ru',
//                'username' => 'www@marshalgrad.ru',
//                'password' => 'vvvv+9033838',
//                'port' => '465', // Port 25 is a very common port too
//                'encryption' => 'ssl', // It is often used, check your provider or mail server specs//

                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'korablinn2020@yandex.ru',
                'password' => 'Pdtplf*%@37',
                'port' => '465', // Port 25 is a very common port too
                'encryption' => 'ssl', // It is often used, check your provider or mail server specs

            ],
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager'   => [
            'class'           => 'app\common\BaseUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                'admin'                              => 'admin/default/index',
                'login'                              => 'site/login',
                'site/gallery/architecture'          => 'architecture/default/index',
                'site/gallery/construction-progress' => 'progress/default/index',
                'site/gallery/tour'                  => 'virtualTour/default/index',
                'site/gallery/tourView'                  => 'virtualTour/default/view',
                '/site/gallery/list'                 => 'gallery/default/index',
                'realty'                             => 'realty/realty/index',
                'news'                               => 'news/news/index',
                'import'                             => 'import/import/index'
            ],
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6Lfa9L0ZAAAAAEmoSkitBtO_77nXPP_ptRfDKU9K',
            'secret' => '6Lfa9L0ZAAAAAHhviP4u8Itx5EujZVWmFY6LAWGG',
        ],
        'db'           => require(__DIR__ . '/db.php'),
    ],
//    'options' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
