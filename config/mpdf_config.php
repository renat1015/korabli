<?php
/**
 * Created by PhpStorm.
 * User: Vadym
 * Date: 23.04.2018
 * Time: 16:54
 */

$this->fontdata = [
    'AAvante' => [
        'R' => 'AAvanteLtDemiBoldItalic.woff',
    ],
    'OpenSansB' => [
        'R' => 'opensans-bold.ttf',
    ],
];