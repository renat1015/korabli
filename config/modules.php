<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 13.05.15
 * Time: 11:37
 */

$modules = getJsonModulesData();
$list = array();

if($modules)
    foreach ($modules as $name => $module) {
        $list[$name] = [
            'class' => $module['class']
        ];
    }
$list['gridview'] = [
   'class' => '\kartik\grid\Module',

];
return $list;


////////////////////////////////////////////////////////
function getModules()
{
    $dir = $_SERVER["DOCUMENT_ROOT"]."/core/modules/";
    $modules = scandir($dir);
    if(!$modules)
        return false;

    unset($modules[0], $modules[1]);

    return $modules;

}

function getJsonModulesData()
{
    $list = getModules();
    $modules = [];

    foreach ($list as $module) {
        $conf = $_SERVER["DOCUMENT_ROOT"].'/core/modules/'.$module.'/config.json';
        if(file_exists($conf))
        {
            $json = file_get_contents($conf);
            $data = json_decode($json, true);
            $modules[$module] = $data;
        }
    }

    return $modules;
}