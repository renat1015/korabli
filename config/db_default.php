<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 12.05.15
 * Time: 9:36
 */

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=fastsite',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];