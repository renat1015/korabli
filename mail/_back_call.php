<?php
/**
 * Created by PhpStorm.
 * User: yurik
 * Date: 11.12.15
 * Time: 15:57
 * @var $model app\modules\contacts\models\ContactsCall
 */
?>
<h3>Заявка на обратный звонок</h3>

<div>Имя: <strong><?= $model->name?></strong></div>
<div>Телефон: <strong><?= $model->phone?></strong></div>
<div>Удобное время для звонка: <strong><?= $model->time?></strong></div>
<div>Со страницы: <strong><?= $model->url?></strong></div>

<hr>

