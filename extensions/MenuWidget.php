<?php
/**
 * Created by PhpStorm.
 * User: Yatskanich Oleksandr
 * Date: 05.11.14
 * Time: 16:31
 */

namespace app\extensions;


use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Menu;

class MenuWidget extends Menu{
    public $items = [];

    protected function renderItem($item)
    {
        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            return strtr($template, [
                '{url}' => (\Yii::$app->request->url != $item['url']) ? Url::to($item['url']) : '#',
                '{label}' => $item['label'],
            ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => $item['label'],
            ]);
        }
    }
} 