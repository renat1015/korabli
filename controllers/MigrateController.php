<?php

namespace app\controllers;

use app\modules\admin\models\Migration;
use app\modules\admin\models\Module;
use Yii;
use yii\web\Controller;

class MigrateController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionMigrate()
    {
        $modules = Yii::$app->request->post('modules');
        if($modules) {
            foreach ($modules as $module) {
                $res = Migration::migrate('up', $module);
                if($res == 0)
                    Module::installModule($module, false);
                echo $module . " - " . ($res == 0 ? 'installed' : 'error') . "\n";
            }
        }
    }

}