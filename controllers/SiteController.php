<?php

namespace app\controllers;

use app\common\BaseController;
use app\common\BaseModel;
use app\modules\mapLocation\models\MapPoint;
use app\modules\user\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use yii\helpers\Html;
use yii\web\Response;
use app\modules\slider\models\Slider;
use app\modules\pointMap\models\Point;
use yii\base\Widget;

class SiteController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = Slider::getDb()->cache(function(){
            return Slider::find()->where(['ordering'=>1, 'status'=>Slider::STATUS_PUBLISHED])->one();
        },BaseModel::DEFAULT_CACHE_DURATION,Slider::getDbDependency());

        $points = Point::find()->where(['status'=>Point::STATUS_PUBLISHED])->all();

        if($model)
            return $this->render('index',['model'=>$model, 'points'=>$points]);

        return false;
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionMapLocations()
    {
        $pointModels = MapPoint::getDb()->cache(function () {
            return MapPoint::find()->where(['status' => MapPoint::STATUS_PUBLISHED])->all();
        }, BaseModel::DEFAULT_CACHE_DURATION, MapPoint::getDbDependency());
        $points = [];
        if ($pointModels) {
            foreach ($pointModels as $point) {
                $points[] = [
                    'main'    => $point->pointType->is_main,
                    'map_icon'    => $point->pointType->getImage(),
                    'title'       => $point->title,
                    'lat_lng'     => explode(',', $point->latlng),
                    'object_desc' => '<div class="clearfix map-object-desc"><div class="image"><img src="' . $point->imageUrl(true) . '" alt="' . $point->title . '"/></div><div class="text"><div class="text"><h3>'.$point->title.'</h3></div><div class="desc">' . $point->description . '</div></div></div>'
                ];
            }
        } else
            $points = false;

        return $this->render('map_locations', ['points' => $points]);
    }

    public function actionGallery()
    {
        return $this->render('gallery');
    }

    public function actionFadeRedirect()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(($is_video = Yii::$app->request->post('after_video')) != null)
            Yii::$app->session->set('is_fade_redirect',true);
        else
            Yii::$app->session->set('is_fade_redirect_with_logo',true);
        return true;
    }
}
